#!/bin/bash
set -e

#function to print bold $1
bold() {
    echo -e "\033[1m$1\033[0m"
}

heading() {
    echo ""
    bold "==$(printf "%${#1}s" | tr " " "=")=="
    bold "  $1  "
    bold "==$(printf "%${#1}s" | tr " " "=")=="
    echo ""
}

heading "Running with env vars:"

echo "DRUSH_REBUILD=${DRUSH_REBUILD}"

# First; run migrations and such
if [[ -z "${DRUSH_REBUILD}" || "${DRUSH_REBUILD}" == "1" ]]; then
    heading "RUNNING MIGRATIONS, IMPORT, LOCALE, ETC."

    cd /var/www/app
    composer bootstrap
    cd -
else
    heading "SKIPPING MIGRATIONS, IMPORT, LOCALE, ETC."
fi

heading "Creating directory $(bold "/var/www/app/private")."

mkdir -p /var/www/app/private

heading "Pushing Deploy Event to Event Logging Service"

# Determine payload to push to Event Logging Service and register Deploy Event in order to make version known.
php .ci/payload_build_info.php | curl \
    -X POST -H "Content-Type: application/json" --data-binary @- \
    "https://event-logging-service.toegangvooriedereen.nl/api/deploy_events" -vvv

heading "Starting supervisord"

exec supervisord -n
