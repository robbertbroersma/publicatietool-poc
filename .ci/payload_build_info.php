<?php

/**
 * Used in .ci/entrypoint.sh to establish payload to push to the Event Logging API,
 * so the deploy is being logged.
 */

declare(strict_types=1);

$appEnv = getenv("APP_ENV");
$appName = getenv("APP_NAME");

if (empty($appName)) {
    fwrite(STDERR, "Not pushing version. APP_NAME=<empty>".PHP_EOL);
    exit(1);
}

$buildInfoAppVersion = getenv("BUILD_INFO_APP_VERSION");
$buildInfoDate = getenv("BUILD_INFO_DATE");
$buildInfoGitCommitHash = getenv("BUILD_INFO_GIT_COMMIT_HASH");
$buildInfoGitCommitMessage = getenv("BUILD_INFO_GIT_COMMIT_MESSAGE");
$buildInfoGitCommitBranch = getenv("BUILD_INFO_GIT_BRANCH");

$environment = match ($appEnv) {
    "prod" => "production",
    "production" => "production",
    "staging" => "staging",
    "sandbox" => "sandbox",
    "qa" => "qa",
    "continuous" => "continuous",
    default => null
};

if (!$environment) {
    fwrite(
        STDERR,
        sprintf(
            "Not pushing version. APP_ENV=%s",
            $appEnv ?: "<empty>"
        ).PHP_EOL
    );
    exit(1);
}

echo json_encode([
    "name" => $appName,
    "environment" => $environment,
    "version" => $buildInfoAppVersion ?: null,
    "gitCommitBranch" => $buildInfoGitCommitBranch ?: null,
    "gitCommitHash" => $buildInfoGitCommitHash ?: null,
    "gitCommitMessage" => $buildInfoGitCommitMessage ?: null,
    "builtAt" => $buildInfoDate ?: null,
    "deployedAt" => date(DateTimeInterface::ATOM),
], JSON_PRETTY_PRINT);
