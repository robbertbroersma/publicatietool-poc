/* eslint-disable cypress/no-unnecessary-waiting */

Cypress.on('uncaught:exception', (err) => {
    console.error('An uncaught exception occurred:', err);
    return false;
});

describe('Document bewerken', () => {
    beforeEach(() => {
        cy.login('Maurice', '12341234');
    });
    afterEach(() => {
        cy.deleteDocuments();
    });

    it('Gebruiker kan via drag-and-drop een .docx uploaden en deze vervolgens bewerken', () => {
        cy.step('navigeer naar de documentenpagina');
        cy.visit('/documenten');
        cy.fileUploadDropzone('docx/4.docx', '4.docx', 'Document 4');
        cy.step('bewerk document 4');
        cy.clickDropbutton('Document 4', 'bewerken');
        cy.visit('/documenten');
    });
});
