import { Options } from 'cypress-axe';

Cypress.on('uncaught:exception', (err) => {
    console.error('An uncaught exception occurred:', err);
    return false;
});

describe('Accessibility', () => {
    const A11Y_OPTIONS: Options = {
        runOnly: { type: 'tag', values: ['wcag21aa'] },
    };

    it('Pagina voldoet aan WCAG21aa toegangkelijkheidseisen', () => {
        cy.visit('/');
        cy.injectAxe();
        cy.checkA11y(undefined, A11Y_OPTIONS);
    });
});
