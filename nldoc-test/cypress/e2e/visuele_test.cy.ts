/* eslint-disable cypress/no-unnecessary-waiting */

Cypress.on('uncaught:exception', (err) => {
    console.error('An uncaught exception occurred:', err);
    return false;
});

describe('Visuele tests op NLDoc', () => {
    beforeEach(() => {
        cy.eyesOpen({
            // The name of the app under test
            appName: 'NLdoc',
            // The name of the test case
            testName: Cypress.currentTest.title,
        });
    });

    afterEach(() => {
        cy.eyesClose();
    });

    it('Controleer landingpagina', () => {
        cy.visit('/');

        cy.get('[class="dialog-off-canvas-main-canvas"]').should('be.visible');

        cy.eyesCheckWindow();
    });

    it('Controleer loginpagina', () => {
        cy.visit('/user/login');

        cy.contains('h1', 'Inloggen');

        cy.eyesCheckWindow();
    });

    it('Controleer documentenoverzichtspagina', () => {
        cy.login('Maurice', '12341234');

        cy.visit('/documenten');
        cy.clickDropbutton('Document 5', 'bewerken');

        cy.eyesCheckWindow();
    });

    it('Controleer document-bewerkpagina', () => {
        cy.step('navigeer naar de documentenpagina');
        cy.visit('/documenten');
        cy.step('upload een .docx via drag and drop');
        cy.get('#pt-dragzone').selectFile('cypress/documents/docx/5.docx', {
            action: 'drag-drop',
        });
        cy.reload(); // submit action
        cy.wait(10000); // eslint-disable-line cypress/no-unnecessary-waiting
        cy.reload();
        cy.wait(10000); // eslint-disable-line cypress/no-unnecessary-waiting
        cy.reload();
        cy.step('controleer de titel van het document na conversie');
        cy.get('table')
            .find('tbody tr:first-child')
            .within(() => {
                cy.get('td:nth-child(2)').should('contain.text', 'Document 5');
            });
        cy.step('controleer de status van het document na conversie');
        cy.get('table')
            .find('tbody tr:first-child')
            .within(() => {
                cy.get('td:nth-child(5)').should('contain.text', 'Concept');
            });
        cy.step('bewerk document 5');
        cy.clickDropbutton('Document 5', 'bewerken');

        cy.contains('h1', 'Document Document 5 bewerken');

        cy.eyesCheckWindow();
    });
});
