/* eslint-disable cypress/no-unnecessary-waiting */

Cypress.on('uncaught:exception', (err) => {
    console.error('An uncaught exception occurred:', err);
    return false;
});

describe('File upload', () => {
    beforeEach(() => {
        cy.login('Maurice', '12341234');
    });

    afterEach(() => {
        cy.deleteDocuments();
    });

    it('Er kan één Docx-bestand worden geupload via bestandenkiezer', () => {
        cy.visit('/documenten');
        cy.fileUploadSelect('docx/1.docx', '1.docx', 'Document 1');
    });

    it.skip('Er kunnen meerdere Docx-bestanden worden geupload via bestandenkiezer', () => {
        cy.visit('/documenten');
        cy.fileUploadSelect('docx/2.docx', '2.docx', 'Document 2'); //hier moet nog een 3.docx bij
    });

    it('Er kan een Docx-bestand worden geupload via drag-and-drop', () => {
        cy.visit('/documenten');
        cy.fileUploadDropzone('docx/4.docx', '4.docx', 'Document 4');
    });

    it.skip('Er kunnen meerdere Docx-bestand worden geupload via drag-and-drop', () => {
        cy.visit('/documenten');
        cy.fileUploadDropzone('docx/5.docx', '5.docx', 'Document 5'); //hier moet nog een 6.docx bij
    });

    it.skip('Er kan één PDF-bestand worden geupload via bestandenkiezer', () => {
        cy.visit('/documenten');
        cy.fileUploadSelect('pdf/1.pdf', '1.pdf', 'PDF 1');
    });

    it.skip('Er kunnen meerdere PDF-bestanden worden geupload via bestandenkiezer', () => {
        cy.visit('/documenten');
        cy.fileUploadSelect('pdf/2.pdf', '2.pdf', 'PDF 2'); //hier moet nog 3.pdf bij
    });

    it.skip('Er kan een PDF-bestand worden geupload via drag-and-drop', () => {
        cy.visit('/documenten');
        cy.fileUploadSelect('pdf/4.pdf', '4.pdf', 'PDF 4');
    });
});
