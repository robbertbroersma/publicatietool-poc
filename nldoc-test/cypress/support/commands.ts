Cypress.Commands.add('login', (username, password) => {
    cy.session([username, password], () => {
        cy.visit('user/login');
        cy.get('[data-drupal-selector="edit-name"]').type(username);
        cy.get('[data-drupal-selector="edit-pass"]').type(password);
        cy.get('#edit-submit').click();
        cy.url().should('contain', 'check_logged_in=1');
    });
});

Cypress.Commands.add('deleteDocuments', () => {
    cy.step('Verwijder alle documenten');
    cy.get('.select-all > .form-checkbox').first().click({ force: true });
    cy.get('#edit-action').select('Inhoud verwijderen');
    cy.get('[data-cy="cy-form-overview-action-submit"]')
        .first()
        .click({ force: true });
    cy.get('[data-drupal-selector="edit-submit"]').click();
});

Cypress.Commands.add('clickDropbutton', (documentname, action) => {
    cy.step('Vind het dropbuttonmenu bij het juiste document');
    cy.contains('table td', `${documentname}`)
        .parent('tr')
        .within(() => {
            cy.step('Selecteer de actie uit het dropbutton-menu');
            cy.get('.views-field-dropbutton').within(() => {
                cy.contains('a', `${action}`).click({ force: true });
            });
        });
});
Cypress.Commands.add(
    'fileUploadDropzone',
    (documentPath, fileName, documentName) => {
        cy.get('#pt-dragzone').selectFile(`cypress/documents/${documentPath}`, {
            action: 'drag-drop',
        });
        cy.reload(); // submit action
        cy.wait(3000); // eslint-disable-line cypress/no-unnecessary-waiting
        cy.reload();
        cy.step('controleer de initiele titel van het document');
        cy.get('table')
            .find('tbody tr:first-child')
            .within(() => {
                cy.get('td:nth-child(2)').should('contain.text', `${fileName}`);
            });

        cy.step('controleer de initiele status van het document');
        cy.get('table')
            .find('tbody tr:first-child')
            .within(() => {
                cy.get('td:nth-child(5)').should(
                    'contain.text',
                    'Needs conversion',
                );
            });
        //cy.wait(10000); // eslint-disable-line cypress/no-unnecessary-waiting
        //cy.reload();
        //cy.step('controleer de titel van het document na conversie');
        //cy.get('table')
        //    .find('tbody tr:first-child')
        //   .within(() => {
        //       cy.get('td:nth-child(2)').should(
        //          'contain.text',
        //          `${documentName}`,
        //       );
        //   });
        //cy.step('controleer de status van het document');
        // cy.get('table')
        //   .find('tbody tr:first-child')
        //  .within(() => {
        //      cy.get('td:nth-child(5)').should('contain.text', 'Concept');
        //   });
    },
);

Cypress.Commands.add(
    'fileUploadSelect',
    (documentPath, fileName, documentName) => {
        cy.contains('.dz-button', 'Drop files here to upload').click();

        cy.get('input[type="file"]').selectFile(
            `cypress/documents/${documentPath}`,
            {
                force: true,
            },
        );
        cy.reload(); // submit action
        cy.wait(3000); // eslint-disable-line cypress/no-unnecessary-waiting
        cy.reload();
        cy.step('controleer de initiele titel van het document');
        cy.get('table')
            .find('tbody tr:first-child')
            .within(() => {
                cy.get('td:nth-child(2)').should('contain.text', `${fileName}`);
            });

        cy.step('controleer de initiele status van het document');
        cy.get('table')
            .find('tbody tr:first-child')
            .within(() => {
                cy.get('td:nth-child(5)').should(
                    'contain.text',
                    'Needs conversion',
                );
            });
        //cy.wait(10000); // eslint-disable-line cypress/no-unnecessary-waiting
        //cy.reload();
        //cy.step('controleer de titel van het document na conversie');
        //cy.get('table')
        //   .find('tbody tr:first-child')
        //  .within(() => {
        //      cy.get('td:nth-child(2)').should(
        //         'contain.text',
        //          `${documentName}`,
        //     );
        //  });
        //cy.step('controleer de status van het document');
        //cy.get('table')
        //   .find('tbody tr:first-child')
        //    .within(() => {
        //       cy.get('td:nth-child(5)').should('contain.text', 'Concept');
        //   });
    },
);
