import '@applitools/eyes-cypress';

declare global {
    // eslint-disable-next-line @typescript-eslint/no-namespace
    namespace Cypress {
        interface Chainable {
            /**
             * Custom command to select DOM element by data-cy attribute.
             * @example cy.dataCy('greeting')
             */
            login(
                username: string,
                password: string,
            ): Chainable<JQuery<HTMLElement>>;

            deleteDocuments(): Chainable<JQuery<HTMLElement>>;

            clickDropbutton(
                documentname: string,
                action: string,
            ): Chainable<JQuery<HTMLElement>>;

            fileUploadDropzone(
                documentPath: string,
                fileName: string,
                documentName: string,
            ): Chainable<JQuery<HTMLElement>>;

            fileUploadSelect(
                documentPath: string,
                fileName: string,
                documentName: string,
            ): Chainable<JQuery<HTMLElement>>;
        }
    }
}
