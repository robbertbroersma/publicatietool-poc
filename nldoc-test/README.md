# NLDoc Test suite

Cypress based automated test suite for NLDoc.

## Requirements

- Node.js
- NPM

## Install

Install Cypress and required dependencies.

```bash
npm install
```

## Run

Run all tests with the default baseUrl (https://test.toegangvooriedereen.nl)

```bash
npm run test:tst
```

Run all tests with the default baseUrl (https://staging.toegangvooriedereen.nl)

```bash
npm run test:acc
```

To run all tests on production (https://toegangvooriedereen.nl)

```bash
npm run test:prd
```

To run a test using the Cypress IDE, replace 'test' with 'start' in the NPM command. You can then select a test suite to run.
