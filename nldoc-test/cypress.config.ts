import { defineConfig } from 'cypress';
import eyesPlugin from '@applitools/eyes-cypress';
export default eyesPlugin(
    defineConfig({
        // the e2e or component configuration
        e2e: {
            setupNodeEvents(on, config) {},
        },
    }),
);
