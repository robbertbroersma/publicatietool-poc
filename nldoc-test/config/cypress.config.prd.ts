import initialize from './initialize';
import { BaseConfig } from './cypress.config.base';

import { defineConfig } from 'cypress';

const config = {
    ...BaseConfig,
    e2e: {
        ...BaseConfig.e2e,
        baseUrl: 'https://www.toegangvooriedereen.nl/',
        excludeSpecPattern: ['**/visuele_test.cy.ts'],
    },
};

export default defineConfig(config);

initialize(module);
