import initialize from './initialize';
import { BaseConfig } from './cypress.config.base';

import { defineConfig } from 'cypress';

const config = {
    ...BaseConfig,
    e2e: {
        ...BaseConfig.e2e,
        baseUrl: 'https://qa.toegangvooriedereen.nl/',
        excludeSpecPattern: [],
    },
};

export default defineConfig(config);

initialize(module);
