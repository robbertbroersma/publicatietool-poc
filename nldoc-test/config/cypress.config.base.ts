import * as dotenv from 'dotenv';

const envs = dotenv.config({ path: __dirname + '/../.env' });

export const BaseConfig = {
    e2e: {
        specPattern: 'cypress/e2e/**/*.ts',
    },
    env: {
        BASIC_AUTH: envs.parsed?.CYPRESS_BASIC_AUTH,
    },
    video: false,
};
