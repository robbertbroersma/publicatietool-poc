import * as Applitools from '@applitools/eyes-cypress';

export default function (module: NodeModule): void {
    Applitools.default(module);
}
