import { defineConfig } from 'cypress';

import { BaseConfig } from './cypress.config.base';
import initialize from './initialize';

const config = {
    ...BaseConfig,
    e2e: {
        ...BaseConfig.e2e,
        baseUrl: 'https://staging.toegangvooriedereen.nl/',
        excludeSpecPattern: ['**/visuele_test.cy.ts'],
    },
};

export default defineConfig(config);

initialize(module);
