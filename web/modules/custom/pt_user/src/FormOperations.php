<?php

declare(strict_types=1);

namespace Drupal\pt_user;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;

/**
 * Defines a class for reacting to form operations.
 *
 * @internal
 */
class FormOperations {

  use StringTranslationTrait;

  /**
   * Provide a form-specific alteration instead of the global hook_form_alter().
   *
   * Implementations are responsible for adding cache contexts/tags/max-age as
   * needed. See https://www.drupal.org/docs/8/api/cache-api/cache-api.
   *
   * Modules can implement hook_form_FORM_ID_alter() to modify a specific form,
   * rather than implementing hook_form_alter() and checking the form ID, or
   * using long switch statements to alter multiple forms.
   *
   * The call order is as follows: all existing form alter functions are called
   * for module A, then all for module B, etc., followed by all for any base
   * theme(s), and finally for the theme itself. The module order is determined
   * by system weight, then by module name.
   *
   * Within each module, form alter hooks are called in the following order:
   * first, hook_form_alter(); second, hook_form_BASE_FORM_ID_alter(); third,
   * hook_form_FORM_ID_alter(). So, for each module, the more general hooks are
   * called first followed by the more specific.
   *
   * @param array $form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form. The arguments that
   *   \Drupal::formBuilder()->getForm() was originally called with are
   *   available in the array $form_state->getBuildInfo()['args'].
   * @param string $form_id
   *   String representing the name of the form itself. Typically this is the
   *   name of the function that generated the form.
   *
   * @see hook_form_alter()
   * @see hook_form_BASE_FORM_ID_alter()
   * @see \Drupal\Core\Form\FormBuilderInterface::prepareForm()
   *
   * @ingroup form_api
   */
  public function formUserLoginFormAlter(array &$form, FormStateInterface $form_state, string $form_id) {
    // Change the 'Name' field label to 'E-mail' and set an appropriate
    // placeholder.
    if (!empty($form['name'])) {
      $form['name']['#title'] = $this->t('E-mail');
      $form['name']['#attributes']['placeholder'] = $this->t('E-mail');
    }

    // Set placeholders on the password input field.
    if (!empty($form['pass'])) {
      $form['pass']['#attributes']['placeholder'] = $this->t('Password');
    }

    // Add a 'Forgot your password' link to the login form.
    $form['actions']['forgot_password'] = [
      '#type' => 'link',
      '#title' => $this->t('Forgot your password?'),
      '#url' => Url::fromRoute('user.pass'),
      '#attributes' => [
        'class' => [
          'forgot-password-link',
        ],
      ],
    ];
  }

  /**
   * Provide a form-specific alteration instead of the global hook_form_alter().
   *
   * Implementations are responsible for adding cache contexts/tags/max-age as
   * needed. See https://www.drupal.org/docs/8/api/cache-api/cache-api.
   *
   * Modules can implement hook_form_FORM_ID_alter() to modify a specific form,
   * rather than implementing hook_form_alter() and checking the form ID, or
   * using long switch statements to alter multiple forms.
   *
   * The call order is as follows: all existing form alter functions are called
   * for module A, then all for module B, etc., followed by all for any base
   * theme(s), and finally for the theme itself. The module order is determined
   * by system weight, then by module name.
   *
   * Within each module, form alter hooks are called in the following order:
   * first, hook_form_alter(); second, hook_form_BASE_FORM_ID_alter(); third,
   * hook_form_FORM_ID_alter(). So, for each module, the more general hooks are
   * called first followed by the more specific.
   *
   * @param array $form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form. The arguments that
   *   \Drupal::formBuilder()->getForm() was originally called with are
   *   available in the array $form_state->getBuildInfo()['args'].
   * @param string $form_id
   *   String representing the name of the form itself. Typically this is the
   *   name of the function that generated the form.
   *
   * @see hook_form_alter()
   * @see hook_form_BASE_FORM_ID_alter()
   * @see \Drupal\Core\Form\FormBuilderInterface::prepareForm()
   *
   * @ingroup form_api
   */
  public function formUserPassAlter(array &$form, FormStateInterface $form_state, string $form_id) {
    $form['#tree'] = TRUE;

    // Change the form description text and move it above the e-mail input
    // field.
    if (!empty($form['mail'])) {
      $form['mail']['#weight'] = -1;
      $form['mail']['#markup'] = $this->t('Forgot your password? No problem. We will send you a link to reset your password.');
    }

    // Set a placeholder on the input field 'Name'.
    if (!empty($form['name'])) {
      $form['name']['#attributes']['placeholder'] = $this->t('E-mail');
    }
    // Add a link to the login form.
    $form['actions']['login'] = [
      '#type' => 'link',
      '#title' => $this->t('Login'),
      '#url' => Url::fromRoute('user.login'),
      '#attributes' => [
        'class' => [
          'login-link',
        ],
      ],
    ];
  }

}
