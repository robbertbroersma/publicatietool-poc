<?php

/**
 * @file
 * Update and (un)install functions for the Publicatietool validation module.
 */

declare(strict_types = 1);

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Add 'code', 'state' and 'checksum' to 'pt_doccess_validation_message' fields.
 *
 * Missing table columns are added to the database schema, to store the 'code',
 * 'state' and 'checksum' information.
 */
function pt_validation_update_10001(&$sandbox) {
  $database_schema = \Drupal::database()->schema();

  $entity_definition_update_manager = \Drupal::entityDefinitionUpdateManager();

  // First, get all 'pt_doccess_validation_message' fields, as for this field
  // type the 'code', 'state' and 'checksum' properties are added and should be
  // reflected to the database.
  $field_map = \Drupal::service('entity_field.manager')
    ->getFieldMapByFieldType('pt_doccess_validation_message');

  // Process per entity type (i.e., node, paragraph) the
  // 'pt_doccess_validation_message' fields.
  foreach ($field_map as $entity_type_id => $fields) {
    $entity_type_storage = \Drupal::entityTypeManager()
      ->getStorage($entity_type_id);

    // Database schema can only be updated, if the values are stored in the
    // database of course.
    if ($entity_type_storage instanceof SqlContentEntityStorage) {
      foreach (array_keys($fields) as $field_name) {
        $field_storage_definition = $entity_definition_update_manager->getFieldStorageDefinition($field_name, $entity_type_id);

        // All properties of the field that should keep a value.
        $field_properties = $field_storage_definition->getPropertyNames();

        // Database column definition, according to each field property.
        $field_columns = $field_storage_definition->getColumns();

        // Get the table and column names, according to the field definition.
        $table_mapping = $entity_type_storage->getTableMapping([
          $field_name => $field_storage_definition,
        ]);
        $table_names = $table_mapping->getDedicatedTableNames();
        $table_columns = $table_mapping->getColumnNames($field_name);

        // Check for each property attached to the field, if currently the
        // corresponding column exists in database. If not, create the table
        // column (according to the schema provided by the field definition).
        foreach ($table_names as $table_name) {
          foreach ($field_properties as $field_property) {
            if ($database_schema->tableExists($table_name) && !$database_schema->fieldExists($table_name, $table_columns[$field_property])) {
              $database_schema->addField($table_name, $table_columns[$field_property], $field_columns[$field_property]);
            }
          }
        }

        // Reflect any changes made to the field storage definition.
        $entity_definition_update_manager->updateFieldStorageDefinition($field_storage_definition);
      }
    }
  }
}
