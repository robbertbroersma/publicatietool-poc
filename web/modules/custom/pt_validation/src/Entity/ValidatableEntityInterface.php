<?php

declare(strict_types=1);

namespace Drupal\pt_validation\Entity;

use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Provides an interface for storing and retrieving validation information.
 */
interface ValidatableEntityInterface extends FieldableEntityInterface {

  /**
   * Returns the validation messages set on the entity.
   *
   * @return \Drupal\pt_validation\Plugin\Field\FieldType\ValidationMessageItemInterface[]
   *   Array of validation message items.
   */
  public function getValidationMessages(): array;

  /**
   * Sets the validation messages.
   *
   * @param string[] $messages
   *   An associative array containing the validation message information,
   *   having 'identifier' and (optional) 'selector' as keys.
   *
   * @return $this
   */
  public function setValidationMessages(array $messages): self;

}
