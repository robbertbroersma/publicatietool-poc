<?php

declare(strict_types=1);

namespace Drupal\pt_validation\Entity;

use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\pt_validation\Plugin\Field\FieldType\ValidationMessageItemInterface;

/**
 * Implements the interface for components that have validation messages.
 *
 * This trait implementation assumes the entity is fieldable and the validation
 * messages are stored in the field 'field_validation_messages'; otherwise, a
 * missing data exception is thrown.
 *
 * @see \Drupal\pt_validation\Entity\ValidatableEntityInterface
 * @see \Drupal\Core\Entity\FieldableEntityInterface
 */
trait ValidatableEntityTrait {

  /**
   * {@inheritdoc}
   */
  public function getValidationMessages(): array {
    if (!$this->hasField('field_validation_messages')) {
      throw new MissingDataException(sprintf("The entity (bundle) %s does not contain the field 'field_validation_messages'.", $this->bundle()));
    }

    $messages = [];
    foreach ($this->get('field_validation_messages') as $item) {
      $messages[] = $item;
    }

    return $messages;
  }

  /**
   * {@inheritdoc}
   */
  public function setValidationMessages(array $messages): self {
    if (!$this->hasField('field_validation_messages')) {
      throw new MissingDataException(sprintf("The entity (bundle) %s does not contain the field 'field_validation_messages'.", $this->bundle()));
    }

    $filtered_messages = [];

    // Map the selector and checksum information to the correct field item
    // property names.
    // @todo NLDOC-98: When the Serialization API is used, these mappings should
    //   most probably done by a (de)normalizer.
    foreach ($messages as $message) {
      if ($message['state'] !== ValidationMessageItemInterface::STATE_RESOLVED) {
        $filtered_messages[] = [
          'identifier' => $message['identifier'],
          'code' => $message['code'],
          'state' => $message['state'],
          'selector' => $message['context']['selector'],
          'checksum_hash' => $message['checksum']['hash'],
          'checksum_algorithm' => $message['checksum']['algorithm'],
        ];
      }
    }

    $this->get('field_validation_messages')->setValue($filtered_messages);

    return $this;
  }

  /**
   * Determines whether the entity has a field with the given name.
   *
   * @param string $field_name
   *   The field name.
   *
   * @return bool
   *   TRUE if the entity has a field with the given name. FALSE otherwise.
   */
  abstract public function hasField($field_name);

  /**
   * Gets the bundle of the entity.
   *
   * @return string
   *   The bundle of the entity. Defaults to the entity type ID if the entity
   *   type does not make use of different bundles.
   */
  abstract public function bundle();

  /**
   * Gets a field item list.
   *
   * @param string $field_name
   *   The name of the field to get; e.g., 'title' or 'name'.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface
   *   The field item list, containing the field items.
   *
   * @throws \InvalidArgumentException
   *   If an invalid field name is given.
   */
  abstract public function get($field_name);

}
