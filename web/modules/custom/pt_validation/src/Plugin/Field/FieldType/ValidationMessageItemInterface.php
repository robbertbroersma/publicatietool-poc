<?php

declare(strict_types=1);

namespace Drupal\pt_validation\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines an interface for the validation message field item.
 */
interface ValidationMessageItemInterface extends FieldItemInterface {

  /**
   * Represents validation messages that should be ignored.
   */
  public const STATE_IGNORED = 'ignored';

  /**
   * Represents validation messages that are resolved.
   */
  public const STATE_RESOLVED = 'resolved';

  /**
   * Represents validation messages that are unresolved.
   */
  public const STATE_UNRESOLVED = 'unresolved';

  /**
   * Represents validation messages that are informative.
   */
  public const SEVERITY_INFO = 'info';

  /**
   * Represents validation messages that are warnings.
   */
  public const SEVERITY_WARNING = 'warning';

  /**
   * Represents validation messages that are errors.
   */
  public const SEVERITY_ERROR = 'error';

  /**
   * Returns the severity level of the validation message.
   *
   * @return string|null
   *   The severity level of the validation message, NULL in case unknown.
   */
  public function getSeverity(): ?string;

  /**
   * Provides the severity label of the validation message.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The translated severity of the validation message.
   */
  public function getSeverityLabel(): TranslatableMarkup;

  /**
   * Provides the message of the validation message.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The translated message of the validation message.
   */
  public function getMessage(): TranslatableMarkup;

}
