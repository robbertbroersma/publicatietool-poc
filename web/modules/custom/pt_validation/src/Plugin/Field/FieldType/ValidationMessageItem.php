<?php

declare(strict_types=1);

namespace Drupal\pt_validation\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides a field type for a validation message.
 *
 * @FieldType(
 *   id = "pt_doccess_validation_message",
 *   label = @Translation("Validation message"),
 *   description = @Translation("A validation message."),
 *   category = @Translation("Publicatietool"),
 *   default_widget = "pt_validation_message",
 *   default_formatter = "pt_validation_message"
 * )
 *
 * @todo NLDOC-559: refactor this solution to use configuration entities to
 *   store and retrieve all data regarding validation messages. The field
 *   attached to the content entity should only contain a reference to the
 *   appropriate validation message (configuration) entity.
 */
class ValidationMessageItem extends FieldItemBase implements ValidationMessageItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];

    $properties['identifier'] = DataDefinition::create('string')
      ->setLabel(t('Identifier'))
      ->setDescription(t('The UUID of the validation information.'));

    $properties['code'] = DataDefinition::create('string')
      ->setLabel(t('Code'))
      ->setDescription(t('Indicates the type of the validation message.'));

    $properties['state'] = DataDefinition::create('string')
      ->setLabel(t('State'))
      ->setDescription(t("Indicates the state of the validation message, either 'ignored', 'resolved' or 'unresolved'."))
      ->addConstraint('AllowedValues', [
        ValidationMessageItemInterface::STATE_IGNORED,
        ValidationMessageItemInterface::STATE_RESOLVED,
        ValidationMessageItemInterface::STATE_UNRESOLVED,
      ]);

    $properties['selector'] = DataDefinition::create('string')
      ->setLabel(t('Selector'))
      ->setDescription(t('The selector defines the pattern to elements to which the validation message applies. In case the selector is empty, it applies to the complete component/entity.'));

    $properties['checksum_algorithm'] = DataDefinition::create('string')
      ->setlabel(t('Checksum algorithm'))
      ->setDescription(t('The algorithm used to generate the checksum.'));

    $properties['checksum_hash'] = DataDefinition::create('string')
      ->setLabel(t('Checksum hash'))
      ->setDescription(t('The hash of the validation message.'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'identifier' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'code' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'state' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'selector' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'checksum_algorithm' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'checksum_hash' => [
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $identifier = $this->get('identifier')->getValue();
    return $identifier === NULL || $identifier === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'identifier';
  }

  /**
   * {@inheritdoc}
   */
  public function getSeverity(): ?string {
    return match ($this->get('code')->getValue()) {
      'attribute-alt-required-on-element', 'attribute-alt-required-on-html-element', 'paragraph-should-have-proper-interpunction', 'sentence-should-start-with-capital', 'table-is-single-row', 'first-heading-is-same-as-title' => ValidationMessageItemInterface::SEVERITY_WARNING,
      default => NULL,
    };
  }

  /**
   * {@inheritdoc}
   */
  public function getSeverityLabel(): TranslatableMarkup {
    return match ($this->getSeverity()) {
      ValidationMessageItemInterface::SEVERITY_INFO => $this->t('Suggestion'),
      ValidationMessageItemInterface::SEVERITY_WARNING => $this->t('Warning'),
      ValidationMessageItemInterface::SEVERITY_ERROR => $this->t('Error'),
      default => $this->t('Unknown'),
    };
  }

  /**
   * {@inheritdoc}
   */
  public function getMessage(): TranslatableMarkup {
    return match ($this->get('code')->getValue()) {
      'attribute-alt-required-on-element' => $this->t('Attribute alt is required for non-decorative elements.'),
      'attribute-alt-required-on-html-element' => $this->t('Attribute alt is required for non-decorative images.'),
      'paragraph-should-have-proper-interpunction' => $this->t('Please make use of interpunction at the end of every paragraph.'),
      'sentence-should-start-with-capital' => $this->t('Sentence should start with capital letter.'),
      'table-is-single-row' => $this->t('This table is a single row table. Please consider using a heading or a text paragraph.'),
      'first-heading-is-same-as-title' => $this->t('The first heading is the same as the title of the document.'),
      default => $this->t('Unknown'),
    };
  }

}
