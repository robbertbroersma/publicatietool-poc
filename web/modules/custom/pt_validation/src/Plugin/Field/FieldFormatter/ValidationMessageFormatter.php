<?php

declare(strict_types=1);

namespace Drupal\pt_validation\Plugin\Field\FieldFormatter;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'pt_validation_message' formatter.
 *
 * @FieldFormatter(
 *   id = "pt_validation_message",
 *   label = @Translation("Validation message"),
 *   field_types = {
 *     "pt_doccess_validation_message"
 *   }
 * )
 */
class ValidationMessageFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      /** @var \Drupal\pt_validation\Plugin\Field\FieldType\ValidationMessageItemInterface $item */

      $elements[$delta]['validation_message'] = [
        '#theme' => 'pt_validation_message',
        '#code' => $item->get('code')->getValue(),
        '#severity' => $item->getSeverity(),
        '#severity_label' => $item->getSeverityLabel(),
        '#message' => $item->getMessage(),
        '#selector' => $item->get('selector')->getValue(),
      ];

      // Create the URL for the assist link based on the identifier.
      $url = Url::fromRoute('pt_assist.assist_form_' . $item->get('code')->getValue(), [
        'paragraphs_item_id' => $item->getEntity()->id(),
      ]);

      // Show the assist link only when the route exists.
      // @todo NLDOC-779: access check should be obsolete when a controller
      //   handles the route (i.e., provides the correct form).
      if ($url->access()) {
        $elements[$delta]['assistant_link'] = [
          '#type' => 'link',
          '#title' => $this->t('Review issues'),
          '#url' => $url,
          '#attributes' => [
            'class' => ['use-ajax'],
            'data-dialog-type' => 'modal',
            'data-dialog-options' => Json::encode([
              'width' => 800,
              'classes' => ['ui-dialog' => ["ui-dialog-warning"]],
            ]),
          ],
          '#attached' => [
            'library' => ['core/drupal.dialog.ajax'],
          ],
        ];
      }
    }

    return $elements;
  }

}
