<?php

declare(strict_types=1);

namespace Drupal\pt_validation\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\pt_validation\Plugin\Field\FieldType\ValidationMessageItemInterface;

/**
 * Plugin implementation of the 'pt_validation_message_widget' widget.
 *
 * This is a simple widget, containing only two fields: identifier and selector.
 * In normal circumstances, the field is not editable by users and only set when
 * processing results of the Document Conversion System.
 *
 * @FieldWidget(
 *   id = "pt_validation_message",
 *   label = @Translation("Validation message"),
 *   field_types = {
 *     "pt_doccess_validation_message"
 *   }
 * )
 */
class ValidationMessageWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['identifier'] = [
      '#type' => 'item',
      '#title' => $this->t('Identifier'),
      '#plain_text' => $items[$delta]->identifier ?? $this->t('Unknown'),
    ];

    $element['code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('code'),
      '#default_value' => $items[$delta]->code ?? '',
      '#required' => TRUE,
    ];

    $element['state'] = [
      '#type' => 'select',
      '#title' => $this->t('State'),
      '#options' => [
        ValidationMessageItemInterface::STATE_IGNORED => $this->t('Ignored'),
        ValidationMessageItemInterface::STATE_RESOLVED => $this->t('Resolved'),
        ValidationMessageItemInterface::STATE_UNRESOLVED => $this->t('Unresolved'),
      ],
      '#default_value' => $items[$delta]->state ?? '',
      '#required' => TRUE,
    ];

    $element['selector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Selector'),
      '#default_value' => $items[$delta]->selector ?? '',
    ];

    $element['checksum_algorithm'] = [
      '#type' => 'item',
      '#title' => $this->t('Checksum algorithm'),
      '#plain_text' => $items[$delta]->checksum_algorithm ?? $this->t('Unknown'),
    ];

    $element['checksum_hash'] = [
      '#type' => 'item',
      '#title' => $this->t('Checksum hash'),
      '#plain_text' => $items[$delta]->checksum_hash ?? $this->t('Unknown'),
    ];

    return $element;
  }

}
