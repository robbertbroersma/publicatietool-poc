<?php

declare(strict_types=1);

namespace Drupal\pt_validation\Service;

/**
 * Provides the client to communicate with the Accessibility Validation API.
 */
interface ValidationClientInterface {

  /**
   * Uses the API to validate the accessibility of a document.
   *
   * @param string $document_structure
   *   The JSON string describing the document structure to validate.
   *
   * @return string|bool
   *   JSON encoded string containing the validated document structure. In case
   *   of an error, returns FALSE.
   */
  public function validateAccessibility(string $document_structure): string|bool;

}
