<?php

declare(strict_types=1);

namespace Drupal\pt_validation\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;
use Psr\Log\LoggerInterface;

/**
 * Provides the client to communicate with the Accessibility Validation API.
 */
class ValidationClient implements ValidationClientInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $httpClient;

  /**
   * Settings for this module.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The logger object, that writes to the specific channel of this module.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Constructs a ValidationClient object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger object, that writes to the specific channel of this module.
   */
  public function __construct(ClientInterface $http_client, ConfigFactoryInterface $config_factory, LoggerInterface $logger) {
    $this->httpClient = $http_client;
    $this->config = $config_factory->get('pt_validation.settings');
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function validateAccessibility(string $document_structure): string|bool {
    try {
      $request = $this->httpClient->request('POST', $this->config->get('url'), [
        RequestOptions::HEADERS => [
          'Content-Type' => 'application/vnd.nldoc-elementstructure+json',
        ],
        RequestOptions::BODY => $document_structure,
      ]);
    }
    catch (\Exception $e) {
      $this->logger->error('Error occurred when trying to validate a document structure: @message.', [
        '@message' => $e->getMessage(),
      ]);

      return FALSE;
    }

    // Log an error and return false if the response is not OK.
    if ($request->getStatusCode() !== 200) {
      $this->logger->error('Status code @code return when validating a document structure: @message.', [
        '@code' => $request->getStatusCode(),
        '@message' => $request->getBody()->getContents(),
      ]);

      return FALSE;
    }

    return $request->getBody()->getContents();
  }

}
