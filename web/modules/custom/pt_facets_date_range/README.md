# PT Facets Date Range Picker

The PT Facets Date Range Picker provides a widget for facets to pick a 
date range. The following ranges are supported:

- Today
- Last 7 days
- Last Month
- Last Year


## Table of contents

- Requirements
- Configuration


## Requirements

The module is tested with Search API and Search API Solr.

- [Search API](https://www.drupal.org/project/search_api)
- ([Search API Solr](https://www.drupal.org/project/search_api_solr))
- [Facets](https://www.drupal.org/project/facets)

This processor and query type expects the selected field to be indexed as
timestamp or this module will not work.


## Configuration

1. Add the date range field values to the search index
    1. The start date range as a string (timestamp)
    2. The end date range as a string (timestamp)
2. Add a facet and enable the Date Range Picker
   1. Select the start date field
   2. Select the end date field
