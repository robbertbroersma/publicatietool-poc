<?php

namespace Drupal\pt_facets_date_range\Plugin\facets\query_type;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\facets\Result\Result;

/**
 * Support date range facets with the Search API scope.
 *
 * @FacetsQueryType(
 *   id = "search_api_date_range_picker",
 *   label = @Translation("Date Range Picker"),
 * )
 */
class SearchApiDateRangePicker extends SearchApiDateRangePickerBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, 'pt_date_range_picker');

    $configuration = $this->getConfiguration();
    $configuration['filter_default'] = $this->dateProcessorConfig['filter_default'];
    $configuration['options'] = $this->dateProcessorConfig['options'];
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): void {
    $query = $this->query;

    $fieldIdentifier = $this->facet->getFieldIdentifier();

    $options = &$query->getOptions();
    $options['search_api_facets'][$fieldIdentifier] = $this->getFacetOptions();

    $operator = $this->facet->getQueryOperator();
    $exclude = $this->facet->getExclude();
    $filter = $query->createConditionGroup($operator, ['facet:' . $fieldIdentifier]);

    $activeItems = $this->facet->getActiveItems();
    if (count($activeItems)) {
      foreach ($activeItems as $activeItem) {
        $this->addConditionGroupsByActiveItem($query, $filter, $exclude, $activeItem, $fieldIdentifier);
      }
    }
    elseif ($this->getFilterDefault()) {
      $range = $this->calculateDefault();

      $conjunction = $exclude ? 'OR' : 'AND';
      $itemFilter = $query->createConditionGroup($conjunction, ['facet:' . $fieldIdentifier]);
      $itemFilter->addCondition($this->facet->getFieldIdentifier(), $range['start'], $exclude ? '<' : '>=');
      $filter->addConditionGroup($itemFilter);
    }
    $query->addConditionGroup($filter);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateRange($value): array {
    if (!in_array($value, $this->allowedValues())) {
      return [];
    }

    switch ($value) {
      case 'last_seven_days':
        $endDate = new DrupalDateTime('now', 'UTC');
        $startDate = new DrupalDateTime('-7 days', 'UTC');
        break;

      case 'last_month':
        $endDate = new DrupalDateTime('now', 'UTC');
        $startDate = new DrupalDateTime('-1 months', 'UTC');
        break;

      case 'last_year':
        $endDate = new DrupalDateTime('now', 'UTC');
        $startDate = new DrupalDateTime('-1 year', 'UTC');
        break;

      default:
      case 'today':
        $startDate = $endDate = new DrupalDateTime('now', 'UTC');
    }

    $dateTime = (new DrupalDateTime('now', 'UTC'))
      ->setTime(0, 0);

    $startDate = $dateTime::createFromFormat('Y-m-d\TH:i:s', $startDate->format('Y-m-d') . 'T00:00:00');
    $stopDate = $dateTime::createFromFormat('Y-m-d\TH:i:s', $endDate->format('Y-m-d') . 'T23:59:59');

    return [
      'start' => $startDate->format('U'),
      'stop' => $stopDate->format('U'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    if (empty($this->results)) {
      return $this->facet;
    }

    $results = [];
    foreach ($this->getOptions() as $key => $option) {
      if ($option) {
        $resultFilter = $this->calculateResultFilter($option);
        $results[$key] = new Result($this->facet, $resultFilter['raw'], $resultFilter['display'], 0);
      }
    }
    $this->facet->setResults($results);

    return $this->facet;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateResultFilter($value): array {
    switch ($value) {
      case 'last_seven_days':
        $display = $this->t('Last 7 days');
        break;

      case 'last_month':
        $display = $this->t('Last month');
        break;

      case 'last_year':
        $display = $this->t('Last year');
        break;

      default:
      case 'today':
        $display = $this->t('Today');
    }

    return [
      'display' => $display,
      'raw' => $value,
    ];
  }

  /**
   * Allowed values.
   */
  protected function allowedValues(): array {
    return [
      'today',
      'last_seven_days',
      'last_month',
      'last_year',
    ];
  }

  /**
   * Get the filter default.
   */
  protected function getFilterDefault(): bool {
    return $this->getConfiguration()['filter_default'];
  }

  /**
   * Get possible options.
   */
  protected function getOptions(): array {
    return $this->getConfiguration()['options'];
  }

}
