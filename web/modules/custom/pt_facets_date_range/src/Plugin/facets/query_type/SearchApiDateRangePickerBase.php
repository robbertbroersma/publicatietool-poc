<?php

namespace Drupal\pt_facets_date_range\Plugin\facets\query_type;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\facets\QueryType\QueryTypeRangeBase;
use Drupal\search_api\Query\ConditionGroupInterface;
use Drupal\search_api\Query\QueryInterface;

/**
 * Provides the base class for the date (range) picker.
 */
class SearchApiDateRangePickerBase extends QueryTypeRangeBase {
  /**
   * Stores the dateProcessorConfig.
   *
   * @var array
   */
  protected array $dateProcessorConfig;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, $processor_id) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $facet = $configuration['facet'];
    $processors = $facet->getProcessors();
    $this->dateProcessorConfig = $processors[$processor_id]->getConfiguration();

    $configuration = $this->getConfiguration();
    $configuration['start_date'] = $this->dateProcessorConfig['start_date'];
    $configuration['end_date'] = $this->dateProcessorConfig['end_date'];

    $this->setConfiguration($configuration);
  }

  /**
   * Add item condition filter.
   */
  protected function addConditionGroupsByActiveItem(QueryInterface $query, ConditionGroupInterface $filter, bool $exclude, string $activeItem, string $fieldIdentifier): void {
    $endDate = $this->getEndDate();
    $range = $this->calculateRange($activeItem);

    $itemFilter = $query->createConditionGroup('AND', ['facet:' . $fieldIdentifier]);
    $itemFilter->addCondition($endDate, $range['start'], $exclude ? '<' : '>=');
    $itemFilter->addCondition($endDate, $range['stop'], $exclude ? '>' : '<=');

    $filter->addConditionGroup($itemFilter);
  }

  /**
   * Calculate default filter.
   */
  protected function calculateDefault(): array {
    $dateTime = new DrupalDateTime();
    $date = new DrupalDateTime('now');

    return [
      'start' => $dateTime::createFromFormat(
        'Y-m-d\TH:i:s',
        $date->format('Y-m-d\TH:i:s')
      )->format('U'),
    ];
  }

  /**
   * Get end date.
   */
  protected function getEndDate(): string {
    return $this->getConfiguration()['end_date'];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateRange($value): array {
    $dateTime = new DrupalDateTime('now', 'UTC');

    $startDate = $dateTime::createFromFormat('Y-m-d\TH:i:s', $value . 'T00:00:00');
    $stopDate = $dateTime::createFromFormat('Y-m-d\TH:i:s', $value . 'T23:59:59');

    return [
      'start' => $startDate->format('U'),
      'stop' => $stopDate->format('U'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateResultFilter($value): array {
    return [
      'display' => $this->t('Date'),
      'raw' => $value,
    ];
  }

}
