<?php

namespace Drupal\pt_facets_date_range\Plugin\facets\query_type;

/**
 * Support for date range facets within the Search API scope.
 *
 * This query type supports dates for all possible backends. This specific
 * implementation of the query type supports a generic solution of adding facets
 * for dates.
 *
 * If you want to have a specific solution for your backend / module to
 * implement dates, you can alter the ::getQueryTypesForDataType method on the
 * backendPlugin to return a different class.
 *
 * @FacetsQueryType(
 *   id = "search_api_date_picker",
 *   label = @Translation("Datepicker"),
 * )
 */
class SearchApiDatePicker extends SearchApiDateRangePickerBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, 'date_picker');
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): void {
    $query = $this->query;

    $fieldIdentifier = $this->facet->getFieldIdentifier();

    $options = &$query->getOptions();
    $options['search_api_facets'][$fieldIdentifier] = $this->getFacetOptions();

    $operator = $this->facet->getQueryOperator();
    $exclude = $this->facet->getExclude();
    $filter = $query->createConditionGroup($operator, ['facet:' . $fieldIdentifier]);

    $activeItems = $this->facet->getActiveItems();
    if (empty($activeItems)) {
      return;
    }

    foreach ($activeItems as $activeItem) {
      $this->addConditionGroupsByActiveItem($query, $filter, $exclude, $activeItem, $fieldIdentifier);
    }
    $query->addConditionGroup($filter);
  }

}
