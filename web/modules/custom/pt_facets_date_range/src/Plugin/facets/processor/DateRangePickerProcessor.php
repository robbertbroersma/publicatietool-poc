<?php

namespace Drupal\pt_facets_date_range\Plugin\facets\processor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\facets\FacetInterface;

/**
 * Processes the date range picker input.
 *
 * @FacetsProcessor(
 *   id = "pt_date_range_picker",
 *   label = @Translation("Date Range Picker"),
 *   description = @Translation("Display range picker options for date fields."),
 *   stages = {
 *     "build" = 35
 *   }
 * )
 */
class DateRangePickerProcessor extends DatePickerProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet): array {
    $build = parent::buildConfigurationForm($form, $form_state, $facet);

    $this->getConfiguration();

    $build['filter_default'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Filter default'),
      '#default_value' => $this->getConfiguration()['filter_default'],
      '#description' => $this->t('Set the default filter to start from today when no option is selected.'),
    ];

    $build['options'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Options'),
      '#default_value' => $this->getConfiguration()['options'],
      '#options' => [
        'today' => $this->t('Today'),
        'last_seven_days' => $this->t('Last 7 days'),
        'last_month' => $this->t('Last month'),
        'last_year' => $this->t('Last year'),
      ],
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryType(): ?string {
    return 'pt_date_range_picker';
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    $config = parent::defaultConfiguration();
    $config['filter_default'] = 0;
    $config['options'] = [];

    return $config;
  }

}
