<?php

namespace Drupal\pt_facets_date_range\Plugin\facets\processor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\facets\FacetInterface;
use Drupal\facets\Processor\BuildProcessorInterface;
use Drupal\facets\Processor\ProcessorPluginBase;

/**
 * The DatePickerProcessor base class.
 */
class DatePickerProcessorBase extends ProcessorPluginBase implements BuildProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet, array $results): array {
    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet): array {
    $this->getConfiguration();

    $build = [];
    $build['start_date'] = [
      '#type' => 'select',
      '#title' => $this->t('Start date field'),
      '#options' => $facet->getFacetSource()->getFields(),
      '#default_value' => $this->getConfiguration()['start_date'],
      '#description' => $this->t('The field should be a timestamp field.'),
      '#required' => TRUE,
    ];

    $build['end_date'] = [
      '#type' => 'select',
      '#title' => $this->t('End date field'),
      '#options' => $facet->getFacetSource()->getFields(),
      '#default_value' => $this->getConfiguration()['end_date'],
      '#description' => $this->t('The field should be a timestamp field.'),
      '#required' => TRUE,
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'start_date' => '',
      'end_date' => '',
    ];
  }

}
