<?php

namespace Drupal\pt_facets_date_range\Plugin\facets\processor;

/**
 * Processes the date range picker input.
 *
 * @FacetsProcessor(
 *   id = "date_picker",
 *   label = @Translation("Date Picker"),
 *   description = @Translation("Display picker options for date fields."),
 *   stages = {
 *     "build" = 35
 *   }
 * )
 */
class DatePickerProcessor extends DatePickerProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function getQueryType(): ?string {
    return 'date_picker';
  }

}
