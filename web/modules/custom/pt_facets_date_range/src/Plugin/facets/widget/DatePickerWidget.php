<?php

namespace Drupal\pt_facets_date_range\Plugin\facets\widget;

use Drupal\facets\FacetInterface;
use Drupal\facets\Widget\WidgetPluginBase;

/**
 * The datepicker widget.
 *
 * @FacetsWidget(
 *   id = "pt_facets_date_range_datepicker",
 *   label = @Translation("Datepicker"),
 *   description = @Translation("A widget that shows a datepicker."),
 * )
 */
class DatePickerWidget extends WidgetPluginBase {

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet): array {
    $build = parent::build($facet);

    $active = $facet->getActiveItems();
    $default = $active[0] ?? '';

    $build['#items'] = [];
    $build['#items']['date'] = [
      [
        '#type' => 'date',
        '#title' => $this
          ->t('Select a date'),
        '#default_value' => $default,
        '#attributes' => [
          'type' => 'date',
          'class' => ['facet-datepicker'],
          'id' => $facet->id(),
          'name' => $facet->id(),
          'data-type' => 'datepicker',
          'value' => $default,
        ],
      ],
    ];

    $build['#attached']['library'][] = 'facets/widget';
    $build['#attached']['library'][] = 'pt_facets_date_range/datepicker';

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function isPropertyRequired($name, $type): bool {
    if (('date_picker' === $name) && 'processors' === $type) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryType(): ?string {
    return 'date_picker';
  }

}
