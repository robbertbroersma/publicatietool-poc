/**
 * @file
 * Provides JavaScript behaviors for the Publicatietool clipboard module.
 */

(function (Drupal, drupalSettings, once) {

  "use strict";

  /**
   * Attaches the copy-to-clipboard functionality.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the functionality to copy content to the clipboard.
   */
  Drupal.behaviors.ptClipboard = {
    attach: function (context) {
      // Select all copy-to-clipboard buttons, once.
      const buttons = once('pt-clipboard', '.pt-clipboard__button', context);

      // Add a click event listener to each button, which copies the provided
      // content to the clipboard when pressed.
      buttons.forEach(function (button) {
        button.addEventListener('click', async function(event) {
          // Prevent the default behavior.
          event.preventDefault();

          // Copy the value belonging to the ID of this button to the clipboard.
          await navigator.clipboard.writeText(drupalSettings.ptClipboard.value[button.id]);
        });
      });
    }
  };

})(Drupal, drupalSettings, once);
