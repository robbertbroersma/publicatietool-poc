INTRODUCTION
------------

Provides a JavaScript library to copy contents to the clipboard. This
functionality can be added by using the `pt_clipboard` theme hook, e.g.,

```php
  $form['copy_to_clipboard'] = [
    '#theme' => 'pt_clipboard',
    '#label' => t('Copy to clipboard'),
    '#value' => 'This is the text to copy',
  ];
```

Above example will add a button with the label "Copy to clipboard" and when
clicked, the text "This is the text to copy" will be copied to the clipboard.
