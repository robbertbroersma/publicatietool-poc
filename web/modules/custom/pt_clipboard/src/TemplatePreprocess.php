<?php

declare(strict_types=1);

namespace Drupal\pt_clipboard;

use Drupal\Component\Utility\Html;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\TypedData\Exception\MissingDataException;

/**
 * Defines a class to define template preprocessors.
 *
 * @internal
 */
class TemplatePreprocess {

  use StringTranslationTrait;

  /**
   * Prepares variables for the pt_clipboard template.
   *
   * Default template: pt_clipboard.html.twig.
   *
   * @param array $variables
   *   An associative array containing:
   *   - label: The label of the button, to copy contents to the clipboard.
   *   - value: The value to copy to the clipboard.
   *   - attributes: An associative array of HTML attributes to apply to the
   *     clipboard button.
   */
  public function templatePreprocessPtClipboard(array &$variables) {
    // No need to proceed further if the value is not set.
    if (!isset($variables['value'])) {
      throw new MissingDataException('Missing value to copy to the clipboard.');
    }

    // Set the default label, in case no one is provided.
    if (empty($variables['label'])) {
      $variables['label'] = $this->t('Copy to clipboard');
    }

    // Generate a unique ID for the copy-to-clipboard button, this to be able
    // to match the correct value to copy with the correct button (in case
    // multiple buttons are attached to the same page).
    $id = Html::getUniqueId('pt-clipboard');

    // Set the appropriate class for the copy-to-clipboard button.
    $variables['attributes']['id'] = $id;
    $variables['attributes']['class'][] = 'pt-clipboard__button';

    // Attach the clipboard library and provide the value to copy to the
    // clipboard, mapped with id of the button.
    $variables['#attached']['library'][] = 'pt_clipboard/pt-clipboard';
    $variables['#attached']['drupalSettings']['ptClipboard']['value'][$id] = $variables['value'];
    unset($variables['value']);
  }

}
