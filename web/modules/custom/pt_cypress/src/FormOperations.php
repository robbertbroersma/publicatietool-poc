<?php

declare(strict_types=1);

namespace Drupal\pt_cypress;

use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a class for reacting to form operations.
 *
 * @internal
 */
class FormOperations {

  /**
   * Provide a form-specific alteration instead of the global hook_form_alter().
   *
   * Implementations are responsible for adding cache contexts/tags/max-age as
   * needed. See https://www.drupal.org/docs/8/api/cache-api/cache-api.
   *
   * Modules can implement hook_form_FORM_ID_alter() to modify a specific form,
   * rather than implementing hook_form_alter() and checking the form ID, or
   * using long switch statements to alter multiple forms.
   *
   * The call order is as follows: all existing form alter functions are called
   * for module A, then all for module B, etc., followed by all for any base
   * theme(s), and finally for the theme itself. The module order is determined
   * by system weight, then by module name.
   *
   * Within each module, form alter hooks are called in the following order:
   * first, hook_form_alter(); second, hook_form_BASE_FORM_ID_alter(); third,
   * hook_form_FORM_ID_alter(). So, for each module, the more general hooks are
   * called first followed by the more specific.
   *
   * @param array $form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form. The arguments that
   *   \Drupal::formBuilder()->getForm() was originally called with are
   *   available in the array $form_state->getBuildInfo()['args'].
   * @param string $form_id
   *   String representing the name of the form itself. Typically this is the
   *   name of the function that generated the form.
   *
   * @see hook_form_alter()
   * @see hook_form_BASE_FORM_ID_alter()
   * @see \Drupal\Core\Form\FormBuilderInterface::prepareForm()
   *
   * @ingroup form_api
   */
  public function formPtDoccessInitialContentFileUploadForm(array &$form, FormStateInterface $form_state, string $form_id) {
    // @todo NLDOC-899: Adjust below to use data-cy attributes for the
    //   drag-and-drop functionality.
    // A class attribute is added to the file upload field, since extra
    // data-cy attributes are filtered out from the file input element.
    if (!empty($form['file'])) {
      $form['file']['#attributes']['class'][] = 'cy-file-upload-button-select';
    }

    if (!empty($form['actions']['submit'])) {
      $form['actions']['submit']['#attributes']['data-cy'][] = 'file-upload-button-submit';
    }
  }

  /**
   * Provide a form-specific alteration instead of the global hook_form_alter().
   *
   * Implementations are responsible for adding cache contexts/tags/max-age as
   * needed. See https://www.drupal.org/docs/8/api/cache-api/cache-api.
   *
   * Modules can implement hook_form_FORM_ID_alter() to modify a specific form,
   * rather than implementing hook_form_alter() and checking the form ID, or
   * using long switch statements to alter multiple forms.
   *
   * The call order is as follows: all existing form alter functions are called
   * for module A, then all for module B, etc., followed by all for any base
   * theme(s), and finally for the theme itself. The module order is determined
   * by system weight, then by module name.
   *
   * Within each module, form alter hooks are called in the following order:
   * first, hook_form_alter(); second, hook_form_BASE_FORM_ID_alter(); third,
   * hook_form_FORM_ID_alter(). So, for each module, the more general hooks are
   * called first followed by the more specific.
   *
   * @param array $form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form. The arguments that
   *   \Drupal::formBuilder()->getForm() was originally called with are
   *   available in the array $form_state->getBuildInfo()['args'].
   * @param string $form_id
   *   String representing the name of the form itself. Typically this is the
   *   name of the function that generated the form.
   *
   * @see hook_form_alter()
   * @see hook_form_BASE_FORM_ID_alter()
   * @see \Drupal\Core\Form\FormBuilderInterface::prepareForm()
   *
   * @ingroup form_api
   */
  public function formViewsExposedFormAlter(array &$form, FormStateInterface $form_state, string $form_id) {
    if (!empty($form['actions']['submit'])) {
      $form['actions']['submit']['#attributes']['data-cy'][] = 'cy-form-overview-filter-submit';
    }
  }

  /**
   * Provide a form-specific alteration instead of the global hook_form_alter().
   *
   * Implementations are responsible for adding cache contexts/tags/max-age as
   * needed. See https://www.drupal.org/docs/8/api/cache-api/cache-api.
   *
   * Modules can implement hook_form_FORM_ID_alter() to modify a specific form,
   * rather than implementing hook_form_alter() and checking the form ID, or
   * using long switch statements to alter multiple forms.
   *
   * The call order is as follows: all existing form alter functions are called
   * for module A, then all for module B, etc., followed by all for any base
   * theme(s), and finally for the theme itself. The module order is determined
   * by system weight, then by module name.
   *
   * Within each module, form alter hooks are called in the following order:
   * first, hook_form_alter(); second, hook_form_BASE_FORM_ID_alter(); third,
   * hook_form_FORM_ID_alter(). So, for each module, the more general hooks are
   * called first followed by the more specific.
   *
   * @param array $form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form. The arguments that
   *   \Drupal::formBuilder()->getForm() was originally called with are
   *   available in the array $form_state->getBuildInfo()['args'].
   * @param string $form_id
   *   String representing the name of the form itself. Typically this is the
   *   name of the function that generated the form.
   *
   * @see hook_form_alter()
   * @see hook_form_BASE_FORM_ID_alter()
   * @see \Drupal\Core\Form\FormBuilderInterface::prepareForm()
   *
   * @ingroup form_api
   */
  public function formViewsFormDocumentsPage(array &$form, FormStateInterface $form_state, string $form_id) {
    if (!empty($form['header']['node_bulk_form']['actions']['submit'])) {
      $form['header']['node_bulk_form']['actions']['submit']['#attributes']['data-cy'][] = 'cy-form-overview-action-submit';
    }

    if (!empty($form['actions']['submit'])) {
      $form['actions']['submit']['#attributes']['data-cy'][] = 'cy-form-overview-action-submit';
    }
  }

}
