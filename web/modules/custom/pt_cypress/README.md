INTRODUCTION
------------

The Publicatietool Cypress module adds functionality to ease the testing process
using [Cypress](https://www.cypress.io/). It mainly alters forms by adding extra
`cy-data` attributes to elements.
