<?php

declare(strict_types=1);

namespace Drupal\pt_corporate_identity\Entity;

use Drupal\Core\TypedData\Exception\MissingDataException;

/**
 * Implements the interface for entities that have a corporate identity.
 *
 * This trait implementation assumes the entity is fieldable and the corporate
 * identity is stored in the field 'field_corporate_identity'; otherwise, a
 * missing data exception is thrown.
 *
 * @see \Drupal\pt_corporate_identity\Entity\CorporateIdentityInterface
 * @see \Drupal\Core\Entity\FieldableEntityInterface
 */
trait CorporateIdentityTrait {

  /**
   * {@inheritdoc}
   */
  public function getCorporateIdentity(): string {
    if (!$this->hasField('field_corporate_identity')) {
      throw new MissingDataException(sprintf("The entity (bundle) %s does not contain the field 'field_corporate_identity'.", $this->bundle()));
    }

    return $this->get('field_corporate_identity')->value ?? CorporateIdentityInterface::DEFAULT_CORPORATE_IDENTITY;
  }

  /**
   * Determines whether the entity has a field with the given name.
   *
   * @param string $field_name
   *   The field name.
   *
   * @return bool
   *   TRUE if the entity has a field with the given name. FALSE otherwise.
   */
  abstract public function hasField($field_name);

  /**
   * Gets the bundle of the entity.
   *
   * @return string
   *   The bundle of the entity. Defaults to the entity type ID if the entity
   *   type does not make use of different bundles.
   */
  abstract public function bundle();

  /**
   * Gets a field item list.
   *
   * @param string $field_name
   *   The name of the field to get; e.g., 'title' or 'name'.
   *
   * @return \Drupal\Core\Field\FieldItemListInterface
   *   The field item list, containing the field items.
   *
   * @throws \InvalidArgumentException
   *   If an invalid field name is given.
   */
  abstract public function get($field_name);

}
