<?php

declare(strict_types=1);

namespace Drupal\pt_corporate_identity\Entity;

use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Provides an interface to determine the corporate identity of an entity.
 */
interface CorporateIdentityInterface extends FieldableEntityInterface {

  /**
   * Default value in case the corporate identity is not set.
   */
  public const DEFAULT_CORPORATE_IDENTITY = 'default';

  /**
   * Gets the corporate identity the entity belongs to.
   *
   * The corporate identity is mainly used to determine which logo, font and
   * colors to use, when displaying the contents.
   *
   * @return string
   *   The corporate identity, or 'default' if none is set.
   */
  public function getCorporateIdentity(): string;

}
