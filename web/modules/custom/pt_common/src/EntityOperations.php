<?php

declare(strict_types=1);

namespace Drupal\pt_common;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class for reacting to entity events.
 *
 * @internal
 */
class EntityOperations implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a new EntityOperations object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, TranslationInterface $string_translation) {
    $this->entityTypeManager = $entity_type_manager;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('string_translation')
    );
  }

  /**
   * Provide a form-specific alteration for shared ('base') forms.
   *
   * Implementations are responsible for adding cache contexts/tags/max-age as
   * needed. See https://www.drupal.org/docs/8/api/cache-api/cache-api.
   *
   * By default, when \Drupal::formBuilder()->getForm() is called, Drupal looks
   * for a function with the same name as the form ID, and uses that function to
   * build the form. In contrast, base forms allow multiple form IDs to be
   * mapped to a single base (also called 'factory') form function.
   *
   * Modules can implement hook_form_BASE_FORM_ID_alter() to modify a specific
   * base form, rather than implementing hook_form_alter() and checking for
   * conditions that would identify the shared form constructor.
   *
   * To identify the base form ID for a particular form (or to determine whether
   * one exists) check the $form_state. The base form ID is stored under
   * $form_state->getBuildInfo()['base_form_id'].
   *
   * The call order is as follows: all existing form alter functions are called
   * for module A, then all for module B, etc., followed by all for any base
   * theme(s), and finally for the theme itself. The module order is determined
   * by system weight, then by module name.
   *
   * Within each module, form alter hooks are called in the following order:
   * first, hook_form_alter(); second, hook_form_BASE_FORM_ID_alter(); third,
   * hook_form_FORM_ID_alter(). So, for each module, the more general hooks are
   * called first followed by the more specific.
   *
   * @param array $form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $form_id
   *   String representing the name of the form itself. Typically this is the
   *   name of the function that generated the form.
   *
   * @see hook_form_alter()
   * @see hook_form_FORM_ID_alter()
   * @see \Drupal\Core\Form\FormBuilderInterface::prepareForm()
   *
   * @ingroup form_api
   */
  public function nodeFormAlter(array &$form, FormStateInterface $form_state, string $form_id) {
    /** @var \Drupal\node\NodeInterface $node */
    $node = $form_state->getFormObject()->getEntity();

    /** @var \Drupal\node\NodeTypeInterface $type */
    $type = $this->entityTypeManager->getStorage('node_type')->load($node->bundle());

    // Set the contents for the pseudo-field, that is registered in
    // \Drupal\pt_common\EntityOperations::entityExtraFieldInfo().
    $form['pt_common_bundle'] = [
      '#theme' => 'pt_common_bundle',
      '#bundle' => $type->label(),
    ];
  }

}
