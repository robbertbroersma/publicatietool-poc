INTRODUCTION
------------

This is a helper module that mainly provides functionality needed by (multiple)
custom modules.

MODULE CONFIGURATION FORMS
--------------------------

The configuration form of custom modules, ideally, should have the route
`/admin/config/publicatietool/<module_name>` and add a menu link (in the
`.links.menu.yml` file) having the property
`parent: pt_common.admin_config_publicatietool`. This ensures that the menu
link is placed under the "Publicatietool configuration" section on
`/admin/config`.

PSEUDO-FIELD
------------

To display the bundle name when editing a node, this module provides the
pseudo-field `pt_common_bundle`. This field is available for all content types
and is hidden by default. To display it, you need to add it to the form display
settings of the content type.
