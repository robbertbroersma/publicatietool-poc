# $Id$
#
# Dutch translation of Drupal (general)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  modules/custom/pt_front/pt_front.info.yml: n/a
#  modules/custom/pt_front/pt_front.routing.yml: n/a
#  modules/custom/pt_front/templates/pt-front-home.html.twig: n/a
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2023-04-18 23:33+0200\n"
"PO-Revision-Date: 2023-04-18 23:33+0200\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: Dutch <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: modules/custom/pt_front/pt_front.info.yml:0
msgid "Publicatietool front"
msgstr ""

#: modules/custom/pt_front/pt_front.info.yml:0
msgid "Provides the temporary, hard-coded front page."
msgstr "Biedt de tijdelijke, hardgecodeerde voorpagina."

#: modules/custom/pt_front/pt_front.info.yml:0
msgid "Publicatietool"
msgstr ""

#: modules/custom/pt_front/pt_front.routing.yml:0
msgid "The solution for accessible documents"
msgstr "De oplossing voor toegankelijke documenten"

#: modules/custom/pt_front/templates/pt-front-home.html.twig:13
msgid "This is the front page."
msgstr "Dit is de voorpagina."

