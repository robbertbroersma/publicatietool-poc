<?php

declare(strict_types=1);

namespace Drupal\pt_front\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Publicatietool front routes.
 */
class FrontController extends ControllerBase {

  /**
   * Returns temporary, hard-coded front page.
   *
   * @return array
   *   A render array representing the temporary, hard-coded front page content.
   */
  public function home() {
    $build = [];

    // Use the 'pt_front_home' theme hook to render the content that is hard-
    // coded in the Twig template.
    $build['content'] = [
      '#theme' => 'pt_front_home',
    ];

    return $build;
  }

}
