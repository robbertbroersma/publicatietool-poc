<?php

namespace Drupal\pt_facets\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the search form for the search block.
 *
 * @internal
 */
class FacetForm extends FormBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a new SearchApiForm.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RendererInterface $renderer) {
    $this->configFactory = $config_factory;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'facets_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $action_url = NULL, $input_name = 'keys', $input_placeholder = '', $submit_value = '', $input_label = '', $input_label_visibility = 'invisible', $pass_get_params = TRUE) {

    $form['select_filter'] = [
      '#type' => 'radios',
      // Prevent op from showing up in the query string.
      '#name' => '',
      '#default_value' => 'none',
      '#options' => [
        'none' => $this->t('Reset'),
        'facet_subject' => $this->t('Subject'),
        'facet_status' => $this->t('Status'),
        'facet_created' => $this->t('Date created'),
        'facet_modified' => $this->t('Date modified'),
        'facet_author' => $this->t('Author'),
      ],
      '#prefix' => '<div class="select-filters">',
      '#suffix' => '</div>',
      '#attributes' => [
        'class' => ['select-filter'],
      ],
      '#pt_facets' => TRUE,
    ];

    // Facet wrapper.
    $form['facets'] = [
      '#type' => 'fieldset',
    ];

    // Subject.
    $form['facets']['facet_subject'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Subject'),
      // @todo get available terms from view or search api Subject facet.
      '#options' => $this->getTerms('subject'),
      '#pt_facets' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="select_filter"]' => ['value' => 'facet_subject'],
        ],
      ],
      '#prefix' => '<div class="form-item-facet js-add-filter">',
      '#suffix' => '</div>',
    ];

    // Status.
    $form['facets']['facet_status_published'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Status'),
      '#options' => [
        0 => $this->t('Concept'),
        1 => $this->t('Published'),
      ],
      '#pt_facets' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="select_filter"]' => ['value' => 'facet_status'],
        ],
      ],
      '#prefix' => '<div class="form-item-facet">',
      '#suffix' => '</div>',
    ];

    // Date created.
    $form['facets']['facet_created'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Date Created'),
      '#options' => $this->dateOptions(),
      '#pt_facets' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="select_filter"]' => ['value' => 'facet_created'],
        ],
      ],
      '#prefix' => '<div class="form-item-facet">',
      '#suffix' => '</div>',
    ];

    // Date modified.
    $form['facets']['facet_modified'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Date modified'),
      '#options' => $this->dateOptions(),
      '#pt_facets' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="select_filter"]' => ['value' => 'facet_modified'],
        ],
      ],
      '#prefix' => '<div class="form-item-facet">',
      '#suffix' => '</div>',
    ];

    // Author.
    $form['facets']['facet_author'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Filter authors'),
      '#options' => $this->getUserNames(),
      '#pt_facets' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="select_filter"]' => ['value' => 'facet_author'],
        ],
      ],
      '#prefix' => '<div class="form-item-facet">',
      '#suffix' => '</div>',
    ];

    $form['facets']['actions'] = [
      '#type' => 'actions',
      '#states' => [
        'visible' => [
          ':input[name="select_filter"]' => ['!value' => 'none'],
        ],
      ],
    ];

    // Reset button.
    $form['facets']['actions']['reset'] = [
      '#type' => 'item',
      '#markup' => '<div class="button button--secondary js-button-reset">' . $this->t('Reset filter') . '</div>',
    ];

    // Submit.
    $form['facets']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply filter'),
      // Prevent op from showing up in the query string.
      '#name' => '',
      '#pt_facets' => TRUE,
    ];

    $form['#attributes']['class'][] = 'facets-form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $form_state_values = $form_state->getValues();

    foreach ($form_state_values as $name => $params) {
      $count = 0;
      if (is_array($params)) {

        foreach ($params as $index_param => $param_value) {

          if ($param_value !== 0) {
            $param_name = 'f[' . $count . ']';
            $param[$param_name] = $name . ':' . $param_value;
            $count++;
          }
        }
      }

    }

    // Redirect to Documents page with search facet parameters.
    if (isset($param)) {
      $url = Url::fromRoute('view.documents.page', [$param]);
      $form_state->setRedirectUrl($url);
    }

  }

  /**
   * {@inheritdoc}
   *
   * @param string $vid
   *   The taxonomy ID.
   *
   * @return array
   *   The associative array with the term id as key.
   */
  private function getTerms(string $vid): array {
    /** @var \Drupal\taxonomy\Entity\Term[] $terms */
    $terms = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties(['vid' => $vid]);

    return array_reduce($terms, static function (array $options, Term $term) {
        $options[$term->id()] = $term->getName();
        return $options;
    }, []);
  }

  /**
   *
   */
  private function getDocumentIds(): array {
    $query = \Drupal::entityQuery('node')
      ->accessCheck(TRUE)
      ->condition('type', 'document');

    return $query->execute();
  }

  /**
   * @return array
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getUserNames(): array {
    $documentIds = $this->getDocumentIds();
    $userNames = [];

    foreach ($documentIds as $documentId) {
      $entity = \Drupal::entityTypeManager()->getStorage('node')->load($documentId);

      if (FALSE === $entity->access('view')) {
        continue;
      }

      $userId = $entity->get('uid')->target_id;
      $account = User::load($userId);
      $userNames[$account->get('name')->value] = $account->get('name')->value;
    }

    return $userNames;
  }

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The associative array with filter options for date.
   */
  private function dateOptions(): array {

    $dates = [];
    $dates['today'] = $this->t('Today');
    $dates['last_seven_days'] = $this->t('Last week');
    $dates['last_month'] = $this->t('Last month');
    $dates['last_year'] = $this->t('Last year');

    return $dates;
  }

}
