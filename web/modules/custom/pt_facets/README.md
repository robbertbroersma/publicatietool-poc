## INTRODUCTION

The pt_facets module adds search facets to the documents page.

## REQUIREMENTS

The module is tested with Search API and Search API Solr.

- [Search API](https://www.drupal.org/project/search_api)
- [Search API Solr](https://www.drupal.org/project/search_api_solr)
- [Facets](https://www.drupal.org/project/facets)
- [PT Facets Date Range]

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.
