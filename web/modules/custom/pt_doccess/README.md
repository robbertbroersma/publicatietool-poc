INTRODUCTION
------------

This module provides the functionality to transform office files (e.g., a Word
document) into a webpage that adheres to the WCAG 2.1 AA accessibility
guidelines.

CONFIGURATION
-------------

There is no specific configuration for this module, but the RabbitMQ and MinIO
S3 file storage services need to be defined properly. The RabbitMQ settings can
be found in the `settings.local.php` file and `config/sync/rabbitmq.config.yml`,
while the MinIO settings are stored in the configuration of the S3 File System
module (i.e., `config/sync/s3fs.settings.yml`).

BUNDLE CLASSES
--------------

A bundle class is provided that represents nodes of the `document` content type,
which provides the functionality to add components to a document.

A document itself is mainly built out of different components, such as headings,
images, tables, lists, etc. Each of these components has an accompanied bundle
class for a paragraph entity. These bundle classes implement the functionality
to handle the respective contents.

PSEUDO-FIELD
------------

To display aggregated accessibility statistics of a document, this module
provides the pseudo-field `pt_doccess_accessibility`. This field is added on
the document editing page in the advanced section (i.e., the input fields shown
in the blocks on the left-hand side of the page).

UPLOADING AND CONVERSION OF OFFICE FILES
----------------------------------------

The module provides a block that can be placed on a page to offer a file upload
form. After uploading the office file (to the MinIO service), an entry is added
to the (RabbitMQ) `queue.publicatietool.inbound` queue containing al needed
information to convert the office file's content.

The Document Conversion System listens for new message on the
`queue.publicatietool.inbound` queue to start converting the file's, and
delivers the result back via the `queue.publicatietool.outbound` queue. A queue
worker processes the conversion result messages and adds the converted content
to the document.

DATA TYPES
----------

To ease the process of creating conversion requests and process the conversion
results, the module provides a number of data types.
