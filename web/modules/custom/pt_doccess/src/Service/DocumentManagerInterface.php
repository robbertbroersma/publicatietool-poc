<?php

declare(strict_types=1);

namespace Drupal\pt_doccess\Service;

use Drupal\pt_doccess\Entity\DocumentInterface;

/**
 * Provides an interface defining a document manager.
 */
interface DocumentManagerInterface {

  /**
   * Schedules a document for conversion.
   *
   * Because converting an office file can take a relatively long time (in the
   * order of several minutes), this process has been set up asynchronously. The
   * request is submitted to the Document Conversion Service and scheduled via a
   * queue. The conversion result is returned via a separate queue and handled
   * by a queue worker.
   *
   * @param \Drupal\pt_doccess\Entity\DocumentInterface $document
   *   The document to be converted.
   */
  public function scheduleDocumentConversion(DocumentInterface $document): void;

  /**
   * Stores the content elements of a conversion result in a document node.
   *
   * @param array $conversion_message_data
   *   Associative array containing the (meta)data of the conversion result.
   */
  public function processDocumentConversionResult(array $conversion_message_data): void;

  /**
   * Validates the contents of a document and stores the result.
   *
   * The Accessibility Validation API is used to check if the contents of the
   * document are valid, i.e., comply to the WCAG 2.1 AA accessibility
   * guidelines.
   *
   * @param \Drupal\pt_doccess\Entity\DocumentInterface $document
   *   The document to be validated.
   */
  public function validateDocumentContent(DocumentInterface $document): void;

}
