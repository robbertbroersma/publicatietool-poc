<?php

declare(strict_types=1);

namespace Drupal\pt_doccess\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\File\Exception\FileWriteException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\TypedData\TypedDataManagerInterface;
use Drupal\file\FileRepositoryInterface;
use Drupal\pt_doccess\Entity\DocumentInterface;
use Drupal\pt_doccess\Entity\HeadingComponentInterface;
use Drupal\pt_doccess\Entity\ImageComponentInterface;
use Drupal\pt_doccess\Entity\ListComponentInterface;
use Drupal\pt_doccess\Entity\QuotationComponentInterface;
use Drupal\pt_doccess\Entity\TableComponentInterface;
use Drupal\pt_doccess\Entity\TextComponentInterface;
use Drupal\pt_validation\Plugin\Field\FieldType\ValidationMessageItemInterface;
use Drupal\pt_validation\Service\ValidationClientInterface;
use Drupal\rabbitmq\Queue\QueueItemWithProperties;
use Psr\Log\LoggerInterface;
use Symfony\Component\Lock\Exception\LockAcquiringException;
use Symfony\Component\Serializer\Serializer;

/**
 * Defines a document manager.
 */
class DocumentManager implements DocumentManagerInterface {

  use StringTranslationTrait;

  /**
   * The name of the queue to submit conversion requests too.
   */
  private const CONVERSION_REQUEST_QUEUE = 'queue.publicatietool.inbound';

  /**
   * Identifier used to prevent simultaneously processing document results.
   *
   * Multiple process can trigger processing the conversion result of a document
   * and/or the validation information of a document.
   */
  private const LOCK_IDENTIFIER = 'pt_doccess:document_manager:';

  /**
   * Stream wrapper URI prefix to place office files for the conversion service.
   */
  private const FILE_CONVERSION_URI_PREFIX = 's3://';

  /**
   * Stream wrapper URI prefix to store the images of the documents.
   */
  private const FILE_DESTINATION_URI_PREFIX = 'public://documents/images';

  /**
   * The validation client.
   *
   * @var \Drupal\pt_validation\Service\ValidationClientInterface
   */
  protected ValidationClientInterface $validationClient;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\Serializer
   */
  protected Serializer $serializer;

  /**
   * The conversion request queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected QueueInterface $conversionRequestQueue;

  /**
   * The typed data manager.
   *
   * @var \Drupal\Core\TypedData\TypedDataManagerInterface
   */
  protected TypedDataManagerInterface $typedDataManager;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected EntityRepositoryInterface $entityRepository;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The file repository service.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected FileRepositoryInterface $fileRepository;

  /**
   * The lock service.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected LockBackendInterface $lock;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * The logger object, that writes to the specific channel of this module.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Constructs a new DocumentManager object.
   *
   * @param \Drupal\pt_validation\Service\ValidationClientInterface $validation_client
   *   The validation client.
   * @param \Symfony\Component\Serializer\Serializer $serializer
   *   The serializer.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \Drupal\Core\TypedData\TypedDataManagerInterface $typed_data_manager
   *   The typed data manager.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\file\FileRepositoryInterface $file_repository
   *   The file repository service.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock service.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user account.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger object, that writes to the specific channel of this module.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The translation service.
   */
  public function __construct(ValidationClientInterface $validation_client, Serializer $serializer, QueueFactory $queue_factory, TypedDataManagerInterface $typed_data_manager, EntityRepositoryInterface $entity_repository, FileSystemInterface $file_system, FileRepositoryInterface $file_repository, LockBackendInterface $lock, AccountProxyInterface $current_user, LoggerInterface $logger, TranslationInterface $string_translation) {
    $this->validationClient = $validation_client;
    $this->serializer = $serializer;
    $this->conversionRequestQueue = $queue_factory->get(self::CONVERSION_REQUEST_QUEUE);
    $this->typedDataManager = $typed_data_manager;
    $this->entityRepository = $entity_repository;
    $this->fileSystem = $file_system;
    $this->fileRepository = $file_repository;
    $this->lock = $lock;
    $this->currentUser = $current_user;
    $this->logger = $logger;
    $this->stringTranslation = $string_translation;
  }

  /**
   * Ensures that the office file is located at the Minio S3 storage service.
   *
   * @param \Drupal\pt_doccess\Entity\DocumentInterface $document
   *   The document for which to ensure the office file is located at the Minio
   *   S3 storage service.
   */
  private function ensureOfficeFileIsLocatedAtS3(DocumentInterface $document): void {
    $office_file = $document->getOfficeFile();

    if ($office_file === NULL) {
      throw new \LogicException('The document should have an office file, but it is not available.');
    }

    // Ensure that the file attached to the document is stored in the Minio S3
    // storage service, before submitting the conversion request.
    $uri = $this->fileSystem->move($office_file->getFileUri(), self::FILE_CONVERSION_URI_PREFIX . $office_file->getFilename(), FileSystemInterface::EXISTS_REPLACE);
    $office_file->setFileUri($uri);
    $office_file->save();
  }

  /**
   * {@inheritdoc}
   */
  public function scheduleDocumentConversion(DocumentInterface $document): void {
    // @todo NLDOC-895: Add (informative) logging to this function.
    // @todo NLDOC-385: Determine whether this is the correct place to ensure the
    //   office file is located at the Minio S3 storage service. Might be better
    //   to place a copy of the file (not the entity) in the Minio S3 storage
    //   when creating the conversion request (see the normalizer invocation
    //   below).
    $this->ensureOfficeFileIsLocatedAtS3($document);

    // Use the Serialization API to construct the conversion request as an
    // array, containing all necessary information for the Document Conversion
    // System to perform the conversion of the document node.
    $conversion_request = $this->serializer->normalize($document, 'pt_doccess_conversion_request');

    // Place the conversion request into the designated queue, where RabbitMQ
    // encodes the array as a JSON string.
    // @todo NLDOC-390: Determine the functionality when the conversion request
    //   queue cannot receive the message.
    $queued = $this->conversionRequestQueue->createItem(
      new QueueItemWithProperties($conversion_request, [
        'content_type' => 'application/json',
      ])
    );

    if (FALSE !== $queued) {
      $this->logger->info('User %user scheduled a conversion request for document %document (%id).', [
        '%user' => $this->currentUser->getAccountName(),
        '%document' => $document->getTitle(),
        '%id' => $document->id(),
      ]);
    }
    else {
      $this->logger->error('User %user failed to schedule a conversion request for document %document (%id).', [
        '%user' => $this->currentUser->getAccountName(),
        '%document' => $document->getTitle(),
        '%id' => $document->id(),
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function processDocumentConversionResult(array $conversion_message_data): void {
    // @todo NLDOC-895: Add (informative) logging to this function.
    /** @var \Drupal\pt_doccess\Plugin\DataType\ConversionMessageData $conversion_message */
    // Create a typed data object from the conversion message data, to make
    // validation of the received information easier.
    $conversion_message = $this->typedDataManager->create(
      $this->typedDataManager->createDataDefinition('pt_doccess_conversion_message'),
      $conversion_message_data
    );

    // Validate the conversion message.
    $violations = $conversion_message->validate();
    if ($violations->count() > 0) {
      foreach ($violations as $violation) {
        $this->logger->error('Conversion result validation failed: @message (@property_path = @invalid_value)', [
          '@message' => $violation->getMessage(),
          '@property_path' => $violation->getPropertyPath(),
          '@invalid_value' => (string) $violation->getInvalidValue(),
        ]);
      }

      // @todo NLDOC-393: Determine what to do with the conversion message, if
      //   there are validation errors.
    }

    // Load the document node, that is associated with the conversion message.
    $document = $this->entityRepository->loadEntityByUuid('node', $conversion_message->get('identifier')
      ->getValue());

    if (!($document instanceof DocumentInterface)) {
      throw new \LogicException("Provided UUID doesn't match a valid document node.");
    }

    // First, obtain a lock to prevent multiple processes from processing the
    // conversion or validation result to the same document (e.g., via another
    // queue worker, action, save, Cron run, etc).
    $this->acquireDocumentLock($document);

    // Only when the office file is available, the contents can be imported.
    $office_file = $document->getOfficeFile();

    if ($office_file === NULL) {
      throw new \LogicException('The document should have an office file, but it is not available.');
    }

    $conversion_content = file_get_contents($conversion_message->get('contentLocation')
      ->getValue());

    if ($conversion_content === FALSE) {
      throw new \LogicException('Unable to retrieve content from the provided location.');
    }

    // Decode the JSON string to an array.
    // @todo NLDOC-98: Below code is a temporary solution to fill the contents
    //   of the document node. This should be replaced by a proper solution,
    //   that uses the Serialization API. Note, that the Typed Data API should
    //   not used, since all logic should be implemented directly in the
    //   document (i.e., node) and component (i.e., paragraph) entities.
    $document_structure = $this->serializer->decode($conversion_content, 'json');

    // Set the title of the document node, if it is available.
    if (!empty($document_structure['attributes']['title'])) {
      $document->setTitle($document_structure['attributes']['title']);
    }

    // Create the file assets if they are available.
    $files = [];
    if (!empty($document_structure['attributes']['assets'])) {
      // Construct a destination directory for the (image) assets similar to
      // the image field's configuration of the image component (paragraph).
      $now = new DrupalDateTime();
      $destination = self::FILE_DESTINATION_URI_PREFIX . DIRECTORY_SEPARATOR . $now->format('Y-m');

      // Ensure that the destination directory exists.
      if (!$this->fileSystem->prepareDirectory($destination, FileSystemInterface::CREATE_DIRECTORY)) {
        throw new FileWriteException(sprintf("The destination directory '%s' is not writable.", $destination));
      }

      foreach ($document_structure['attributes']['assets'] as $asset) {
        if (!isset($asset['contentLocation'])) {
          throw new \LogicException('The asset should have a content location, but it is not available.');
        }

        // Retrieve the asset's content, from the MinIO S3 storage service.
        $asset_content = file_get_contents('s3://' . $asset['contentLocation']);

        if ($asset_content === FALSE) {
          throw new \LogicException('Unable to retrieve asset content from the provided location.');
        }

        if (!isset($asset['filename'])) {
          throw new \LogicException('The asset should have a filename, but it is not available.');
        }

        // Get the name of the file.
        $filename = $asset['filename'];

        // Write the asset's content to the destination directory, and keep
        // track of all the files that are created. This is used to connect
        // image components to the correct file asset.
        $files['./' . $filename] = $this->fileRepository->writeData($asset_content, $destination . DIRECTORY_SEPARATOR . $this->fileSystem->basename($filename));
      }
    }

    // Process each document structure member and create the corresponding
    // component (paragraph) entities.
    foreach ($document_structure['members'] as $member) {
      switch ($member['type']) {
        case 'heading':
          // In the document node, the title is the heading with level 1.
          // Therefore, the heading level should be increased by 1.
          $document->addHeadingComponent($member['content'], $member['attributes']['level'] + 1, $member['validation']);
          break;

        case 'list-unordered':
        case 'list-ordered':
          $list_type = $member['type'] === 'list-unordered' ? ListComponentInterface::UNORDERED_LIST : ListComponentInterface::ORDERED_LIST;
          $document->addListComponent(array_map(static function (array $member) {
            return $member['content'];
          }, $member['members']), $list_type, $member['validation']);
          break;

        case 'table':
          $document->addTableComponent($member['content'], $member['validation']);
          break;

        case 'quotation':
          $document->addQuotationComponent($member['content'], $member['validation']);
          break;

        case 'image':
          if (empty($member['attributes']['src'])) {
            throw new \LogicException('The image should have a source, but it is not available.');
          }

          if (!isset($files[$member['attributes']['src']])) {
            throw new \LogicException(sprintf("The image's source '%s' is not available.", $member['attributes']['src']));
          }

          $document->addImageComponent($files[$member['attributes']['src']], $member['attributes']['alt'] ?? '', $member['validation']);
          break;

        case 'text':
        default:
          $document->addTextComponent($member['content'], FALSE, $member['validation']);
      }
    }

    // Add an appropriate revision log message, so users can understand the
    // revision that is automatically created.
    $document->setRevisionLogMessage($this->t('Imported the contents of the provided office file.'));

    // Indicate that the document is converted.
    $document->processedConversion();

    // Save the document node, which also ensures all coupled paragraph
    // component entities are stored to the database.
    $document->save();

    $this->logger->info('Added the contents of the office file %filename to document %document (%id).', [
      '%filename' => $office_file->getFilename(),
      '%document' => $document->getTitle(),
      '%id' => $document->id(),
    ]);

    // Finally, release the lock for this document.
    $this->releaseDocumentLock($document);
  }

  /**
   * {@inheritdoc}
   */
  public function validateDocumentContent(DocumentInterface $document): void {
    // @todo NLDOC-895: Add (informative) logging to this function.
    // First, obtain a lock to prevent multiple processes from processing the
    // conversion or validation result to the same document (e.g., via another
    // queue worker, action, save, Cron run, etc).
    $this->acquireDocumentLock($document);

    // Create a serialized representation of the provided document.
    $serialized_document = $this->elementStructure($document);

    // Invoke the Accessibility Validation API to validate the (serialized)
    // document.
    $validation_result = $this->validationClient->validateAccessibility($serialized_document);

    // Update the result of the Accessibility Validation API.
    if (FALSE !== $validation_result) {
      $this->updateValidationMessages($document, $validation_result);
      $document->processedAccessibilityValidation();
      $document->save();
    }

    // Finally, release the lock for this document.
    $this->releaseDocumentLock($document);
  }

  /**
   * Creates a serialized representation of the provided document.
   *
   * @param \Drupal\pt_doccess\Entity\DocumentInterface $document
   *   The document for which to create a serialized representation.
   *
   * @return string
   *   The serialized representation of the provided document.
   */
  private function elementStructure(DocumentInterface $document): string {
    $structure = new \stdClass();
    $structure->identifier = $document->uuid();
    $structure->type = 'document';
    $structure->attributes = new \stdClass();
    $structure->attributes->title = $document->getTitle();
    $structure->members = [];

    foreach ($document->getComponents() as $component) {
      $member = new \stdClass();
      $member->identifier = $component->uuid();
      $member->attributes = new \stdClass();

      if ($component instanceof HeadingComponentInterface) {
        $member->type = 'heading';
        $member->content = $component->getText();
        $member->attributes->level = $component->getLevel();
      }
      elseif ($component instanceof ImageComponentInterface) {
        $member->type = 'image';
        $member->attributes->alt = $component->getAlternateText();
        $member->attributes->src = $component->getImage()->label();
      }
      elseif ($component instanceof ListComponentInterface) {
        $member->type = $component->getListType() === ListComponentInterface::UNORDERED_LIST ? 'list-unordered' : 'list-ordered';
        $member->members = [];
        foreach ($component->getListMembers() as $item) {
          $member_item = new \stdClass();
          // The Accessibility Validation API needs a UUID for each list member,
          // set the one from the example as each member doesn't have a UUID.
          $member_item->identifier = 'b9ba84e5-d360-41fd-9939-24b45c4b0a3c';
          $member_item->type = 'list-member';
          $member_item->content = $item;
          $member_item->attributes = new \stdClass();
          $member_item->validation = [];

          $member->members[] = $member_item;
        }
      }
      elseif ($component instanceof QuotationComponentInterface) {
        $member->type = 'quotation';
        $member->content = $component->getQuotation();
      }
      elseif ($component instanceof TableComponentInterface) {
        $member->type = 'table';
        $member->content = $component->getTable();
      }
      elseif ($component instanceof TextComponentInterface) {
        $member->type = 'text';
        $member->content = $component->getText();
      }

      $member->validation = [];
      foreach ($component->getValidationMessages() as $item) {
        $validation_message = new \stdClass();
        $validation_message->identifier = $item->get('identifier')->getValue();
        $validation_message->code = $item->get('code')->getValue();
        $validation_message->state = $item->get('state')->getValue();
        $validation_message->context = new \stdClass();
        $validation_message->context->selector = $item->get('selector')->getValue();
        $validation_message->checksum = new \stdClass();
        $validation_message->checksum->algorithm = $item->get('checksum_algorithm')->getValue();
        $validation_message->checksum->hash = $item->get('checksum_hash')->getValue();
        // @todo NLDOC-821: determine the wanted behavior for unknown
        //   validation messages.
        $validation_message->severity = $item->getSeverity() ?? ValidationMessageItemInterface::SEVERITY_WARNING;
        $validation_message->message = $item->getMessage();

        $member->validation[] = $validation_message;
      }

      $structure->members[] = $member;
    }

    return Json::encode($structure);
  }

  /**
   * Updates the validation messages per component of the provided document.
   *
   * @param \Drupal\pt_doccess\Entity\DocumentInterface $document
   *   The document for which to update the validation messages.
   * @param string $validation_result
   *   The result of the Accessibility Validation API.
   */
  private function updateValidationMessages(DocumentInterface $document, string $validation_result): void {
    // Decode the JSON string to an array.
    // @todo NLDOC-98: Below code is a temporary solution to fill the contents
    //   of the document node. This should be replaced by a proper solution,
    //   that uses the Serialization API.
    $validated_document_structure = $this->serializer->decode($validation_result, 'json');

    // Store the validation message per UUID of the component it applies too.
    $validation_messages = [];
    foreach ($validated_document_structure['members'] as $member) {
      $validation_messages[$member['identifier']] = $member['validation'];
    }

    // Process each component and update its validation messages.
    foreach ($document->getComponents() as $component) {
      $component->setValidationMessages($validation_messages[$component->uuid()] ?? []);
      $component->save();
    }
  }

  /**
   * Helper function to acquire a lock for the provided document.
   *
   * @param \Drupal\pt_doccess\Entity\DocumentInterface $document
   *   The document for which to acquire a lock.
   */
  private function acquireDocumentLock(DocumentInterface $document): void {
    if (!$this->lock->acquire(self::LOCK_IDENTIFIER . $document->id())) {
      throw new LockAcquiringException(sprintf("Unable to acquire lock '%s'.", self::LOCK_IDENTIFIER . $document->id()));
    }
  }

  /**
   * Helper function to release a lock for the provided document.
   *
   * @param \Drupal\pt_doccess\Entity\DocumentInterface $document
   *   The document for which to release a lock.
   */
  private function releaseDocumentLock(DocumentInterface $document): void {
    $this->lock->release(self::LOCK_IDENTIFIER . $document->id());
  }

}
