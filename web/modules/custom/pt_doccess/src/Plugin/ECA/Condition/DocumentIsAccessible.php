<?php

declare(strict_types=1);

namespace Drupal\pt_doccess\Plugin\ECA\Condition;

use Drupal\eca\Plugin\ECA\Condition\ConditionBase;
use Drupal\pt_doccess\Entity\DocumentInterface;
use Drupal\pt_validation\Plugin\Field\FieldType\ValidationMessageItemInterface;

/**
 * Plugin implementation of the ECA condition to check document accessibility.
 *
 * @EcaCondition(
 *   id = "pt_doccess_document_is_accessible",
 *   label = @Translation("Entity: is accessible"),
 *   description = @Translation("Evaluates whether there are no accessibility errors for the entity."),
 *   context_definitions = {
 *     "entity" = @ContextDefinition("entity", label = @Translation("Entity"))
 *   }
 * )
 */
class DocumentIsAccessible extends ConditionBase {

  /**
   * {@inheritdoc}
   */
  public function evaluate(): bool {
    $entity = $this->getValueFromContext('entity');

    // Ensure that the entity is a document, otherwise accessibility statistics
    // cannot be generated.
    if (!($entity instanceof DocumentInterface)) {
      throw new \LogicException('Only documents can be checked on accessibility.');
    }

    // Retrieve the accessibility statistics.
    $accessibility = $entity->aggregateAccessibilityStatistics();

    // A document is considered accessible, when there are no warnings and no
    // errors.
    return $this->negationCheck(empty($accessibility[ValidationMessageItemInterface::SEVERITY_WARNING]) && empty($accessibility[ValidationMessageItemInterface::SEVERITY_ERROR]));
  }

}
