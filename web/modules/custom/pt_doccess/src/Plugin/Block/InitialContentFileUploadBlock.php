<?php

declare(strict_types=1);

namespace Drupal\pt_doccess\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides an 'InitialContentFileUploadBlock' block to upload office files.
 *
 * @Block(
 *   id = "pt_doccess_initial_content_file_upload_block",
 *   admin_label = @Translation("Initial content file upload"),
 *   category = @Translation("Publicatietool"),
 * )
 */
class InitialContentFileUploadBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    return [
      '#theme' => 'pt_dragzone',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account): AccessResult {
    return AccessResult::allowedIfHasPermission($account, 'upload initial content files');
  }

}
