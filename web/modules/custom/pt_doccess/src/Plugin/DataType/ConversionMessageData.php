<?php

declare(strict_types=1);

namespace Drupal\pt_doccess\Plugin\DataType;

use Drupal\Core\TypedData\Plugin\DataType\Map;

/**
 * Defines the "ConversionMessage" data type.
 *
 * @DataType(
 *   id = "pt_doccess_conversion_message",
 *   label = @Translation("Conversion message"),
 *   definition_class = "\Drupal\pt_doccess\TypedData\Definition\ConversionMessageDefinition"
 * )
 */
class ConversionMessageData extends Map {}
