<?php

declare(strict_types=1);

namespace Drupal\pt_doccess\Plugin\DataType;

use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\Core\TypedData\Plugin\DataType\Uri;

/**
 * The content location data type.
 *
 * The plain value of a content location is a URI, with as default pointing to
 * the S3 storage service.
 *
 * @DataType(
 *   id = "pt_doccess_content_location_uri",
 *   label = @Translation("Content location")
 * )
 */
class ContentLocationUri extends Uri {

  /**
   * Default stream wrapper.
   */
  private const UPLOAD_LOCATION = 's3://';

  /**
   * {@inheritdoc}
   */
  public function setValue($value, $notify = TRUE) {
    // Set the default stream wrapper, if no scheme has been set.
    if (!StreamWrapperManager::getScheme($value)) {
      $value = self::UPLOAD_LOCATION . $value;
    }

    parent::setValue($value, $notify);
  }

}
