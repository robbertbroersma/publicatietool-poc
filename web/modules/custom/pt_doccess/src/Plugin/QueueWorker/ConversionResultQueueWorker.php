<?php

declare(strict_types=1);

namespace Drupal\pt_doccess\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\pt_doccess\Service\DocumentManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes conversion results, provided by the Document Conversion Service.
 *
 * @QueueWorker(
 *   id = "queue.publicatietool.outbound",
 *   title = @Translation("Conversion result"),
 *   cron = {"time" = 60}
 * )
 */
class ConversionResultQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The document manager service.
   *
   * @var \Drupal\pt_doccess\Service\DocumentManagerInterface
   */
  protected DocumentManagerInterface $documentManager;

  /**
   * Constructs a new ConversionResultQueueWorker object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\pt_doccess\Service\DocumentManagerInterface $document_manager
   *   The document manager service.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, DocumentManagerInterface $document_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->documentManager = $document_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('pt_doccess.document_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem(mixed $data): void {
    // @todo NLDOC-422: refactor the exceptions that can occur when processing
    //   the conversion result and determine the behavior for processing
    //   messages on this queue.
    $this->documentManager->processDocumentConversionResult($data);
  }

}
