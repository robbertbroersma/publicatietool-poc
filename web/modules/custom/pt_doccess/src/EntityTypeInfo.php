<?php

declare(strict_types=1);

namespace Drupal\pt_doccess;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\pt_doccess\Entity\Document;
use Drupal\pt_doccess\Entity\HeadingComponent;
use Drupal\pt_doccess\Entity\ImageComponent;
use Drupal\pt_doccess\Entity\ListComponent;
use Drupal\pt_doccess\Entity\QuotationComponent;
use Drupal\pt_doccess\Entity\TableComponent;
use Drupal\pt_doccess\Entity\TextComponent;

/**
 * Manipulates entity type information.
 *
 * This class contains primarily bridged hooks for compile-time or
 * cache-clear-time hooks. Runtime hooks should be placed in EntityOperations.
 *
 * @internal
 */
class EntityTypeInfo {

  use StringTranslationTrait;

  /**
   * Describe the bundles for entity types.
   *
   * @return array
   *   An associative array of all entity bundles, keyed by the entity
   *   type name, and then the bundle name, with the following keys:
   *   - label: The human-readable name of the bundle.
   *   - uri_callback: (optional) The same as the 'uri_callback' key defined for
   *     the entity type in the EntityTypeManager, but for the bundle only. When
   *     determining the URI of an entity, if a 'uri_callback' is defined for
   *     both the entity type and the bundle, the one for the bundle is used.
   *   - translatable: (optional) A boolean value specifying whether this bundle
   *     has translation support enabled. Defaults to FALSE.
   *   - class: (optional) The fully qualified class name for this bundle. If
   *     omitted, the class from the entity type definition will be used.
   *     Multiple bundles must not use the same subclass. If a class is reused
   *     by multiple bundles, an
   *     \Drupal\Core\Entity\Exception\AmbiguousBundleClassException will be
   *     thrown.
   *
   * @see \Drupal\Core\Entity\EntityTypeBundleInfo::getBundleInfo()
   * @see hook_entity_bundle_info_alter()
   */
  public function entityBundleInfoAlter(array &$bundles): array {
    // Define the document node entity bundle class.
    if (isset($bundles['node'][Document::BUNDLE])) {
      $bundles['node'][Document::BUNDLE]['class'] = Document::class;
    }

    // Define all components as paragraph entity bundle classes.
    $components = [
      HeadingComponent::BUNDLE => HeadingComponent::class,
      ImageComponent::BUNDLE => ImageComponent::class,
      ListComponent::BUNDLE => ListComponent::class,
      QuotationComponent::BUNDLE => QuotationComponent::class,
      TableComponent::BUNDLE => TableComponent::class,
      TextComponent::BUNDLE => TextComponent::class,
    ];
    foreach ($components as $bundle => $class) {
      if (isset($bundles['paragraph'][$bundle])) {
        $bundles['paragraph'][$bundle]['class'] = $class;
      }
    }

    return $bundles;
  }

  /**
   * Gets the "extra fields" for a bundle.
   *
   * @return array
   *   A nested array of 'pseudo-field' elements. Each list is nested within the
   *   following keys: entity type, bundle name, context (either 'form' or
   *   'display'). The keys are the name of the elements as appearing in the
   *   renderable array (either the entity form or the displayed entity). The
   *   value is an associative array:
   *   - label: The human readable name of the element. Make sure you sanitize
   *     this appropriately.
   *   - description: A short description of the element contents.
   *   - weight: The default weight of the element.
   *   - visible: (optional) The default visibility of the element. Defaults to
   *     TRUE.
   *   - edit: (optional) String containing markup (normally a link) used as the
   *     element's 'edit' operation in the administration interface. Only for
   *     'form' context.
   *   - delete: (optional) String containing markup (normally a link) used as
   *     the element's 'delete' operation in the administration interface. Only
   *     for 'form' context.
   *
   * @see hook_entity_extra_field_info()
   */
  public function entityExtraFieldInfo() {
    $extra_fields = [];

    $extra_fields['node'][Document::BUNDLE]['form'] = [
      'pt_doccess_accessibility' => [
        'label' => $this->t('Accessibility'),
        'description' => $this->t("Summary of the document's accessibility information."),
        'weight' => 0,
        'visible' => TRUE,
      ],
    ];

    return $extra_fields;
  }

}
