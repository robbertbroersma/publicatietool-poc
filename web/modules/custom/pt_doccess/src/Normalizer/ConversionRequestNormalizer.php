<?php

declare(strict_types=1);

namespace Drupal\pt_doccess\Normalizer;

use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\pt_doccess\Entity\DocumentInterface;
use Drupal\serialization\Normalizer\NormalizerBase;

/**
 * Creates an array structure for a conversion request based on a document node.
 */
class ConversionRequestNormalizer extends NormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = DocumentInterface::class;

  /**
   * {@inheritdoc}
   */
  protected $format = 'pt_doccess_conversion_request';

  /**
   * {@inheritdoc}
   */
  public function normalize(mixed $object, string $format = NULL, array $context = []) {
    // @todo NLDOC-895: Add (informative) logging to this function.
    if (!($object instanceof DocumentInterface)) {
      throw new \InvalidArgumentException('Only document nodes can be normalized for a conversion request.');
    }

    // Retrieve the office file that contains the content that needs to be
    // imported into the document node.
    $office_file = $object->getOfficeFile();

    if (!$office_file) {
      throw new MissingDataException('No conversion request can be generated if the document node does not have an office file.');
    }

    $office_file_uri = $office_file->getFileUri();

    if (StreamWrapperManager::getScheme($office_file_uri) !== 's3') {
      throw new \InvalidArgumentException('The Document Conversion System expects office files that are stored in S3 storage.');
    }

    // Provide all data that is needed by the Document Conversion Service to
    // perform the conversion.
    // Note: S3fsStream::realpath() does not support realpath() and always
    // returns FALSE. Therefore, the file system service cannot be used to
    // retrieve the real path of the office file.
    return [
      'identifier' => $object->uuid(),
      'contentLocation' => StreamWrapperManager::getTarget($office_file_uri),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function hasCacheableSupportsMethod(): bool {
    return TRUE;
  }

}
