<?php

declare(strict_types = 1);

namespace Drupal\pt_doccess\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\node\NodeStorageInterface;
use Drupal\pt_doccess\Entity\Document;
use Drupal\pt_dragzone\Event\DragzoneEvents;
use Drupal\pt_dragzone\Event\DragzoneUploadEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Handles processed file uploads made by the drag-and-drop functionality.
 */
class FileUploadEventSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The node storage.
   *
   * @var \Drupal\node\NodeStorageInterface
   */
  protected NodeStorageInterface $nodeStorage;

  /**
   * Constructs a FileUploadEventSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The translation service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, TranslationInterface $string_translation) {
    $this->nodeStorage = $entity_type_manager->getStorage('node');
    $this->stringTranslation = $string_translation;
  }

  /**
   * Creates a document node entity and attaches the uploaded file.
   *
   * @param \Drupal\pt_dragzone\Event\DragzoneUploadEvent $event
   *   The event object storing the information regarding the uploaded file.
   */
  public function onDragzoneFileUpload(DragzoneUploadEvent $event): void {
    // @todo NLDOC-895: Add (informative) logging to this function.
    // Retrieve the uploaded file entity that represents the uploaded file using
    // the drag-and-drop functionality.
    $file = $event->getFile();

    // Create a document node entity and indicate that the contents of the
    // uploaded office file should be imported (i.e., converted).
    /** @var \Drupal\pt_doccess\Entity\DocumentInterface $document */
    $document = $this->nodeStorage->create(['type' => Document::BUNDLE]);
    $document->setTitle($file->getFilename());
    $document->needsConversion($file);
    $document->setRevisionLogMessage($this->t('Created by uploading an office file.'));
    $document->save();
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];

    // Workaround to prevent a fatal error when deploying this functionality.
    // @see https://www.drupal.org/project/drupal/issues/2825358
    // @todo NLDOC-892: Remove this workaround once this code is deployed to
    //   production.
    if (class_exists(DragzoneEvents::class)) {
      $events[DragzoneEvents::DRAGZONE_FILE_UPLOAD] = 'onDragzoneFileUpload';
    }

    return $events;
  }

}
