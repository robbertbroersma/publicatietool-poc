<?php

declare(strict_types=1);

namespace Drupal\pt_doccess\TypedData\Definition;

use Drupal\Core\TypedData\ComplexDataDefinitionBase;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the data definition of a conversion message.
 *
 * A conversion message is sent to or received from the Document Conversion
 * System (DCS). When sent to the DCS it is a request to convert the office file
 * located at the `contentLocation` property. When received from the DCS it is a
 * response, on a conversion request, containing the converted content elements
 * (which are stored in the file of the `contentLocation` property). The
 * `identifier` property is the UUID of the document node that initiated the
 * request and should store the contents of the response.
 */
class ConversionMessageDefinition extends ComplexDataDefinitionBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(): array {
    if (!isset($this->propertyDefinitions)) {
      $info = &$this->propertyDefinitions;

      $info['identifier'] = DataDefinition::create('string')
        ->setLabel('Identifier')
        ->setDescription('The UUID of the document node corresponds to the conversion request and/or result.')
        ->setRequired(TRUE)
        ->addConstraint('Uuid');

      $info['contentLocation'] = DataDefinition::create('pt_doccess_content_location_uri')
        ->setLabel('Content location')
        ->setDescription('The URI of the file containing the office file for the conversion request or the content elements of a conversion result.')
        ->setRequired(TRUE);
    }

    return $this->propertyDefinitions;
  }

}
