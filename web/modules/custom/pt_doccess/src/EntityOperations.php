<?php

declare(strict_types=1);

namespace Drupal\pt_doccess;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\pt_doccess\Entity\DocumentInterface;

/**
 * Defines a class for reacting to entity events.
 *
 * @internal
 */
class EntityOperations {

  use StringTranslationTrait;

  /**
   * Provide a form-specific alteration for shared ('base') forms.
   *
   * Implementations are responsible for adding cache contexts/tags/max-age as
   * needed. See https://www.drupal.org/docs/8/api/cache-api/cache-api.
   *
   * By default, when \Drupal::formBuilder()->getForm() is called, Drupal looks
   * for a function with the same name as the form ID, and uses that function to
   * build the form. In contrast, base forms allow multiple form IDs to be
   * mapped to a single base (also called 'factory') form function.
   *
   * Modules can implement hook_form_BASE_FORM_ID_alter() to modify a specific
   * base form, rather than implementing hook_form_alter() and checking for
   * conditions that would identify the shared form constructor.
   *
   * To identify the base form ID for a particular form (or to determine whether
   * one exists) check the $form_state. The base form ID is stored under
   * $form_state->getBuildInfo()['base_form_id'].
   *
   * The call order is as follows: all existing form alter functions are called
   * for module A, then all for module B, etc., followed by all for any base
   * theme(s), and finally for the theme itself. The module order is determined
   * by system weight, then by module name.
   *
   * Within each module, form alter hooks are called in the following order:
   * first, hook_form_alter(); second, hook_form_BASE_FORM_ID_alter(); third,
   * hook_form_FORM_ID_alter(). So, for each module, the more general hooks are
   * called first followed by the more specific.
   *
   * @param array $form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $form_id
   *   String representing the name of the form itself. Typically this is the
   *   name of the function that generated the form.
   *
   * @see hook_form_alter()
   * @see hook_form_FORM_ID_alter()
   * @see \Drupal\Core\Form\FormBuilderInterface::prepareForm()
   *
   * @ingroup form_api
   */
  public function nodeFormAlter(array &$form, FormStateInterface $form_state, string $form_id) {
    /** @var \Drupal\node\NodeInterface $node */
    $node = $form_state->getFormObject()->getEntity();

    // Show the accessibility information of the document, by retrieving the
    // severity status of all components.
    if ($node instanceof DocumentInterface) {
      $form['pt_doccess_accessibility'] = [
        '#type' => 'details',
        '#title' => $this->t('Accessibility'),
        '#group' => 'advanced',
      ];

      // Retrieve the accessibility statistics of the document.
      $accessibility = $node->aggregateAccessibilityStatistics();

      $form['pt_doccess_accessibility']['summary'] = [
        '#theme' => 'pt_doccess_accessibility_summary',
        '#info' => $accessibility['info'] ?? 0,
        '#warning' => $accessibility['warning'] ?? 0,
        '#error' => $accessibility['error'] ?? 0,
      ];
    }

    // Alter the titles and weights of elements in the document meta
    // information section.
    if (!empty($form['meta'])) {
      $form['meta']['#title'] = $this->t('Document information');

      // When the author information is present, move it to the top.
      if (!empty($form['meta']['author'])) {
        $form['meta']['author']['#weight'] = 0;
      }

      // Update the order and title of the element indicating the publication
      // status.
      if (!empty($form['meta']['published'])) {
        $form['meta']['published']['#weight'] = 5;
        $form['meta']['published']['#title'] = $this->t('Status');
      }

      // Update the order and title of the element indicating the changed date.
      if (!empty($form['meta']['changed'])) {
        $form['meta']['changed']['#weight'] = 10;
        $form['meta']['changed']['#title'] = $this->t('Last modification');
      }

      // Update the element containing the revision information.
      if (!empty($form['revision_information'])) {
        $form['revision_information']['#weight'] = 15;
        $form['revision_information']['#type'] = 'container';
        $form['revision_information']['#group'] = 'meta';

        // Add a link to the document history.
        if (!$node->isNew()) {
          $form['revision_information']['link'] = [
            '#type' => 'link',
            '#title' => $this->t('Document history'),
            '#url' => $node->toUrl('version-history'),
          ];
        }
      }

      // The action field should be displayed in the meta section.
      if (!empty($form['field_action'])) {
        $form['field_action']['#group'] = 'meta';
      }

      // Add all action buttons/links to the meta section, except for the
      // delete link.
      if (!empty($form['actions'])) {
        $form['meta']['actions'] = $form['actions'];
        unset($form['meta']['actions']['delete']);
      }
    }

    // Remove descriptions of the path alias field.
    if (!empty($form['path'])) {
      // Change the label/title of the text field to enter the path alias.
      $form['path']['widget'][0]['alias']['#title'] = $this->t('URL');

      // Remove the descriptions and the JavaScript libraries that indicate the
      // alias that will be created.
      unset(
        $form['path']['widget'][0]['pathauto']['#description'],
        $form['path']['widget'][0]['alias']['#description'],
        $form['path']['widget'][0]['alias']['#attached'],
        $form['path']['widget'][0]['#attached']
      );

      // Add a button to copy the URL to visit the document to the clipboard.
      if (!$node->isNew()) {
        $form['path']['widget'][0]['copy'] = [
          '#theme' => 'pt_clipboard',
          '#label' => $this->t('Copy URL'),
          '#value' => $node->toUrl('canonical', [
            'absolute' => TRUE,
            'path_processing' => FALSE,
          ])->toString(),
        ];
      }
    }

    // Add an extra details section to the advanced section to show
    // additional metadata (i.e., fields).
    $form['extra'] = [
      '#type' => 'details',
      '#title' => $this->t('Metadata'),
      '#group' => 'advanced',
      '#weight' => 100,
    ];

    // The subject field should be displayed in the extra section.
    if (!empty($form['field_subject'])) {
      $form['field_subject']['#group'] = 'extra';
    }
  }

}
