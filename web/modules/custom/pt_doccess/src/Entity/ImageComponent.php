<?php

declare(strict_types=1);

namespace Drupal\pt_doccess\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\file\FileInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\pt_validation\Entity\ValidatableEntityTrait;

/**
 * Defines the image component paragraph entity bundle class.
 */
class ImageComponent extends Paragraph implements ImageComponentInterface {

  use ValidatableEntityTrait;

  /**
   * The bundle name of the image component paragraph entity.
   */
  public const BUNDLE = 'image';

  /**
   * Name of the (internal) field that contains the image.
   */
  private const IMAGE_FIELD_NAME = 'field_image';

  /**
   * {@inheritdoc}
   */
  public function getImage(): FileInterface {
    return $this->get(self::IMAGE_FIELD_NAME)->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setImage(FileInterface $image): ImageComponentInterface {
    $this->get(self::IMAGE_FIELD_NAME)->entity = $image;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAlternateText(): string {
    return $this->get(self::IMAGE_FIELD_NAME)->alt;
  }

  /**
   * {@inheritdoc}
   */
  public function setAlternateText(string $alternate_text): ImageComponentInterface {
    $this->get(self::IMAGE_FIELD_NAME)->alt = $alternate_text;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE): void {
    parent::postSave($storage, $update);

    // By logging changes for the image component, we can create statistics to
    // make the inner working of the application visible.
    if ($update) {
      // Get the parent entity of this paragraph component, which normally is a
      // document node entity.
      $parent = $this->getParentEntity();

      if (!($parent instanceof DocumentInterface)) {
        throw new \LogicException(sprintf('The parent entity of the image component with ID %d is not a document.', $this->id()));
      }

      // Get the original version of this image component.
      /** @var \Drupal\pt_doccess\Entity\ImageComponentInterface $original */
      $original = $this->original;

      if (!($original instanceof ImageComponentInterface)) {
        throw new \LogicException(sprintf('The original version of the image component with ID %d is not a heading component.', $this->id()));
      }

      // If the image has changed, log the change.
      if ($this->getImage()->id() !== $original->getImage()->id()) {
        \Drupal::logger('pt_doccess')
          ->info('User %user changed the file of an image component from %original_image to %current_image for document %document (%id).', [
            '%user' => \Drupal::currentUser()->getAccountName(),
            '%original_image' => $this->getImage()->label(),
            '%current_image' => $original->getImage()->label(),
            '%document' => $parent->getTitle(),
            '%id' => $parent->id(),
          ]);
      }

      // Log changes of the alternate text.
      if ($this->getAlternateText() !== $original->getAlternateText()) {
        \Drupal::logger('pt_doccess')
          ->info('User %user changed the alternate text of an image component from %original_alternate_text to %current_alternate_text for document %document (%id).', [
            '%user' => \Drupal::currentUser()->getAccountName(),
            '%original_alternate_text' => $this->getAlternateText(),
            '%current_alternate_text' => $original->getAlternateText(),
            '%document' => $parent->getTitle(),
            '%id' => $parent->id(),
          ]);
      }
    }
  }

}
