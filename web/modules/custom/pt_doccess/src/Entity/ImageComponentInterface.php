<?php

declare(strict_types=1);

namespace Drupal\pt_doccess\Entity;

use Drupal\file\FileInterface;

/**
 * Defines the image component paragraph entity bundle class.
 */
interface ImageComponentInterface extends ComponentInterface {

  /**
   * Gets the image of this image component.
   *
   * @return \Drupal\file\FileInterface
   *   The image of this image component.
   */
  public function getImage(): FileInterface;

  /**
   * Sets the image of this image component.
   *
   * @param \Drupal\file\FileInterface $image
   *   The image of this image component.
   *
   * @return $this
   */
  public function setImage(FileInterface $image): self;

  /**
   * Gets the alternate text of this image component.
   *
   * @return string
   *   The alternate text of this image component.
   */
  public function getAlternateText(): string;

  /**
   * Sets the alternate text of this image component.
   *
   * @param string $alternate_text
   *   The alternate text of this image component.
   *
   * @return $this
   */
  public function setAlternateText(string $alternate_text): self;

}
