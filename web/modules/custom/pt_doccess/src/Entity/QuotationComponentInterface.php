<?php

declare(strict_types=1);

namespace Drupal\pt_doccess\Entity;

/**
 * Defines the quotation component paragraph entity bundle class.
 */
interface QuotationComponentInterface extends ComponentInterface {

  /**
   * Gets the text content of this quotation component.
   *
   * @return string
   *   The quotation text.
   */
  public function getQuotation(): string;

  /**
   * Sets the text content of this quotation component.
   *
   * @param string $quotation
   *   The quotation text.
   *
   * @return $this
   */
  public function setQuotation(string $quotation): self;

}
