<?php

declare(strict_types=1);

namespace Drupal\pt_doccess\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\pt_validation\Entity\ValidatableEntityTrait;

/**
 * Defines the text component paragraph entity bundle class.
 */
class TextComponent extends Paragraph implements TextComponentInterface {

  use ValidatableEntityTrait;

  /**
   * The bundle name of the text component paragraph entity.
   */
  public const BUNDLE = 'text';

  /**
   * Name of the (internal) field that contains the text.
   */
  private const TEXT_FIELD_NAME = 'field_text';

  /**
   * Name of the (internal) field that contains the introduction flag.
   */
  private const IS_INTRODUCTION_FIELD_NAME = 'field_is_introduction';

  /**
   * {@inheritdoc}
   */
  public function getText(): string {
    return $this->get(self::TEXT_FIELD_NAME)->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setText(string $text): self {
    $this->set(self::TEXT_FIELD_NAME, [
      'value' => $text,
      'format' => 'basic_html',
    ]);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isIntroductive(): bool {
    return (bool) $this->get(self::IS_INTRODUCTION_FIELD_NAME)->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setIntroductive(bool $is_introduction = TRUE): self {
    $this->set(self::IS_INTRODUCTION_FIELD_NAME, $is_introduction);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE): void {
    parent::postSave($storage, $update);

    // By logging changes for the text component, we can create statistics
    // to make the inner working of the application visible.
    if ($update) {
      // Get the parent entity of this paragraph component, which normally is a
      // document node entity.
      $parent = $this->getParentEntity();

      if (!($parent instanceof DocumentInterface)) {
        throw new \LogicException(sprintf('The parent entity of the text component with ID %d is not a document.', $this->id()));
      }

      // Get the original version of this text component.
      /** @var \Drupal\pt_doccess\Entity\TextComponentInterface $original */
      $original = $this->original;

      if (!($original instanceof TextComponentInterface)) {
        throw new \LogicException(sprintf('The original version of the text component with ID %d is not a text component.', $this->id()));
      }

      // Keep track of changes that were made in the text.
      if ($this->getText() !== $original->getText()) {
        \Drupal::logger('pt_doccess')
          ->info('User %user updated the text of a text component for %document (%id).', [
            '%user' => \Drupal::currentUser()->getAccountName(),
            '%document' => $parent->getTitle(),
            '%id' => $parent->id(),
          ]);
      }

      // Keep track of changes that were made in the introduction flag.
      if ($this->isIntroductive() !== $original->isIntroductive()) {
        if ($this->isIntroductive()) {
          \Drupal::logger('pt_doccess')
            ->info('User %user changed a text component to introductive for %document (%id).', [
              '%user' => \Drupal::currentUser()->getAccountName(),
              '%document' => $parent->getTitle(),
              '%id' => $parent->id(),
            ]);
        }
        else {
          \Drupal::logger('pt_doccess')
            ->info('User %user changed a text component to non-introductive for %document (%id).', [
              '%user' => \Drupal::currentUser()->getAccountName(),
              '%document' => $parent->getTitle(),
              '%id' => $parent->id(),
            ]);
        }
      }
    }
  }

}
