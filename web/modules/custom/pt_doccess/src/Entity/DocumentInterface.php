<?php

declare(strict_types=1);

namespace Drupal\pt_doccess\Entity;

use Drupal\file\FileInterface;
use Drupal\node\NodeInterface;
use Drupal\pt_corporate_identity\Entity\CorporateIdentityInterface;

/**
 * Provides an interface defining a document node bundle entity.
 */
interface DocumentInterface extends NodeInterface, CorporateIdentityInterface {

  /**
   * Moderation state indicating that content needs to be imported from a file.
   */
  public const STATE_NEEDS_CONVERSION = 'needs_conversion';

  /**
   * Moderation state indicating that content has been imported from a file.
   *
   * Note: this state should only be used while updating the document in code,
   * and is needed to ensure that document validation is not triggered when
   * processing the conversion result.
   */
  public const STATE_CONVERTED = 'converted';

  /**
   * Moderation state to indicate to validate on accessibility.
   */
  public const STATE_NEEDS_ACCESSIBILITY_VALIDATION = 'needs_accessibility_validation';

  /**
   * Moderation state indicating that the content has been validated.
   *
   * This does not mean the content adheres to the accessibility requirements.
   *
   * Note: this state should only be used while updating the document in code,
   * and is needed to prevent looping when saving the result provided by the
   * Accessibility Validation API.
   */
  public const STATE_ACCESSIBILITY_VALIDATED = 'accessibility_validated';

  /**
   * Moderation state indicating that content is a concept/draft.
   */
  public const STATE_DRAFT = 'draft';

  /**
   * Moderation state indicating that content is published.
   */
  public const STATE_PUBLISHED = 'published';

  /**
   * Moderation state indicating that the content is unpublished.
   */
  public const STATE_ARCHIVED = 'archived';

  /**
   * Sets the moderation state of the document to indicate it needs conversion.
   *
   * @param \Drupal\file\FileInterface $file
   *   The office file containing the content to add.
   *
   * @return $this
   */
  public function needsConversion(FileInterface $file): self;

  /**
   * Sets the moderation state of the document to indicate it is converted.
   *
   * @return $this
   */
  public function processedConversion(): self;

  /**
   * Sets the moderation state of the document to indicate it needs validation.
   *
   * @return $this
   */
  public function needsAccessibilityValidation(): self;

  /**
   * Sets the moderation state of the document to indicate it is validated.
   *
   * @return $this
   */
  public function processedAccessibilityValidation(): DocumentInterface;

  /**
   * Returns the office file containing the content to add.
   *
   * @return \Drupal\file\FileInterface|null
   *   The office file containing the content to add, or NULL if no such file.
   */
  public function getOfficeFile(): ?FileInterface;

  /**
   * Delivers the accessibility compliance statistics of an entity.
   *
   * The statistics are expressed in the amount of 'info', 'warning' and 'error'
   * validation messages are coupled to the entity.
   *
   * @return array
   *   Associative array with keys representing the validation severity, and the
   *   value the number of times the issue occurs in the entity.
   */
  public function aggregateAccessibilityStatistics(): array;

  /**
   * Gets the accessibility compliance statistics sum of an entity.
   *
   * This gives back the sum of the amounts of 'info', 'warning' and 'error'
   * validation messages that are coupled to the entity.
   *
   * @return int
   *   Sum of validation severities.
   */
  public function getAggregateAccessibilityStatisticsCount(): int;

  /**
   * Returns the components of the document.
   *
   * @return \Drupal\pt_doccess\Entity\ComponentInterface[]
   *   The components of the document.
   */
  public function getComponents(): array;

  /**
   * Adds a heading component to the document.
   *
   * @param string $text
   *   The text of the heading.
   * @param int $level
   *   (optional) The level of the heading, i.e., <h2>-<h6>. Defaults to <h2>.
   * @param string[] $validation_messages
   *   (optional) Validation messages to add to the component. Defaults to [].
   *
   * @return $this
   */
  public function addHeadingComponent(string $text, int $level = 2, array $validation_messages = []): self;

  /**
   * Adds a text component to the document.
   *
   * @param string $text
   *   The text to add.
   * @param bool $is_introduction
   *   (optional) Whether the text is an introduction, or not. Defaults to
   *   FALSE.
   * @param string[] $validation_messages
   *   (optional) Validation messages to add to the component. Defaults to [].
   *
   * @return $this
   */
  public function addTextComponent(string $text, bool $is_introduction = FALSE, array $validation_messages = []): self;

  /**
   * Adds an image component to the document.
   *
   * @param \Drupal\file\FileInterface $image
   *   The image to add.
   * @param string $alternate_text
   *   (optional) The alternate text of the image. Defaults to ''.
   * @param string[] $validation_messages
   *   (optional) Validation messages to add to the component. Defaults to [].
   *
   * @return $this
   */
  public function addImageComponent(FileInterface $image, string $alternate_text = '', array $validation_messages = []): self;

  /**
   * Adds a list component to the document.
   *
   * @param string[] $items
   *   The list items of this list component.
   * @param string $type
   *   (optional) The type of the list, i.e., 'ul' or 'ol'. Defaults to 'ul'.
   * @param string[] $validation_messages
   *   (optional) Validation messages to add to the component. Defaults to [].
   *
   * @return $this
   */
  public function addListComponent(array $items, string $type = ListComponentInterface::UNORDERED_LIST, array $validation_messages = []): self;

  /**
   * Adds a quotation component to the document.
   *
   * @param string $text
   *   The text of the quotation.
   * @param string[] $validation_messages
   *   (optional) Validation messages to add to the component. Defaults to [].
   *
   * @return $this
   */
  public function addQuotationComponent(string $text, array $validation_messages = []): self;

  /**
   * Adds a table component to the document.
   *
   * @param string $text
   *   Text representation of the table.
   * @param string[] $validation_messages
   *   (optional) Validation messages to add to the component. Defaults to [].
   *
   * @return $this
   */
  public function addTableComponent(string $text, array $validation_messages = []): self;

}
