<?php

declare(strict_types=1);

namespace Drupal\pt_doccess\Entity;

/**
 * Defines the text component paragraph entity bundle class.
 */
interface TextComponentInterface extends ComponentInterface {

  /**
   * Gets the text content of this text component.
   *
   * @return string
   *   The text content of this text component.
   */
  public function getText(): string;

  /**
   * Sets the text content of this text component.
   *
   * @param string $text
   *   The text content of this text component.
   *
   * @return $this
   */
  public function setText(string $text): self;

  /**
   * Indicates whether the text component is an introduction.
   *
   * @return bool
   *   TRUE if the text component is an introduction, FALSE otherwise.
   */
  public function isIntroductive(): bool;

  /**
   * Indicates that the text component is an introduction.
   *
   * @param bool $is_introduction
   *   (optional) Whether this text component is an introduction. Defaults to
   *   TRUE.
   *
   * @return $this
   */
  public function setIntroductive(bool $is_introduction = TRUE): self;

}
