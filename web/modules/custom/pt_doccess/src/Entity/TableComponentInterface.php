<?php

declare(strict_types=1);

namespace Drupal\pt_doccess\Entity;

/**
 * Defines the table component paragraph entity bundle class.
 */
interface TableComponentInterface extends ComponentInterface {

  /**
   * Gets the content of this table component.
   *
   * Since a text field is used to store the table, we have reduced setting the
   * table to this sole function (to prevent unnecessary complexity). When a
   * different solution is used (e.g., using a multi-value field), this function
   * should most likely be split into multiple functions.
   *
   * @return string
   *   The table content.
   */
  public function getTable(): string;

  /**
   * Sets the table content of this table component.
   *
   * Since a text field is used to store the table, we have reduced setting the
   * table to this sole function (to prevent unnecessary complexity). When a
   * different solution is used (e.g., using a multi-value field), this function
   * should most likely be split into multiple functions.
   *
   * @param string $table
   *   The table content of this table component.
   *
   * @return $this
   */
  public function setTable(string $table): self;

}
