<?php

declare(strict_types=1);

namespace Drupal\pt_doccess\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\pt_validation\Entity\ValidatableEntityTrait;

/**
 * Defines the list component paragraph entity bundle class.
 */
class ListComponent extends Paragraph implements ListComponentInterface {

  use ValidatableEntityTrait;

  /**
   * The bundle name of the list component paragraph entity.
   */
  public const BUNDLE = 'list';

  /**
   * Name of the (internal) field that contains the list (as text).
   */
  private const LIST_FIELD_NAME = 'field_text';

  /**
   * {@inheritdoc}
   */
  public function getListType(): string {
    return str_contains($this->getList(), '<ol>') ? ListComponentInterface::ORDERED_LIST : ListComponentInterface::UNORDERED_LIST;
  }

  /**
   * {@inheritdoc}
   */
  public function getListMembers(): array {
    $list = $this->getList();

    // Remove the list tags.
    $list = str_replace(['<ol>', '</ol>', '<ul>', '</ul>'], '', $list);

    // Split the list into items.
    $items = explode('</li>', $list);

    // Remove the last item, which is an empty string.
    array_pop($items);

    // Remove the list item tags.
    foreach ($items as &$item) {
      $item = str_replace(['<li>', '</li>'], '', $item);
    }

    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function getList(): string {
    return $this->get(self::LIST_FIELD_NAME)->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setList(array $items, string $type): ListComponentInterface {
    if ($type !== self::ORDERED_LIST && $type !== self::UNORDERED_LIST) {
      throw new \InvalidArgumentException("Invalid list type, either 'ol' or 'ul' is allowed.");
    }

    // @todo Determine a valid implementation once the Twig files are defined to
    //   render a list.
    $list = '<' . $type . '>';
    foreach ($items as $item) {
      $list .= '<li>' . $item . '</li>';
    }
    $list .= '</' . $type . '>';

    $this->set(self::LIST_FIELD_NAME, [
      'value' => $list,
      'format' => 'basic_html',
    ]);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE): void {
    parent::postSave($storage, $update);

    // By logging changes for the list component, we can create statistics to
    // make the inner working of the application visible.
    if ($update) {
      // Get the parent entity of this paragraph component, which normally is a
      // document node entity.
      $parent = $this->getParentEntity();

      if (!($parent instanceof DocumentInterface)) {
        throw new \LogicException(sprintf('The parent entity of the list component with ID %d is not a document.', $this->id()));
      }

      // Get the original version of this list component.
      /** @var \Drupal\pt_doccess\Entity\ListComponentInterface $original */
      $original = $this->original;

      if (!($original instanceof ListComponentInterface)) {
        throw new \LogicException(sprintf('The original version of the list component with ID %d is not a list component.', $this->id()));
      }

      // Keep track of changes that were made in the text of the list.
      if ($this->getList() !== $original->getList()) {
        \Drupal::logger('pt_doccess')
          ->info('User %user updated the text of a list component for %document (%id).', [
            '%user' => \Drupal::currentUser()->getAccountName(),
            '%document' => $parent->getTitle(),
            '%id' => $parent->id(),
          ]);
      }
    }
  }

}
