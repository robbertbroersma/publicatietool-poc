<?php

declare(strict_types=1);

namespace Drupal\pt_doccess\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\pt_validation\Entity\ValidatableEntityTrait;

/**
 * Defines the heading component paragraph entity bundle class.
 */
class HeadingComponent extends Paragraph implements HeadingComponentInterface {

  use ValidatableEntityTrait;

  /**
   * The bundle name of the heading component paragraph entity.
   */
  public const BUNDLE = 'heading';

  /**
   * Name of the (internal) field that contains the heading data.
   */
  private const HEADING_FIELD_NAME = 'field_heading';

  /**
   * {@inheritdoc}
   */
  public function getLevel(): int {
    return match ($this->get(self::HEADING_FIELD_NAME)->size) {
      'h1' => 1,
      'h2' => 2,
      'h3' => 3,
      'h4' => 4,
      'h5' => 5,
      'h6' => 6,
      default => throw new \LogicException(sprintf('The heading level %s is not supported.', $this->get(self::HEADING_FIELD_NAME)->size)),
    };
  }

  /**
   * {@inheritdoc}
   */
  public function setLevel(int $level): self {
    if ($level < 1 || $level > 6) {
      throw new \InvalidArgumentException('The heading level must be between 1 and 6.');
    }

    $this->get(self::HEADING_FIELD_NAME)->size = 'h' . $level;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getText(): string {
    return $this->get(self::HEADING_FIELD_NAME)->text;
  }

  /**
   * {@inheritdoc}
   */
  public function setText(string $text): self {
    $this->get(self::HEADING_FIELD_NAME)->text = $text;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE): void {
    parent::postSave($storage, $update);

    // By logging changes for the header component, we can create statistics to
    // make the inner working of the application visible.
    if ($update) {
      // Get the parent entity of this paragraph component, which normally is a
      // document node entity.
      $parent = $this->getParentEntity();

      if (!($parent instanceof DocumentInterface)) {
        throw new \LogicException(sprintf('The parent entity of the heading component with ID %d is not a document.', $this->id()));
      }

      // Get the original version of this heading component.
      /** @var \Drupal\pt_doccess\Entity\HeadingComponentInterface $original */
      $original = $this->original;

      if (!($original instanceof HeadingComponentInterface)) {
        throw new \LogicException(sprintf('The original version of the heading component with ID %d is not a heading component.', $this->id()));
      }

      // Log changes to the heading level.
      if ($this->getLevel() !== $original->getLevel()) {
        \Drupal::logger('pt_doccess')
          ->info('User %user updated the level of a heading component from heading %original_level to heading %current_level for %document (%id).', [
            '%user' => \Drupal::currentUser()->getAccountName(),
            '%original_level' => $original->getLevel(),
            '%current_level' => $this->getLevel(),
            '%document' => $parent->getTitle(),
            '%id' => $parent->id(),
          ]);
      }

      // Keep track of changes that were made in the text of the heading.
      if ($this->getText() !== $original->getText()) {
        \Drupal::logger('pt_doccess')
          ->info('User %user updated the text of a heading component for %document (%id).', [
            '%user' => \Drupal::currentUser()->getAccountName(),
            '%document' => $parent->getTitle(),
            '%id' => $parent->id(),
          ]);
      }
    }
  }

}
