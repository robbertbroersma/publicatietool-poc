<?php

declare(strict_types=1);

namespace Drupal\pt_doccess\Entity;

/**
 * Defines the heading component paragraph entity bundle class.
 */
interface HeadingComponentInterface extends ComponentInterface {

  /**
   * Gets the heading level.
   *
   * @return int
   *   The heading level, i.e., 1-6.
   */
  public function getLevel(): int;

  /**
   * Sets the heading level.
   *
   * @param int $level
   *   The heading level using an integer value, between 1 and 6.
   *
   * @return $this
   */
  public function setLevel(int $level): self;

  /**
   * Gets the heading text.
   *
   * @return string
   *   The heading text.
   */
  public function getText(): string;

  /**
   * Sets the heading text.
   *
   * @param string $text
   *   The heading text.
   *
   * @return $this
   */
  public function setText(string $text): self;

}
