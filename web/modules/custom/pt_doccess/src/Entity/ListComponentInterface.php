<?php

declare(strict_types=1);

namespace Drupal\pt_doccess\Entity;

/**
 * Defines the list component paragraph entity bundle class.
 */
interface ListComponentInterface extends ComponentInterface {

  /**
   * List type to indicate an ordered list.
   */
  public const ORDERED_LIST = 'ol';

  /**
   * List type to indicate an unordered list.
   */
  public const UNORDERED_LIST = 'ul';

  /**
   * Gets the list items.
   *
   * Since a text field is used to store the list, we have reduced getting the
   * list to this sole function (to prevent unnecessary complexity). When a
   * different solution is used (e.g., using a multi-value field), this function
   * should most likely be split into two functions, one to get the list type
   * and the other to get the items.
   *
   * @return string
   *   The items of this list component.
   */
  public function getList(): string;

  /**
   * Provides the list type.
   *
   * @return string
   *   The list type of this list component, either 'ul' for an unordered list
   *   or 'ol' for an ordered list.
   */
  public function getListType(): string;

  /**
   * Gets the list members.
   *
   * @return string[]
   *   The list members of this list component.
   */
  public function getListMembers(): array;

  /**
   * Creates a list of the specified type.
   *
   * Since a text field is used to store the list, we have reduced setting the
   * list to this sole function (to prevent unnecessary complexity). When a
   * different solution is used (e.g., using a multi-value field), this function
   * should most likely be split into two functions, one to set the list type
   * and the other to set the items.
   *
   * @param string[] $items
   *   The list items of this list component.
   * @param string $type
   *   The list type of this list component, either 'ul' for an unordered list
   *   or 'ol' for an ordered list.
   *
   * @return $this
   */
  public function setList(array $items, string $type): self;

}
