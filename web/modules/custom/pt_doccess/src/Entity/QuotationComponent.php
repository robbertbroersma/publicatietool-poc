<?php

declare(strict_types=1);

namespace Drupal\pt_doccess\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\pt_validation\Entity\ValidatableEntityTrait;

/**
 * Defines the quotation component paragraph entity bundle class.
 */
class QuotationComponent extends Paragraph implements QuotationComponentInterface {

  use ValidatableEntityTrait;

  /**
   * The bundle name of the quotation component paragraph entity.
   */
  public const BUNDLE = 'quotation';

  /**
   * Name of the (internal) field that contains the quotation.
   */
  private const QUOTATION_FIELD_NAME = 'field_text';

  /**
   * {@inheritdoc}
   */
  public function getQuotation(): string {
    return $this->get(self::QUOTATION_FIELD_NAME)->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setQuotation(string $quotation): QuotationComponentInterface {
    $this->set(self::QUOTATION_FIELD_NAME, [
      'value' => $quotation,
      'format' => 'basic_html',
    ]);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE): void {
    parent::postSave($storage, $update);

    // By logging changes for the quotation component, we can create statistics
    // to make the inner working of the application visible.
    if ($update) {
      // Get the parent entity of this paragraph component, which normally is a
      // document node entity.
      $parent = $this->getParentEntity();

      if (!($parent instanceof DocumentInterface)) {
        throw new \LogicException(sprintf('The parent entity of the quotation component with ID %d is not a document.', $this->id()));
      }

      // Get the original version of this quotation component.
      /** @var \Drupal\pt_doccess\Entity\QuotationComponentInterface $original */
      $original = $this->original;

      if (!($original instanceof QuotationComponentInterface)) {
        throw new \LogicException(sprintf('The original version of the quotation component with ID %d is not a quotation component.', $this->id()));
      }

      // Keep track of changes that were made in the text of the list.
      if ($this->getQuotation() !== $original->getQuotation()) {
        \Drupal::logger('pt_doccess')
          ->info('User %user updated the text of a quotation component for %document (%id).', [
            '%user' => \Drupal::currentUser()->getAccountName(),
            '%document' => $parent->getTitle(),
            '%id' => $parent->id(),
          ]);
      }
    }
  }

}
