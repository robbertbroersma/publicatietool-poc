<?php

declare(strict_types=1);

namespace Drupal\pt_doccess\Entity;

use Drupal\paragraphs\ParagraphInterface;
use Drupal\pt_validation\Entity\ValidatableEntityInterface;

/**
 * Defines the base interface for component paragraph entity bundle class.
 */
interface ComponentInterface extends ParagraphInterface, ValidatableEntityInterface {}
