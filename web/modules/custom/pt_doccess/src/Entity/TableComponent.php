<?php

declare(strict_types=1);

namespace Drupal\pt_doccess\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\pt_validation\Entity\ValidatableEntityTrait;

/**
 * Defines the table component paragraph entity bundle class.
 */
class TableComponent extends Paragraph implements TableComponentInterface {

  use ValidatableEntityTrait;

  /**
   * The bundle name of the table component paragraph entity.
   */
  public const BUNDLE = 'table';

  /**
   * Name of the (internal) field that contains the table (as text).
   */
  private const TABLE_FIELD_NAME = 'field_text';

  /**
   * {@inheritdoc}
   */
  public function getTable(): string {
    return $this->get(self::TABLE_FIELD_NAME)->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTable(string $table): TableComponentInterface {
    $this->set(self::TABLE_FIELD_NAME, [
      'value' => $table,
      'format' => 'basic_html',
    ]);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE): void {
    parent::postSave($storage, $update);

    // By logging changes for the table component, we can create statistics
    // to make the inner working of the application visible.
    if ($update) {
      // Get the parent entity of this paragraph component, which normally is a
      // document node entity.
      $parent = $this->getParentEntity();

      if (!($parent instanceof DocumentInterface)) {
        throw new \LogicException(sprintf('The parent entity of the table component with ID %d is not a document.', $this->id()));
      }

      // Get the original version of this table component.
      /** @var \Drupal\pt_doccess\Entity\TableComponentInterface $original */
      $original = $this->original;

      if (!($original instanceof TableComponentInterface)) {
        throw new \LogicException(sprintf('The original version of the table component with ID %d is not a table component.', $this->id()));
      }

      // Keep track of changes that were made in the text of the table.
      if ($this->getTable() !== $original->getTable()) {
        \Drupal::logger('pt_doccess')
          ->info('User %user updated the text of a table component for %document (%id).', [
            '%user' => \Drupal::currentUser()->getAccountName(),
            '%document' => $parent->getTitle(),
            '%id' => $parent->id(),
          ]);
      }
    }
  }

}
