<?php

declare(strict_types=1);

namespace Drupal\pt_doccess\Entity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\file\FileInterface;
use Drupal\group\Entity\GroupMembership;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\pt_corporate_identity\Entity\CorporateIdentityTrait;
use Drupal\pt_validation\Plugin\Field\FieldType\ValidationMessageItemInterface;

/**
 * Defines the document node entity bundle class.
 */
class Document extends Node implements DocumentInterface {

  use CorporateIdentityTrait;

  /**
   * The bundle name of the document node entity.
   */
  public const BUNDLE = 'document';

  /**
   * Name of the field that contains the moderation state of the document.
   */
  private const MODERATION_STATE_FIELD_NAME = 'moderation_state';

  /**
   * Name of the field that contains a reference to the office file.
   */
  private const OFFICE_FILE_FIELD_NAME = 'field_office_file';

  /**
   * Name of the field that contains the (paragraph) component entities.
   */
  private const COMPONENTS_FIELD_NAME = 'field_content';

  /**
   * Provides the moderation state of the document.
   *
   * @return string
   *   The moderation state of the document.
   */
  private function getModerationState(): string {
    if (!$this->hasField(self::MODERATION_STATE_FIELD_NAME)) {
      throw new MissingDataException(sprintf("The entity (bundle) %s does not contain the field '%s'.", $this->bundle(), self::MODERATION_STATE_FIELD_NAME));
    }

    return $this->get(self::MODERATION_STATE_FIELD_NAME)->value;
  }

  /**
   * Sets the moderation state of the document.
   *
   * @param string $moderation_state
   *   The moderation state of the document.
   *
   * @return $this
   */
  private function setModerationState(string $moderation_state): self {
    if (!$this->hasField(self::MODERATION_STATE_FIELD_NAME)) {
      throw new MissingDataException(sprintf("The entity (bundle) %s does not contain the field '%s'.", $this->bundle(), self::MODERATION_STATE_FIELD_NAME));
    }

    $this->set(self::MODERATION_STATE_FIELD_NAME, $moderation_state);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function needsConversion(FileInterface $file): DocumentInterface {
    // @todo NLDOC-895: Add (informative) logging to this function.
    // Store the office file in the field attached to the document.
    $this->setOfficeFile($file);

    // Use the moderation state to indicate the document needs conversion. In
    // the postSave() function, the document manager will schedule the import
    // process using the RabbitMQ queue.
    $this->setModerationState(DocumentInterface::STATE_NEEDS_CONVERSION);

    return $this;
  }

  /**
   * Sets the moderation state of the document to indicate it is validated.
   *
   * @return $this
   */
  public function processedConversion(): DocumentInterface {
    // @todo NLDOC-895: Add (informative) logging to this function.
    // Delete the office file to prevent being imported again and to clean up
    // disk space.
    // Note: this is important to prevent unwanted behaviors when users revert
    // the document to the initial/earlier revision. Otherwise, the document
    // could be imported again.
    $this->getOfficeFile()?->delete();

    // Also remove the reference to the office file.
    $this->setOfficeFile(NULL);

    // Use the moderation state to indicate the document is converted.  By using
    // this temporary state, we prevent that the document is validated directly
    // after conversion. In the postSave() function, the status will be changed
    // to 'draft'.
    $this->setModerationState(DocumentInterface::STATE_CONVERTED);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function needsAccessibilityValidation(): DocumentInterface {
    // @todo NLDOC-895: Add (informative) logging to this function.
    // In case the action is set to 'document_action', the document should not
    // be validated (again); as the ECA workflow has set the proper moderation
    // state, based on the previous validation result.
    if ('document_action' !== $this->get('field_action')->value) {
      $this->setModerationState(DocumentInterface::STATE_NEEDS_ACCESSIBILITY_VALIDATION);
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function processedAccessibilityValidation(): DocumentInterface {
    // @todo NLDOC-895: Add (informative) logging to this function.
    $this->setModerationState(DocumentInterface::STATE_ACCESSIBILITY_VALIDATED);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOfficeFile(): ?FileInterface {
    if (!$this->hasField(self::OFFICE_FILE_FIELD_NAME)) {
      throw new MissingDataException(sprintf("The entity (bundle) %s does not contain the field '%s'.", $this->bundle(), self::OFFICE_FILE_FIELD_NAME));
    }

    return $this->get(self::OFFICE_FILE_FIELD_NAME)->entity;
  }

  /**
   * Sets the office file containing the content to add.
   *
   * @param \Drupal\file\FileInterface|null $file
   *   The office file containing the content to add.
   *
   * @return $this
   */
  private function setOfficeFile(?FileInterface $file): DocumentInterface {
    if (!$this->hasField(self::OFFICE_FILE_FIELD_NAME)) {
      throw new MissingDataException(sprintf("The entity (bundle) %s does not contain the field '%s'.", $this->bundle(), self::OFFICE_FILE_FIELD_NAME));
    }

    $this->set(self::OFFICE_FILE_FIELD_NAME, $file);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function aggregateAccessibilityStatistics(): array {
    // @todo NLDOC-895: Add (informative) logging to this function.
    $accessibility = [
      ValidationMessageItemInterface::SEVERITY_INFO => 0,
      ValidationMessageItemInterface::SEVERITY_WARNING => 0,
      ValidationMessageItemInterface::SEVERITY_ERROR => 0,
    ];

    // Process al the components and aggregate the severity status of the
    // validation messages.
    foreach ($this->getComponents() as $component) {
      foreach ($component->getValidationMessages() as $validation_message) {
        $severity = $validation_message->getSeverity();

        // In case the severity is unknown, we log an error and mark it as
        // informative in order to generate a correct total statistic and log
        // an error.
        if (NULL === $severity) {
          \Drupal::logger('pt_doccess')
            ->error("Unknown validation message '%code' on document %document_title (%document_id) for component %component_bundle (%component_id).", [
              '%code' => $validation_message->get('code')->getValue(),
              '%document_title' => $this->getTitle(),
              '%document_id' => $this->id(),
              '%component_id' => $component->id(),
              '%component_bundle' => $component->bundle(),
            ]);

          // @todo NLDOC-821: determine the wanted behavior for unknown
          //   validation messages.
          $severity = ValidationMessageItemInterface::SEVERITY_INFO;
        }

        $accessibility[$severity]++;
      }
    }

    return $accessibility;
  }

  /**
   * {@inheritdoc}
   */
  public function getAggregateAccessibilityStatisticsCount(): int {
    return array_sum($this->aggregateAccessibilityStatistics());
  }

  /**
   * {@inheritdoc}
   */
  public function getComponents(): array {
    if (!$this->hasField(self::COMPONENTS_FIELD_NAME)) {
      throw new MissingDataException(sprintf("The entity (bundle) %s does not contain the field '%s'.", $this->bundle(), self::COMPONENTS_FIELD_NAME));
    }

    return $this->get(self::COMPONENTS_FIELD_NAME)->referencedEntities();
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    // Ensure the title text is not more than 255 characters long.
    // @todo NLDOC-468: is created to determine the wanted behavior for long
    //   heading and titles. Most likely, the title stores a truncated version
    //   of the title and the full title is stored in a separate heading field.
    $title = mb_substr($title, 0, 255);

    return parent::setTitle($title);
  }

  /**
   * {@inheritdoc}
   */
  public function addHeadingComponent(string $text, int $level = 2, array $validation_messages = []): self {
    if ($level < 2 || $level > 6) {
      throw new \InvalidArgumentException('Invalid heading level, documents only allow <h2> - <h6>.');
    }

    // Ensure the heading text is not more than 255 characters long.
    // @todo NLDOC-468: is created to determine the wanted behavior for long
    //   heading and titles. Most likely, the heading field should be adjusted
    //   to allow longer heading text.
    $text = mb_substr($text, 0, 255);

    /** @var \Drupal\pt_doccess\Entity\HeadingComponentInterface $heading_component */
    $heading_component = $this->createComponent(HeadingComponent::BUNDLE);
    $heading_component->setText($text);
    $heading_component->setLevel($level);
    $heading_component->setValidationMessages($validation_messages);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addTextComponent(string $text, bool $is_introduction = FALSE, array $validation_messages = []): self {
    /** @var \Drupal\pt_doccess\Entity\TextComponentInterface $text_component */
    $text_component = $this->createComponent(TextComponent::BUNDLE);
    $text_component->setText($text);
    $text_component->setIntroductive($is_introduction);
    $text_component->setValidationMessages($validation_messages);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addImageComponent(FileInterface $image, string $alternate_text = '', array $validation_messages = []): self {
    /** @var \Drupal\pt_doccess\Entity\ImageComponentInterface $image_component */
    $image_component = $this->createComponent(ImageComponent::BUNDLE);
    $image_component->setImage($image);
    $image_component->setAlternateText($alternate_text);
    $image_component->setValidationMessages($validation_messages);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addListComponent(array $items, string $type = ListComponentInterface::UNORDERED_LIST, array $validation_messages = []): self {
    /** @var \Drupal\pt_doccess\Entity\ListComponentInterface $list_component */
    $list_component = $this->createComponent(ListComponent::BUNDLE);
    $list_component->setList($items, $type);
    $list_component->setValidationMessages($validation_messages);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addQuotationComponent(string $text, array $validation_messages = []): self {
    /** @var \Drupal\pt_doccess\Entity\QuotationComponentInterface $quotation_component */
    $quotation_component = $this->createComponent(QuotationComponent::BUNDLE);
    $quotation_component->setQuotation($text);
    $quotation_component->setValidationMessages($validation_messages);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addTableComponent(string $text, array $validation_messages = []): self {
    /** @var \Drupal\pt_doccess\Entity\TableComponentInterface $table_component */
    $table_component = $this->createComponent(TableComponent::BUNDLE);
    $table_component->setTable($text);
    $table_component->setValidationMessages($validation_messages);

    return $this;
  }

  /**
   * Creates a new paragraph component and attaches it to this document entity.
   *
   * @param string $component_type
   *   The paragraph component type.
   *
   * @return \Drupal\paragraphs\ParagraphInterface
   *   The paragraph bundle that corresponds to the given component type, which
   *   is attached to the 'field_content' field of this document.
   */
  protected function createComponent(string $component_type): ParagraphInterface {
    // Create the paragraph component entity.
    $component = Paragraph::create([
      'type' => $component_type,
      'parent_id' => $this->id(),
      'parent_type' => $this->getEntityTypeId(),
      'parent_field_name' => self::COMPONENTS_FIELD_NAME,
    ]);

    // Add the paragraph to this document.
    $this->get(self::COMPONENTS_FIELD_NAME)->appendItem($component);

    return $component;
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation = 'view', AccountInterface $account = NULL, $return_as_object = FALSE): bool|AccessResultInterface {
    $moderation_state = $this->getModerationState();

    // Prevent editing documents that are being converted or validated (i.e.,
    // not having the status 'draft', 'published' or 'archived').
    // Note: the inner-state 'accessibility_validated' should under normal
    // operation not be available/visible to the user; however, to ensure that
    // the workflow defined using the ECA module works correctly, this state
    // should also be allowed (for now).
    if ('update' === $operation && DocumentInterface::STATE_DRAFT !== $moderation_state && DocumentInterface::STATE_PUBLISHED !== $moderation_state && DocumentInterface::STATE_ACCESSIBILITY_VALIDATED !== $moderation_state && DocumentInterface::STATE_ARCHIVED !== $moderation_state) {
      return $return_as_object ? AccessResult::forbidden('Updating the document is not allowed, the contents of an office file should be (automatically) imported first.') : FALSE;
    }

    return parent::access($operation, $account, $return_as_object);
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    switch ($this->getModerationState()) {
      case DocumentInterface::STATE_CONVERTED:
        // After the completion of a conversion process, the document should be
        // marked as a concept (i.e., draft).
        $this->setModerationState(DocumentInterface::STATE_DRAFT);
        break;

      case DocumentInterface::STATE_DRAFT:
      case DocumentInterface::STATE_PUBLISHED:
      case DocumentInterface::STATE_ARCHIVED:
        // In case the document is a 'draft' or 'published', set the moderation
        // state to 'needs_accessibility_validation' to ensure that in the post-
        // save hook the Accessibility Validation API is used to determine/
        // update the accessibility properties/status of the document.
        $this->needsAccessibilityValidation();
        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE): void {
    parent::postSave($storage, $update);

    // When a new document has been created, make it part of the organization
    // the author is part of.
    if (!$update) {
      $this->assignToAuthorOrganizations();
    }

    $this->set('field_accessibility_errors', $this->getAggregateAccessibilityStatisticsCount());

    $moderation_state = $this->getModerationState();

    // After saving the document completely, determine whether the contents of
    // the office file should be imported or if accessibility checks are needed.
    if (DocumentInterface::STATE_NEEDS_CONVERSION === $moderation_state) {
      \Drupal::service('pt_doccess.document_manager')->scheduleDocumentConversion($this);
    }
    elseif (DocumentInterface::STATE_NEEDS_ACCESSIBILITY_VALIDATION === $moderation_state) {
      // @todo NLDOC-581: determine/refactor below solution to keep the original
      //   variable as this is a "save" in a "save" process and normally in the
      //   post-save Drupal unsets the original variable (which leeds to errors
      //   when continuing the original post-save process).
      $temp = NULL;
      if ($this->original) {
        $temp = clone $this->original;
      }
      \Drupal::service('pt_doccess.document_manager')->validateDocumentContent($this);
      if ($temp) {
        $this->original = $temp;
      }
    }

    // By logging changes made to the document, we can create statistics to make
    // the inner working of the application visible.
    if ($update) {
      // Get the original version of this document.
      /** @var \Drupal\pt_doccess\Entity\DocumentInterface $original */
      $original = $this->original;

      // Add a log message when the title is changed.
      if ($this->getTitle() !== $original->getTitle()) {
        \Drupal::logger('pt_doccess')
          ->info('User %user changed the title of document %id from %old_title to %new_title.', [
            '%user' => \Drupal::currentUser()->getAccountName(),
            '%id' => $this->id(),
            '%old_title' => $original->getTitle(),
            '%new_title' => $this->getTitle(),
          ]);
      }
    }
  }

  /**
   * Makes the document a member of the author's organizations.
   *
   * @todo NLDOC-1004: ideally this should be implemented using the ECA workflow
   *   module (if that get used for the rest of the workflows), this reduces the
   *   complexity in the custom code and stimulates code reuse.
   *
   * @return $this
   */
  private function assignToAuthorOrganizations(): self {
    // The document should be assigned to the same organization(s), the author
    // of the document is part of.
    $account = $this->getOwner();

    // Load all membership entities of the document's author.
    /** @var \Drupal\group\Entity\GroupMembershipInterface[] $user_memberships */
    $user_memberships = GroupMembership::loadByUser($account);

    // @todo NLDOC-1003: determine/implement the desired behavior when a
    //   document cannot be assigned to organizations (i.e., the author is not
    //   is not a member of any organization).
    // Add a document group node relationship for each organization the author
    // is a member of.
    foreach ($user_memberships as $membership) {
      $organization = $membership->getGroup();
      $organization->addRelationship($this, 'group_node:' . $this->bundle());
    }

    return $this;
  }

}
