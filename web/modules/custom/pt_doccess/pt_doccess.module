<?php

/**
 * @file
 * Primary module hooks for Publicatietool doccess module.
 */

declare(strict_types=1);

use Drupal\Core\Form\FormStateInterface;
use Drupal\pt_doccess\EntityOperations;
use Drupal\pt_doccess\EntityTypeInfo;

/**
 * Implements hook_theme().
 */
function pt_doccess_theme($existing, $type, $theme, $path) {
  return [
    'pt_doccess_accessibility_summary' => [
      'variables' => [
        'info' => 0,
        'warning' => 0,
        'error' => 0,
        'attributes' => [],
      ],
    ],
  ];
}

/**
 * Implements hook_entity_bundle_info_alter().
 */
function pt_doccess_entity_bundle_info_alter(array &$bundles): array {
  return \Drupal::service('class_resolver')
    ->getInstanceFromDefinition(EntityTypeInfo::class)
    ->entityBundleInfoAlter($bundles);
}

/**
 * Implements hook_entity_extra_field_info().
 */
function pt_doccess_entity_extra_field_info() {
  return \Drupal::service('class_resolver')
    ->getInstanceFromDefinition(EntityTypeInfo::class)
    ->entityExtraFieldInfo();
}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for \Drupal\node\NodeForm.
 */
function pt_doccess_form_node_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  \Drupal::service('class_resolver')
    ->getInstanceFromDefinition(EntityOperations::class)
    ->nodeFormAlter($form, $form_state, $form_id);
}
