<?php

declare(strict_types = 1);

namespace Drupal\pt_dragzone\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\file\Upload\FormUploadedFile;
use Drupal\pt_dragzone\Service\DragzoneFileManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns responses for Publicatietool dragzone routes.
 */
class DragzoneController extends ControllerBase {

  /**
   * Manages the file upload process using the drag-and-drop functionality.
   *
   * @var \Drupal\pt_dragzone\Service\DragzoneFileManagerInterface
   */
  protected DragzoneFileManagerInterface $dragzoneFileManager;

  /**
   * Constructs a DragzoneController object.
   *
   * @param \Drupal\pt_dragzone\Service\DragzoneFileManagerInterface $dragzone_file_manager
   *   Manages the file upload process using the drag-and-drop functionality.
   */
  public function __construct(DragzoneFileManagerInterface $dragzone_file_manager) {
    $this->dragzoneFileManager = $dragzone_file_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('pt_dragzone.file_manager')
    );
  }

  /**
   * Handles the file upload using the drag-and-drop functionality.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request object.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response object.
   */
  public function upload(Request $request): Response {
    // @todo NLDOC-894: Add (informative) logging to this function.
    $uploaded_file = $request->files->get('file');

    // Ensure that the correct/expected object is received.
    if (!$uploaded_file instanceof UploadedFile) {
      throw new \LogicException('No valid file was uploaded in the provided request.');
    }

    // Use the bridge class to process the file upload process.
    $this->dragzoneFileManager->processFileUpload(new FormUploadedFile($uploaded_file));

    // @todo NLDOC-897: Add a proper response.
    return new Response();
  }

}
