<?php

declare(strict_types=1);

namespace Drupal\pt_dragzone\Event;

/**
 * Defines events for the drag-and-drop functionality.
 */
class DragzoneEvents {

  /**
   * The name of the event fired when a file uploaded using drag-and-drop.
   *
   * This event allows modules to react to file uploads that used the
   * drag-and-drop functionality provided by this module. The event listener
   * method receives a \Drupal\pt_doccess\Event\DragzoneFileUploadEvent
   * instance.
   *
   * @Event
   *
   * @see \Drupal\pt_dragzone\Service\DragzoneFileManager::processFileUpload()
   */
  public const DRAGZONE_FILE_UPLOAD = 'pt_dragzone.file_upload';

}
