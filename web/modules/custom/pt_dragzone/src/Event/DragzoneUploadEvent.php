<?php

declare(strict_types=1);

namespace Drupal\pt_dragzone\Event;

use Drupal\file\FileInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Provides an event that is fired when a file is uploaded using drag-and-drop.
 */
class DragzoneUploadEvent extends Event {

  /**
   * The entity representing the uploaded file.
   *
   * @var \Drupal\file\FileInterface
   */
  protected FileInterface $file;

  /**
   * Constructs a new DragzoneUploadEvent object.
   *
   * @param \Drupal\file\FileInterface $file
   *   The entity representing the uploaded file.
   */
  public function __construct(FileInterface $file) {
    $this->file = $file;
  }

  /**
   * Returns the entity representing the uploaded file.
   *
   * @return \Drupal\file\FileInterface
   *   The uploaded file.
   */
  public function getFile(): FileInterface {
    return $this->file;
  }

}
