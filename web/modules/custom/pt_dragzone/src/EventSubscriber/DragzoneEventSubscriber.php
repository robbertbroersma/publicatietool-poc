<?php

declare(strict_types = 1);

namespace Drupal\pt_dragzone\EventSubscriber;

use Drupal\vendor_stream_wrapper\Event\VendorStreamWrapperCollectSafeListRegexPatternsEvent;
use Drupal\vendor_stream_wrapper\Event\VendorStreamWrapperEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Provides safe-list patterns for the drag-and-drop library files.
 */
class DragzoneEventSubscriber implements EventSubscriberInterface {

  /**
   * Sets the patterns for vendor files that should be publicly accessible.
   *
   * @param \Drupal\vendor_stream_wrapper\Event\VendorStreamWrapperCollectSafeListRegexPatternsEvent $event
   *   The event object storing the patterns for files/directories of the vendor
   *   directory that should be publicly accessible.
   */
  public function onCollectSafeListRegexPatterns(VendorStreamWrapperCollectSafeListRegexPatternsEvent $event): void {
    $event->getVendorStreamWrapperManager()->addSafeListRegexPatterns([
      '/^enyo\/dropzone\/dist\/(basic|dropzone)\.css$/',
      '/^enyo\/dropzone\/dist\/dropzone\-min\.js$/',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      VendorStreamWrapperEvents::COLLECT_SAFE_LIST_REGEX_PATTERNS => ['onCollectSafeListRegexPatterns'],
    ];
  }

}
