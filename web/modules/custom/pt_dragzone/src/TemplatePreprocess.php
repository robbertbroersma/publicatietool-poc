<?php

declare(strict_types=1);

namespace Drupal\pt_dragzone;

use Drupal\Component\Utility\Html;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;

/**
 * Defines a class to define template preprocessors.
 *
 * @internal
 */
class TemplatePreprocess {

  use StringTranslationTrait;

  /**
   * Prepares variables for the pt_dragzone template.
   *
   * Default template: pt-dragzone.html.twig.
   *
   * @param array $variables
   *   An associative array containing:
   *   - attributes: An associative array of HTML attributes to apply to the
   *     drag-and-drop (wrapper) element.
   */
  public function templatePreprocessPtDragzone(array &$variables) {
    // @todo NLDOC-898: Implement this functionality using a render element.
    // Ensure that an id is set for the drag-and-drop element.
    if (empty($variables['attributes']['id'])) {
      $variables['attributes']['id'] = Html::getUniqueId('pt-dragzone');
    }

    // Attach the class needed by the JavaScript to find the drag-and-drop
    // element.
    $variables['attributes']['class'][] = 'pt-dragzone';

    // Since the upload path that the drag-and-drop upload functionality uses is
    // protected using an CSRF token, generate the path and store it as an
    // attribute on the drag-and-drop element.
    $variables['attributes']['pt-dragzone-upload-url'] = Url::fromRoute('pt_dragzone.upload')->toString();

    // Attach the library needed for the drag-and-drop functionality.
    $variables['#attached']['library'][] = 'pt_dragzone/pt-dragzone';
  }

}
