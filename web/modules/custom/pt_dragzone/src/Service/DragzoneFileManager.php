<?php

declare(strict_types = 1);

namespace Drupal\pt_dragzone\Service;

use Drupal\file\Upload\FileUploadHandler;
use Drupal\file\Upload\UploadedFileInterface;
use Drupal\pt_dragzone\Event\DragzoneEvents;
use Drupal\pt_dragzone\Event\DragzoneUploadEvent;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Manages the file upload process using the drag-and-drop functionality.
 */
class DragzoneFileManager implements DragzoneFileManagerInterface {

  /**
   * Handles validating and creating file entities from file uploads.
   *
   * @var \Drupal\file\Upload\FileUploadHandler
   */
  protected FileUploadHandler $fileUploadHandler;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * Constructs a DragzoneFileManager object.
   *
   * @param \Drupal\file\Upload\FileUploadHandler $file_upload_handler
   *   Handles validating and creating file entities from file uploads.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(FileUploadHandler $file_upload_handler, EventDispatcherInterface $event_dispatcher) {
    $this->fileUploadHandler = $file_upload_handler;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public function processFileUpload(UploadedFileInterface $file): void {
    // Use the file upload handler to store the uploaded file as a temporary
    // object and prevent performing validation on its extension, as this should
    // be handled by the logic handling the uploaded file event.
    $file_upload_result = $this->fileUploadHandler->handleFileUpload($file, ['file_validate_extensions' => []]);

    // Dispatch an event to allow other modules to react to the uploaded file.
    $event = new DragzoneUploadEvent($file_upload_result->getFile());
    $this->eventDispatcher->dispatch($event, DragzoneEvents::DRAGZONE_FILE_UPLOAD);

    // @todo NLDOC-894: Add (informative) logging to this function.
  }

}
