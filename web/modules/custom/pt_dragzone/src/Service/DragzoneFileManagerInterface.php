<?php

declare(strict_types = 1);

namespace Drupal\pt_dragzone\Service;

use Drupal\file\Upload\UploadedFileInterface;

/**
 * Manages the file upload process using the drag-and-drop functionality.
 */
interface DragzoneFileManagerInterface {

  /**
   * Handles the file upload and dispatches event after completion.
   *
   * @param \Drupal\file\Upload\UploadedFileInterface $file
   *   The uploaded file.
   */
  public function processFileUpload(UploadedFileInterface $file): void;

}
