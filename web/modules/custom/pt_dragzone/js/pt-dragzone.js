/**
 * @file
 * Provides JavaScript behaviors for the Publicatietool dragzone module.
 */
(function (Drupal, once) {

  "use strict";

  /**
   * Attaches the dragzone functionality.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the functionality to upload files using drag-and-drop.
   */
  Drupal.behaviors.ptDragzone = {
    attach: function (context) {
      // Select all drag-and-drop elements, once.
      const dragzones = once('pt-dragzone', '.pt-dragzone', context);

      // Initialize the drag-and-drop functionality for each element.
      dragzones.forEach(function (element) {

        // Enable basic styling by setting the 'dropzone' class.
        element.classList.add('dropzone');

        // Initialize the Dropzone object.
        const docDropzone = new Dropzone('#' + element.id, {
          url: element.getAttribute('pt-dragzone-upload-url'),
          disablePreviews: true,

          init: function () {

            // Create a progressbar that shows the total upload progress.
            this.on("totaluploadprogress", function (progress) {
              var preview = document.getElementById("progress-container");
              preview.style.display = "block";

              // Get the number of files in queue.
              var count = this.getAcceptedFiles().length;
              preview.querySelector(".progress--bar").style.width = `${progress}%`;
              preview.querySelector(".progress--text").textContent = `${count} bestanden uploaden`;
              if (progress >= 100) {

                setTimeout(() => {
                  preview.querySelector(".procress-done").style.opacity = `1`;
                  preview.querySelector(".progress").style.opacity = `0`;
                  preview.querySelector(".progress--text").textContent = `${count} bestanden succesvol geupload`;
                }, 1000);

                // Reload the page when all files are uploaded.
                setTimeout(() => {
                  //@todo reload with an ajax call.
                  window.location.reload();
                }, 3000);

              }
            });
          }
        });

      });
    }
  };

})(Drupal, once);
