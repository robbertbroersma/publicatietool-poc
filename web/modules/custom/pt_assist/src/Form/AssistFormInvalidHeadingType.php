<?php

declare(strict_types=1);

namespace Drupal\pt_assist\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the assist error form.
 */
class AssistFormInvalidHeadingType extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pt_assist_form_invalid_heading_type';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $paragraphs_item_id = NULL) {
    // Temporary hardcoded input data.
    $data = [
      // Data from Rule engine.
      'type' => 'title',
      'id' => 'field-content-3',
      'attribute' => 'h4',
      'validation_type' => 'error',
      'error_type' => 'wrong attribute',

      // Data from current paragraph:
      // field_content[][subform][field_heading][0][container][text].
      'current_content' => 'Cras mattis consectetur purus sit.',
      'content_fix' => 'Cras mattis consectetur purus sit.',
      // Get this from the Rule engine?
      'attribute_fix' => 'h3',

      // Data from message table created in Drupal.
      'validation_message' => 'Er is een onjuist gebruik van titels in deze tekst. De volgorde van de niveaus van de titels is niet correct.',
      'validation_message_fix' => 'We denken dat deze titel niveau 3 hoort te zijn, zullen we dit voor je aanpassen?',
    ];

    // Step 1 validation message.
    $form['step_1'] = [
      '#type' => 'fieldset',
      '#states' => [
        'invisible' => [
          ':input[name="confirm_finding"]' => ['value' => 'yes'],
        ],
      ],
    ];

    $form['step_1']['info'] = [
      '#type' => 'item',
      '#markup' => '<p>Validatietype: ' . $data['error_type'] . '</p>' .
      '<p>paragraaf ID: ' . $paragraphs_item_id . '</p>',
    ];
    $form['step_1']['validation_message'] = [
      '#type' => 'item',
      '#markup' => '<p>' . $data['validation_message'] . '</p>',
    ];

    $form['step_1']['current_content'] = [
      '#type' => 'item',
      '#markup' => '<h4>' . $data['current_content'] . '</h4>',
    ];

    // Conditional field for showing step 2.
    $form['step_1']['confirm_finding'] = [
      '#type' => 'radios',
      '#title' => $this->t('Do you want to change this?'),
      '#options' => [
        'yes' => $this->t('yes'),
        'no' => $this->t('no'),
      ],
      '#attributes' => [
        'class' => ['confirm_finding'],
      ],
    ];

    // Step 2: validation fix proposal.
    $form['step_2'] = [
      '#type' => 'fieldset',
      // Shown when 'Yes' is selected in field 'confirm_finding'.
      '#states' => [
        'visible' => [
          ':input[name="confirm_finding"]' => ['value' => 'yes'],
        ],
      ],
    ];

    $form['step_2']['validation_message_fix'] = [
      '#type' => 'item',
      '#markup' => '<p>' . $data['validation_message_fix'] . '</p>',
    ];

    $form['step_2']['content_fix'] = [
      '#type' => 'item',
      '#markup' => '<h3>' . $data['content_fix'] . '</h3>',
    ];

    $form['step_2']['user_actions'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'user-actions',
        ],
      ],
    ];

    $form['step_2']['user_actions']['yes'] = [
      '#type' => 'submit',
      '#value' => $this->t('yes'),
      '#ajax' => [
        'callback' => [$this, 'ajaxSubmit'],
        'event' => 'click',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
      '#attributes' => [
        'class' => ['button', 'button--success'],
      ],
    ];

    $form['step_2']['user_actions']['no'] = [
      '#type' => 'submit',
      '#value' => $this->t('no'),
      '#ajax' => [
        'callback' => [$this, 'closeModal'],
        'event' => 'click',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
      '#attributes' => [
        'class' => ['button', 'button--error'],
      ],
    ];

    $form['#attributes']['class'][] = 'assist-form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * Ajax callback triggered by the 'yes' submit button element.
   *
   * @param array $form
   *   Drupal Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form State.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax data in the response.
   */
  public function ajaxSubmit(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(
      new CloseModalDialogCommand()
    );
    $response->addCommand(
      // Replaces the content of the ajax link.
      // @todo get the id for the paragraph field that needs the fix and pass as
      //   the first argument.
      new ReplaceCommand('#open-assist', $this->buildParagraphContent($form_state)),
    );
    return $response;
  }

  /**
   * Ajax callback triggered by the 'no' submit button element.
   *
   * @param array $form
   *   Drupal Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form State.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax data in the response.
   */
  public function closeModal(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(
      new CloseModalDialogCommand()
    );
    return $response;
  }

  /**
   * Constructs the content to show to the user after clicking 'yes'.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form State.
   *
   * @return array
   *   Render array.
   */
  private function buildParagraphContent(FormStateInterface $form_state) {
    // @todo Get the fixed paragraph field and replace the current one.
    // Update widget link.
    $resolvedLink = $this->t('All issues are resolved.');

    $content = '<div class="messages messages--status">' . $resolvedLink . '</div>';

    return [
      '#markup' => $content,
    ];
  }

}
