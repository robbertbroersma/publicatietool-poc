<?php

declare(strict_types=1);

namespace Drupal\pt_assist\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the assist error form.
 */
class AssistFormMissingAltText extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected FileUrlGeneratorInterface $fileUrlGenerator;

  /**
   * Constructs a new AssistFormMissingAltText.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file URL generator.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FileUrlGeneratorInterface $file_url_generator) {
    $this->entityTypeManager = $entity_type_manager;
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    $form = new static(
      $container->get('entity_type.manager'),
      $container->get('file_url_generator')
    );

    $form->setStringTranslation($container->get('string_translation'));
    $form->setMessenger($container->get('messenger'));

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pt_assist_form_missing_alt_text';
  }

  /**
   * Set the parent entity of the paragraph.
   *
   * @param int $pid
   *   Paragraph ID.
   *
   * @return $this
   *
   * @todo move this function to a custom class or use/extent the
   * ImageComponent class from the pt_doccess module.
   *
   * @todo use dependency injection instead of File::load.
   */
  public function getMedia($pid) {
    $path = '';
    $paragraph = Paragraph::load($pid);
    if (!empty($paragraph)) {
      $paragraphField = $paragraph->get('field_image');
      $paragraphFieldImage = $paragraphField->getValue();
      $paragraphFieldImageTid = $paragraphFieldImage[0]['target_id'];
      $file = $this->entityTypeManager->getStorage('file')->load($paragraphFieldImageTid);
      $uri = $file->getFileUri();
      $path = $this->fileUrlGenerator->generateAbsoluteString($uri);
    }
    return $path;

  }

  /**
   * {@inheritdoc}
   *
   * @todo move this function to a custom a class.
   */
  public function setAltText($pid, $altText) {
    $paragraph = Paragraph::load($pid);
    if (!empty($paragraph)) {
      $paragraphField = $paragraph->get('field_image');
      $paragraphField->alt = $altText;
      $paragraph->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $identifier = NULL, $paragraphs_item_id = NULL) {
    $data = [
      // Text strings for the 'missing alt text' assist form.
      // @todo Create a config object or make a constants.
      // @todo Use English source text and make it translatable.
      'validation_message' => 'Deze afbeelding mist een alternatieve tekst.',
      'validation_message_question' => 'Is deze afbeelding decoratief?',
      'validation_message_info' => 'Een afbeelding is decoratief zodra deze geen visuele of tekstuele informatie bevat die de lezer nodig heeft om het document te begrijpen.',
      'validation_message_no_fix' => 'Deze afbeelding heeft geen alternatieve tekst nodig.',
      'validation_message_fix' => 'Geef een beschrijving van deze afbeelding.',
      'validation_message_fix_info' => 'Dit is een informatieve afbeelding, om toegankelijk te zijn hoort deze te worden voorzien van een alternatieve tekst. Beschrijf wat de gebruiker in de afbeelding ziet of welke tekst hier in te lezen is.',
    ];

    // The paragraph ID from where the assisant is opened.
    $form['paragraph_id'] = [
      '#type' => 'hidden',
      '#value' => $paragraphs_item_id,
    ];

    // Form message.
    $form['message'] = [
      '#type' => 'item',
      '#markup' => $data['validation_message'],
    ];

    $form['validation_message_question'] = [
      '#type' => 'item',
      '#markup' => $data['validation_message_question'],
    ];

    $form['validation_message_question_yes'] = [
      '#type' => 'item',
      '#markup' => $this->t("yes"),
      '#states' => [
        'visible' => [
          ':input[name="confirm_finding"]' => ['value' => 'yes'],
        ],
      ],
    ];

    $form['validation_message_question_no'] = [
      '#type' => 'item',
      '#markup' => $this->t("no"),
      '#states' => [
        'visible' => [
          ':input[name="confirm_finding"]' => ['value' => 'no'],
        ],
      ],
    ];

    // Show image.
    $form['current_content_image'] = [
      '#type' => 'item',
    // @todo create a image style and view mode for this.
      '#markup' => '<img src="' . $this->getMedia($paragraphs_item_id) . '">',
    ];

    // Step 1 validation message.
    $form['step_1'] = [
      '#type' => 'fieldset',
      '#states' => [
        'invisible' => [
          [':input[name="confirm_finding"]' => ['value' => 'yes']],
          'or',
          [':input[name="confirm_finding"]' => ['value' => 'no']],
        ],
      ],
    ];

    // Conditional field for showing step 2.
    $form['step_1']['confirm_finding'] = [
      '#type' => 'radios',
      '#title' => $data['validation_message_info'],
      '#options' => [
        'yes' => $this->t('yes'),
        'no' => $this->t('no'),
      ],
      '#attributes' => [
        'class' => ['confirm_finding'],
      ],
    ];

    // Step 2: validation fix proposal.
    $form['step_2'] = [
      '#type' => 'fieldset',
      // Shown when 'Yes' is selected in field 'confirm_finding'.
      '#states' => [
        'visible' => [
          ':input[name="confirm_finding"]' => ['value' => 'yes'],
        ],
      ],
    ];

    $form['step_2']['validation_message_no_fix'] = [
      '#type' => 'item',
      '#markup' => '<p>' . $data['validation_message_no_fix'] . '</p>',
    ];

    $form['step_2']['user_actions'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'user-actions',
        ],
      ],
    ];

    $form['step_2']['user_actions']['apply'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply'),
      '#ajax' => [
        'callback' => [$this, 'closeModal'],
        'event' => 'click',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
      '#attributes' => [
        'class' => ['button', 'button--primary'],
      ],
    ];

    // Step 3.
    $form['step_3'] = [
      '#type' => 'fieldset',
      // Shown when 'no' is selected in field 'confirm_finding'.
      '#states' => [
        'visible' => [
          ':input[name="confirm_finding"]' => ['value' => 'no'],
        ],
      ],
    ];

    $form['step_3']['validation_message_fix'] = [
      '#type' => 'item',
      '#markup' => '<p>' . $data['validation_message_fix'] . '</p>',
    ];

    $form['step_3']['validation_message_fix_info'] = [
      '#type' => 'item',
      '#markup' => '<p>' . $data['validation_message_fix_info'] . '</p>',
    ];

    $form['step_3']['alt_text'] = [
      '#type' => 'textfield',
      '#size' => '60',
      '#title' => $this->t('Image alt text'),
      '#placeholder' => $this->t('Image alt text'),
    ];

    $form['step_3']['user_actions'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'user-actions',
        ],
      ],
    ];
    $form['step_3']['user_actions']['confirm'] = [
      '#type' => 'submit',
      '#value' => $this->t('Toepassen'),
      '#ajax' => [
        'callback' => [$this, 'ajaxSubmit'],
        'event' => 'click',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
      '#attributes' => [
        'class' => ['button', 'button--primary'],
      ],
    ];

    $form['#attributes']['class'][] = 'assist-form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * Ajax callback triggered by the 'yes' submit button element.
   *
   * @param array $form
   *   Drupal Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form State.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax data in the response.
   */
  public function ajaxSubmit(array &$form, FormStateInterface $form_state) {
    $this->setAltText($form_state->getValue('paragraph_id'), $form_state->getValue('alt_text'));
    $response = new AjaxResponse();
    $response->addCommand(
      new CloseModalDialogCommand()
    );
    $response->addCommand(
      // Replaces the content of the ajax link.
      // @todo get the id for the paragraph field that needs the fix and pass as
      //   the first argument.
      new ReplaceCommand('.field--name-field-validation-messages', $this->buildParagraphContent($form_state)),
    );
    $response->addCommand(
      // Replaces the alt text input field.
      new ReplaceCommand('.image-widget-data .form-text', '<input class="form-text" type="text" value="' . $form_state->getValue('alt_text') . '" disabled>'),
    );
    return $response;
  }

  /**
   * Ajax callback triggered by the 'no' submit button element.
   *
   * @param array $form
   *   Drupal Form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form State.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax data in the response.
   */
  public function closeModal(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(
      new CloseModalDialogCommand()
    );
    return $response;
  }

  /**
   * Constructs the content to show to the user after clicking 'yes'.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form State.
   *
   * @return array
   *   Render array.
   */
  private function buildParagraphContent(FormStateInterface $form_state) {
    // @todo add the paragraph alt text to the paragraph temporary storage.
    // Update widget link.
    $resolvedLink = $this->t('All issues are resolved.');

    $content = '<div class="messages messages--status">' . $resolvedLink . '</div>';

    return [
      '#markup' => $content,
    ];
  }

}
