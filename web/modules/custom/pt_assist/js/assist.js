/**
 * @todo: Remove jQuery dependency, see https://digitoegankelijk.atlassian.net/browse/NLDOC-584
 * 
 * @todo: Error CKeditor. Patch or wait for update?
 * https://www.drupal.org/project/drupal/issues/3351600
 * Issue #3351600: ckeditor5.dialog.fix.js throw "Uncaught TypeError: event.target.classList is 
 * undefined" in Drupal 10 with the editor in a modal.
 * https://git.drupalcode.org/issue/drupal-3351600/-/tree/3351600-ckeditor5.dialog.fix.js-throw-uncaught-if-not-undefined
 */

(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.assistant = {

    attach: function (context, settings) {
   
      // Add an event listener to the form submission event.
      $(document).on('submit', 'form', function () {
        // Close the dialog modal using its ID or class.
        $('.dialog-modal').dialog('close');
      });
    }
  }

})(jQuery, Drupal);
