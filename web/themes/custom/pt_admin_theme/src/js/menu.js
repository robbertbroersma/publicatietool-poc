(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.menus = {

    attach: function (context, settings) {

      /* Get elements */
      const mainMenuToggle = once('once-main-menu-toggle', document.querySelector('.js-main-menu-toggle'));

      /* Click event */
      $(mainMenuToggle).on('click', toggleMenu);

      function toggleMenu() {
        document.body.classList.toggle('mobile-nav-open');
      };
    }
  }

})(jQuery, Drupal, drupalSettings);
