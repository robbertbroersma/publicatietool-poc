(function (Drupal, once) {
  'use strict';
  Drupal.behaviors.facets = {

    attach: function (context, settings) {

      // Reset form button.
      const facetsForm = document.getElementById('facets-form');
      let resetButton = document.querySelector('.js-button-reset');
      if(resetButton) {
        resetButton.addEventListener("click", resetForm);
      }

      function resetForm(e) {
        e.preventDefault();
        facetsForm.reset();
      }

  }}

  Drupal.behaviors.facetSearchField = {

    attach: function (context, settings) {

      // Create searchfield.
      const searchField = document.createElement("input");
        searchField.id = "filter";
        searchField.type = "text";
        searchField.placeholder = "Zoek onderwerpen";
        searchField.classList.add("filter","form-text");

      // Create submitbutton.
      const submitButton = document.createElement("input");
        submitButton.id = "filter-submit";
        submitButton.value = "zoeken";
        submitButton.type = "button";
        submitButton.classList.add("button","form-submit","js-form-submit");

      // Add search field and button to action container.
      const searchGroup = document.createElement("div");
        searchGroup.id = "filter-actions";
        searchGroup.classList.add("filter-actions");
      searchGroup.prepend(searchField, submitButton);

      // Add searchfield above options.
      const OptionContainer = document.querySelector(".js-add-filter .fieldset-wrapper");
      if(OptionContainer) {
        OptionContainer.prepend(searchGroup);
      }

      // Create an array with the checkbox options.
      let els = Array.from(document.querySelectorAll('.js-add-filter .js-form-type-checkbox'));
      
      // Create an array with the checkox labels.
      let labels = els.map(el => el.children[1].textContent);

      // Compare the labels from the user input with the labels from the list. 
      // If there is a match, hide all other options.
      const handler = value => {
        const matching = labels.map((label, idx, arr) => label.toLowerCase().includes(value.toLowerCase()) ? idx : null).filter(el => el != null);
        els.forEach((el, idx) => {
          if (matching.includes(idx)) {
            els[idx].style.display = 'block';
          } else {
            els[idx].style.display = 'none';
          }
        });
      };

      /* Show results upon submit */
      submitButton.addEventListener('click', () => handler.call(null, searchField.value));

  }}

})(Drupal, once);
