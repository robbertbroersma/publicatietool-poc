/**
 * @file
 * Provides JavaScript behaviors for the user menu.
 */
(function (Drupal, once) {

  "use strict";

  /**
   * User menu behaviour.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches the functionality for interaction with the user menu.
   */
  Drupal.behaviors.usemenu = {
    attach: function (context) {

      /* Get elements */
      const userMenuToggle = once('once-user-menu-toggle', document.querySelector('.js-user-menu-toggle'));
      
      /* Click event */
      userMenuToggle.forEach((menu) => {
        menu.addEventListener("click", toggleMenu);
      });

      function toggleMenu() {
        if(this.getAttribute("aria-expanded") === 'false') {        
          this.setAttribute("aria-expanded", true);
          this.parentElement.querySelector('.js-user-menu').removeAttribute("hidden");
        } else {
          this.setAttribute("aria-expanded", false);
          this.parentElement.querySelector('.js-user-menu').setAttribute("hidden", true);
        }
      };
    }
  };

})(Drupal, once);
