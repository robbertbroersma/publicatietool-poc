(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.dragzone = {

    attach: function (context, settings) {
      const node = $('.node__content')
      node.append('<button type="button" class="informative-close">Sluiten</button>');

      $('.informative-close').on('click', function () {
        $('.ui-dialog-content').dialog('close');
      })
    }
  }

})(jQuery, Drupal, drupalSettings);