const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const rename = require('gulp-rename');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
// const babel = require('gulp-babel');

const paths = {
  styles: {
    src: './src/scss/**/*.scss',
    dest: './css'
  },

  scripts: {
    src: './src/js/**/*.js',
    dest: './js'
  }
};

/*
 * Tasks
 */

function styles() {
  return gulp.src(paths.styles.src)
    .pipe(sass())
    .pipe(gulp.dest(paths.styles.dest));
}

function scripts() {
  return gulp.src(paths.scripts.src, { sourcemaps: true })
    // .pipe(babel())
    .pipe(uglify())
    .pipe(gulp.dest(paths.scripts.dest));
}

function watchFiles() {
  gulp.watch(paths.styles.src, styles)
  gulp.watch(paths.scripts.src, scripts)
}

/*
 * Exports
 */
exports.styles = styles;
exports.scripts = scripts;
exports.watch = watchFiles;

/*
 * Default task
 */
gulp.task('default', gulp.parallel(styles, scripts));

