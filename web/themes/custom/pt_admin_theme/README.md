# Publicatietool proof-of-concept

Administration theme specifically built for the publicatietool proof-of-concept.
It uses SASS variables for fonts, colors and layout so the theme can easily be
adjusted, but the base CSS files and images are already in place.

The contributed [Theme Negotiation by Rules](https://www.drupal.org/project/theme_rule)
is used to enable this theme for pages that have an "administrative" character,
such as the document overview and edit pages, the user profile page, etc. These
are not all traditional Drupal administration pages, but they are typically only
available for authenticated users. Navigate to `/admin/appearance/theme-rules`
to change the conditions that determine when this theme is used.

## REQUIREMENTS
Only needed for further theme development.
- [npm](https://www.npmjs.com/get-npm)
- [gulp](https://gulpjs.com/)

## INSTALLATION
- Enable the theme through the Drupal UI as administration theme.

## NPM & GULP TASKS
Only needed for further theme development.
- `$ npm install`
  - Installs node dependencies (run this from the theme directory).
- `$ gulp`
  - Generates all css and js files to the theme's root directory.

## THEME DEVELOPMENT
- First step in adjusting the theme is by changing the variables in
  `/scr/scss/base/_variables.scss`.
- Run `$ gulp` from within the theme root to build the  CSS and JS files.
