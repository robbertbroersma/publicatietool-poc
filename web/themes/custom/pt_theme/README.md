# Publicatietool proof-of-concept

Theme for the publicatietool proof-of-concept, with a Fractal component library 
integration to provide styleguide driven development.
It uses SASS variables for fonts, colors and layout so the theme can easily be 
adjusted.

## REQUIREMENTS

- [npm](https://www.npmjs.com/get-npm)
- [gulp](https://gulpjs.com/)

## REQUIRED MODULE

[Component Libraries](https://www.drupal.org/project/components)

## INSTALLATION

- Enable the theme though the Drupal UI.

## NPM & GULP TASKS

- `$ npm install`
  - Installs node dependencies (run this from the theme directory).
- `$ gulp build` 
  - Generates all css and js files to the `/public` directory. These files are 
    used by both Drupal and the Fractal components styleguide.
- `$ fractal start --sync`
  - Starts a local nodejs server. Fractal will run at `http://localhost:3000`
  - Using the --sync option enables BrowserSync when starting the server 
    (`https://fractal.build/guide/web/development-server.html#browsersync-integration`).
- `$ fractal build`
  - Static build of the Fractal styleguide under `/build`, so it will 
  be available under `/themes/custom/pt_theme/build/`.

## THEME DEVELOPMENT

- First step in adjusting the theme is by changing the variables in 
  `/scr/scss/base/_variables.scss`.
- Components are changed and/or added in `/src/components`. See 
  `https://fractal.build/` for details.
- The components twig files are used in the Drupal templates with twig functions 
  `extends` or `include`.
