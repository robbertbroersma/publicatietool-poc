
'use strict';

/* Create a new Fractal instance and export it. */
const fractal = require('@frctl/fractal').create();

/* Set the title of the project. */
fractal.set('project.title', 'Publicatietool proof-of-concept Component Library');

/* Tell Fractal where the components will live. */
fractal.components.set('path', __dirname + '/src/components');

/* Tell Fractal where the documentation pages will live */
fractal.docs.set('path', __dirname + '/src/docs');

/* Set the static HTML build destination. */
fractal.web.set('builder.dest', __dirname + '/build');

/* Set the path for static assets that can be used inside components. */
fractal.web.set('static.path', __dirname + '/public');

/* Use the integrated BrowserSync option by default. */
fractal.web.set('server.sync', true);

/* Default preview layout. */
fractal.components.set('default.preview', '@preview');

/* Require the Twig adapter */
const twigAdapter = require('@goat-cli/fractal-twig-adapter');
const twig = twigAdapter({
  nameSpaces: {
    atoms: '01-atoms',
    molecules: '02-molecules',
    organisms: '03-organisms',
    templates: '04-templates',
    pages: '05-pages',
  },
});

fractal.components.engine(twig);
fractal.components.set('ext', '.twig'); 
/* Set theme. */
const mandelbrot = require('@frctl/mandelbrot');
const pocTheme = mandelbrot({
  skin: {
    name: 'default',
    accent: '#A90061',
    complement: '#FFFFFF',
    links: '#A90061',
},
  panels: ['html', 'view', 'context', 'resources', 'info', 'notes'],
});
fractal.web.theme(pocTheme);

/* Set default status to WIP. */
fractal.components.set('default.status', 'wip');

/* Export the configured Fractal instance for use by the CLI tool. */
module.exports = fractal;
