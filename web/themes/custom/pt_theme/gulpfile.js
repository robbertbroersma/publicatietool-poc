const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const fractal = require('./fractal.config.js')
const logger = fractal.cli.console;
const rename = require('gulp-rename');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
// const babel = require('gulp-babel');
 
const paths = {
  styles: {
    src: './src/scss/**/*.scss',
    dest: './public/css'
  },

  scripts: {
    src: './src/js/**/*.js',
    dest: './public/js'
  },
  images: {
    src: './src/images/**/*',
    dest: './public/images'
  },
  fonts: {
    src: './src/fonts/**/*',
    dest: './public/fonts'
  }
};
 
/*
 * Tasks
 */

/*
 * FRACTAL
 */

/*
 * Start the Fractal server
 *
 * Passing the option 'sync: true' means it will
 * use BrowserSync to watch for changes to the filesystem and refresh the browser automatically.
 *
 * This task will also log any errors to the console.
 */

gulp.task('fractal:start', function(){
  const server = fractal.web.server({
      sync: true
  });
  server.on('error', err => logger.error(err.message));
  return server.start().then(() => {
      logger.success(`Fractal server is now running at ${server.url}`);
  });
});

/*
* Run a static export of the project web UI.
*
* This task will report on progress using the 'progress' event emitted by the
* builder instance, and log any errors to the terminal.
*
* The build destination will be the directory specified in the 'builder.dest'
* configuration option set above.
*/

function fractalBuild() {
  const builder = fractal.web.builder();
  builder.on('progress', (completed, total) => logger.update(`Exported ${completed} of ${total} items`, 'info'));
  builder.on('error', err => logger.error(err.message));
  return builder.build().then(() => {
      logger.success('Fractal build completed!');
  });
};

function styles() {
  return gulp.src(paths.styles.src)
    .pipe(sass())
    // .pipe(cleanCSS({ compatibility: '*' }))
    // .pipe(rename({
    //   suffix: '.min'
    // }))
    .pipe(gulp.dest(paths.styles.dest));
}

function scripts() {
  return gulp.src(paths.scripts.src, { sourcemaps: true })
    // .pipe(babel())
    .pipe(uglify())
    .pipe(gulp.dest(paths.scripts.dest));
}

function images() {
  return gulp.src(paths.images.src)
    .pipe(gulp.dest(paths.images.dest));
}

function fonts() {
  return gulp.src(paths.fonts.src)
    .pipe(gulp.dest(paths.fonts.dest));
}

function watch() {
  gulp.watch(paths.styles.src, styles);
  gulp.watch(paths.scripts.src, scripts);
  gulp.watch(paths.images.src, images);
  gulp.watch(paths.fonts.src, fonts);
}

/*
 * Specifying series or parallel
 */
const build = gulp.series(gulp.parallel(styles, scripts, images, fonts, fractalBuild));

/*
 * Exports
 */
exports.styles = styles;
exports.scripts = scripts;
exports.images = images;
exports.fonts = fonts;
exports.build = build;
// exports.watch = watch;

/*
 * Default task
 */
gulp.task( 'default',gulp.parallel(styles, scripts) );
