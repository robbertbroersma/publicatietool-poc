# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- NLDOC-896: Dropzone upload behaviour conform ux design.

## [3.8.2] - 2023-11-28

### Added

- NLDOC-885: User menu accessibility.
- NLDOC-1075: Added Documents overview keyboard operability.

## [3.8.1] - 2023-11-27

### Added

- NLDOC-1080: Added Documents overview accessibility issues.
- NLDOC-900: Added Dropzone explanation-link and popup.
- NLDOC-1103: Added two menu's to the website for the footer.
- NLDOC-896: Added dropzone styling.
- NLDOC-944: Added first, preposition and last name to profile.
- NLDOC-882: Added group view next to user.
- NLDOC-1074: Documents view styling refining.

## [3.8.0] - 2023-11-21

### Added

- NLDOC-1088: Fixed undefined method error on Documents view.
- NLDOC-1086: Added gulp watch.
- NLDOC-1079: Added field for documentsoverview.
- NLDOC-1078: Added user email registration module.
- NLDOC-71: Added Sentry / Raven.
- NLDOC-1081: Removed Atd.
- NLDOC-1087: PHP Code sniffer errors and added drupal/coder.

## [3.7.3] - 2023-11-15

### Added

- NLDOC-1021: Added filter field to the subject facets.
- NLDOC-573: Added the login_onlyemail module.
- NLDOC-576: Added extra row for a11y findings.

## [3.7.2] - 2023-11-08

### Added

- NLDOC-1027: Added custom module date range.
- NLDOC-1027: Added facet filtering on date modified.
- NLDOC-1026: Added facet filtering on date created.

## [3.7.1] - 2023-11-08

### Added

- NLDOC-1028: Added Facet filtering Author
- NLDOC-954: Added simple edit.

### Changed

- NLDOC-1023: Drupal updates core + contrib

## [3.7.0] - 2023-11-01

### Added

- NLDOC-1025: Added facet Filtering on Status.
- NLDOC-1024: Added facet Filtering on Subject.
- NLDOC-436: Added search facets to documents overview page.

## [3.6.1] - 2023-10-23

### Changed

- NLDOC-998: Applied Drupal updates.

## [3.6.0] - 2023-10-18

### Added

- NLDOC-940: bound document to user creating document (edit this description, created by Stephan)

## [3.5.0] - 2023-10-18

### Added

- NLDOC-997: Updated the accessibility summary shown when editing a document, to
  only indicated that a document is only considered accessible when there are no
  errors and warnings.
- NLDOC-988: Added archived moderation state to remove the publication status
  from a document.
- NLDOC-988: Added ECA workflow to perform 'save as concept', 'publish',
  'unpublish' or 'delete' action on a document.
- NLDOC-989: Added functionality to assign documents to the author's
  organization.

## [3.4.1] - 2023-10-17

### Fixed

- NLDOC-923: RabbitMQ Dead Letter Exchange.

## [3.4.0] - 2023-10-10

### Added

- NLDOC-435: Added Solr configuration to index the documents.

### Fixed

- NLDOC-534: Fixed the issue that editors and organization manager could not
  edit documents.
- NLDOC-739: Added meta information for 'first-heading-is-same-as-title'
  validation warnings, to prevent "unknown validation message" errors.
- NLDOC-852: Applied quickfix to mitigate the bug that occurs when handling
  unknown validation messages.
- NLDOC-928: Added patches to allow the use of stream wrapper for defining the
  path of library asset files.

## [3.3.2] - 2023-10-04

### Changed

- NLDOC-977: Extended README.md with instructions to apply updates for Drupal
  core and contributed modules; updated contributed Admin Toolbar module.

## [3.3.1] - 2023-09-26

### Changed

- NLDOC-888 NLDOC-962: Applied Drupal updates.

## [3.3.0] - 2023-09-20

### Added

- NLDOC-110: Added drag-and-drop upload functionality and replaced the content
  upload form with this drag-and-drop functionality.

### Changed

- NLDOC-962: Applied Drupal core security update.

## [3.2.1] - 2023-09-14

### Changed

- NLDOC-906: Updated Drupal core, Drush and Paragraphs module.

## [3.2.0] - 2023-09-05

### Fixed

- NLDOC-877: Added appropriate caching to the user profile block.

### Changed

- NLDOC-862: Split off arm64/amd64 architectures properly.
- NLDOC-875: Updated Configuration Inspector, Group and Redirect modules.

### Fixed

- NLDOC-773 NLDOC-876: Table exceeding the article borders.
- NLDOC-773: Removed footer menu from pages using the PT Admin theme.

## [3.1.5] - 2023-08-30

### Fixed

- NLDOC-862: Fix error in previous hotfix

## [3.1.4] - 2023-08-30

### Fixed

- NLDOC-862: Fix error in previous hotfix

## [3.1.3] - 2023-08-30

### Fixed

- NLDOC-862: Hotfix; manifests not working properly.

## [3.1.2] - 2023-08-30

### Changed

- NLDOC-862: Splitted off architectures `amd64` and `arm64` to different jobs in
  CI.

## [3.1.1] - 2023-08-23

### Fixed

- NLDOC-854: Corrected the endpoint url of the accessibility validation api.

## [3.1.0] - 2023-08-23

### Added

- NLDOC-842: Added functionality to validate accessibility when saving a
  document.

## [3.0.3] - 2023-08-17

### Changed

- NLDOC-811: Updated contributed Rabbit Hole module and Drush.

## [3.0.2] - 2023-08-16

### Fixed

- NLDOC-843: Rolled back the changes of NLDOC-461, since the RabbitMQ queue
  worker is not working properly.

## [3.0.1] - 2023-08-13

### Fixed

- NLDOC-840: Fixed warning unused css library in pt_theme.

## [3.0.0] - 2023-08-10

### Added

- NLDOC-813: Added scaffold to communicate with the Validation API.

### Changed

- NLDOC-813: Updated the validation message field item, to be able to store the
  all properties provided by the ESC/RE.

## [2.5.4] - 2023-08-08

### Changed

- NLDOC-581: Changed missing alt attribute validation from error to warning.

## [2.5.3] - 2023-08-03

### Changed

- NLDOC-823: Updated Drupal core to 10.1.2.
- NLDOC-823: Replaced custom pt_translation module with contributed Automatic
  Translation Template Discovery module.
- NLDOC-461: Use RabbitMQ queue worker instead of Cron to process conversion
  results.

### Fixed

- NLDOC-754: Fixed error message showing when an unknown validation message is
  returned by the ESC/RE.

## [2.5.2] - 2023-08-01

### Added

- NLDOC-709: DTAP structure for deployments.

## [2.5.1] - 2023-07-26

### Changed

- NLDOC-784: updated DDEV configuration after version 1.22.0 update.

## [2.5.0] - 2023-07-25

### Added

- NLDOC-691: Added functionality to add a copy-to-clipboard button, and attached
  it to the URL-alias field on the document edit page.

### Changed

- NLDOC-691: Changed the text of the URL-alias field on the document edit page.

## [2.4.1] - 2023-07-22

### Fixed

- NLDOC-659: SVG's not working properly (NGINX container)

## [2.4.0] - 2023-07-21

### Added

- NLDOC-583: Added the assistant to solve a missing alt text warning.
- NLDOC-733: Added Phing task to help to do Drupal updates.
- NLDOC-326: Added Rabbit Hole module to prevent direct access to terms of the
  subject taxonomy.

### Changed

- NLDOC-755: Updated Rabbit Hole module and Drush.
- NLDOC-733: Updated Drupal core to 10.1.1.
- NLDOC-715: Removed the document type taxonomy and the accompanying field
  `field_document_type` from the document content type.
- NLDOC-742: Hided corporate identity field from document edit page.
- NLDOC-325: Changed the widget to select element for the subject of a document.

### Removed

- NLDOC-755: 'pt_theme/file' library, since it is not used and in its current
  condition obstructing BigPipe from working correctly.

## [2.3.0] - 2023-07-12

### Changed

- NLDOC-712: Added custom attributes 'cy-data' on the documents overview page
  for Cypress tracking.

## [2.2.4] - 2023-07-06

### Changed

- NLDOC-700: Updated the Phing build and Composer bootstrap script to be
  compatible with Drush 12.

## [2.2.3] - 2023-07-06

### Changed

- NLDOC-151: NLDOC-659 Adjustments public theme.
- NLDOC-657: Updated multiple contributed modules.

## [2.2.2] - 2023-07-04

- NLDOC-111: Redeploy via Gitlab CI/CD.
- NLDOC-675: Fix for deployment environments.

## [2.2.1] - 2023-07-04

### Added

- NLDOC-111: Docker image for componenten.toegangvooriedereen.nl.

## [2.2.0] - 2023-07-04

### Added

- NLDOC-594: Refactor input fields/blocks in the left part of the edit page.
- NLDOC-575: Show number of errors, warnings and suggestions in entire document
  when editing a document.

## [2.1.1] - 2023-06-29

### Added

- NLDOC-667 NLDOC-666 Splitted configs.

## [2.0.0] - 2023-06-28

Start of keeping changelog.
